﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class SetUsername : MonoBehaviour
{
    public SaveData saveData;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetString("username", "-1") == "-1")
        {
            
        }
        else
        {
            saveData.username = PlayerPrefs.GetString("username", "-1");
            SceneManager.LoadScene("Menu");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetUsernamee()
    {
        if (text.text!="")
        {
            saveData.username = text.text;
            PlayerPrefs.SetString("username", text.text);
            SceneManager.LoadScene("Menu");
        }
        
    }
}
