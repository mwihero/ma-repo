// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:1,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:14,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33307,y:32651,varname:node_4013,prsc:2|diff-1304-RGB,spec-1974-OUT,gloss-7822-OUT,normal-4891-OUT,emission-5026-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32377,y:32558,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_UVTile,id:8663,x:32157,y:32931,varname:node_8663,prsc:2|UVIN-4045-UVOUT,WDT-802-OUT,HGT-802-OUT,TILE-6889-OUT;n:type:ShaderForge.SFN_Tex2d,id:4646,x:32546,y:32781,ptovrint:False,ptlb:node_4646,ptin:_node_4646,varname:node_4646,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d29bed4968e9f2b49bc51f3a79e31058,ntxv:0,isnm:False|UVIN-8663-UVOUT;n:type:ShaderForge.SFN_Multiply,id:5026,x:32873,y:32742,varname:node_5026,prsc:2|A-1304-RGB,B-8387-OUT;n:type:ShaderForge.SFN_Normalize,id:4891,x:33026,y:33050,varname:node_4891,prsc:2|IN-6524-OUT;n:type:ShaderForge.SFN_NormalBlend,id:6524,x:32829,y:33060,varname:node_6524,prsc:2|BSE-9271-OUT,DTL-9356-OUT;n:type:ShaderForge.SFN_Vector3,id:9356,x:32590,y:32993,varname:node_9356,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_TexCoord,id:4045,x:31864,y:32864,varname:node_4045,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Vector1,id:802,x:31852,y:33005,varname:node_802,prsc:2,v1:1;n:type:ShaderForge.SFN_Lerp,id:9271,x:32644,y:33168,varname:node_9271,prsc:2|A-9356-OUT,B-9356-OUT,T-7609-OUT;n:type:ShaderForge.SFN_Multiply,id:7822,x:32829,y:32908,varname:node_7822,prsc:2|A-4646-R,B-2624-OUT;n:type:ShaderForge.SFN_Slider,id:8387,x:31921,y:33125,ptovrint:False,ptlb:Emissive,ptin:_Emissive,varname:node_8387,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8311,max:1;n:type:ShaderForge.SFN_Slider,id:2624,x:32119,y:33190,ptovrint:False,ptlb:Roughnes,ptin:_Roughnes,varname:node_2624,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:7609,x:32297,y:33268,ptovrint:False,ptlb:Normal_Ref_Blnd,ptin:_Normal_Ref_Blnd,varname:node_7609,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:6889,x:31861,y:33298,ptovrint:False,ptlb:Tile,ptin:_Tile,varname:node_6889,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Vector1,id:1974,x:32996,y:32616,varname:node_1974,prsc:2,v1:0;proporder:1304-4646-7609-2624-8387-6889;pass:END;sub:END;*/

Shader "Shader Forge/Merda" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _node_4646 ("node_4646", 2D) = "white" {}
        _Normal_Ref_Blnd ("Normal_Ref_Blnd", Range(0, 1)) = 0
        _Roughnes ("Roughnes", Range(0, 1)) = 0
        _Emissive ("Emissive", Range(0, 1)) = 0.8311
        _Tile ("Tile", Range(0, 1)) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            ColorMask RGB
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 2.0
            uniform float4 _Color;
            uniform sampler2D _node_4646; uniform float4 _node_4646_ST;
            uniform float _Emissive;
            uniform float _Roughnes;
            uniform float _Normal_Ref_Blnd;
            uniform float _Tile;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_9356 = float3(0,0,1);
                float3 node_6524_nrm_base = lerp(node_9356,node_9356,_Normal_Ref_Blnd) + float3(0,0,1);
                float3 node_6524_nrm_detail = node_9356 * float3(-1,-1,1);
                float3 node_6524_nrm_combined = node_6524_nrm_base*dot(node_6524_nrm_base, node_6524_nrm_detail)/node_6524_nrm_base.z - node_6524_nrm_detail;
                float3 node_6524 = node_6524_nrm_combined;
                float3 normalLocal = normalize(node_6524);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_802 = 1.0;
                float2 node_8663_tc_rcp = float2(1.0,1.0)/float2( node_802, node_802 );
                float node_8663_ty = floor(_Tile * node_8663_tc_rcp.x);
                float node_8663_tx = _Tile - node_802 * node_8663_ty;
                float2 node_8663 = (i.uv0 + float2(node_8663_tx, node_8663_ty)) * node_8663_tc_rcp;
                float4 _node_4646_var = tex2D(_node_4646,TRANSFORM_TEX(node_8663, _node_4646));
                float gloss = 1.0 - (_node_4646_var.r*_Roughnes); // Convert roughness to gloss
                float perceptualRoughness = (_node_4646_var.r*_Roughnes);
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = 0.0;
                float specularMonochrome;
                float3 diffuseColor = _Color.rgb; // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_Color.rgb*_Emissive);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            ColorMask RGB
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 2.0
            uniform float4 _Color;
            uniform sampler2D _node_4646; uniform float4 _node_4646_ST;
            uniform float _Emissive;
            uniform float _Roughnes;
            uniform float _Normal_Ref_Blnd;
            uniform float _Tile;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 node_9356 = float3(0,0,1);
                float3 node_6524_nrm_base = lerp(node_9356,node_9356,_Normal_Ref_Blnd) + float3(0,0,1);
                float3 node_6524_nrm_detail = node_9356 * float3(-1,-1,1);
                float3 node_6524_nrm_combined = node_6524_nrm_base*dot(node_6524_nrm_base, node_6524_nrm_detail)/node_6524_nrm_base.z - node_6524_nrm_detail;
                float3 node_6524 = node_6524_nrm_combined;
                float3 normalLocal = normalize(node_6524);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float node_802 = 1.0;
                float2 node_8663_tc_rcp = float2(1.0,1.0)/float2( node_802, node_802 );
                float node_8663_ty = floor(_Tile * node_8663_tc_rcp.x);
                float node_8663_tx = _Tile - node_802 * node_8663_ty;
                float2 node_8663 = (i.uv0 + float2(node_8663_tx, node_8663_ty)) * node_8663_tc_rcp;
                float4 _node_4646_var = tex2D(_node_4646,TRANSFORM_TEX(node_8663, _node_4646));
                float gloss = 1.0 - (_node_4646_var.r*_Roughnes); // Convert roughness to gloss
                float perceptualRoughness = (_node_4646_var.r*_Roughnes);
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = 0.0;
                float specularMonochrome;
                float3 diffuseColor = _Color.rgb; // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
