﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RankRow : MonoBehaviour
{
    public Image userPic;
    public Text pos;
    public Text name;
    public Text trophies;
    public Text goalsMade;
    public Text gameWins;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetTexts(int pos,string name, int trophies)
    {
        this.pos.text = ""+pos;
        this.name.text = name;
        this.trophies.text = "" + trophies;
    }
}
