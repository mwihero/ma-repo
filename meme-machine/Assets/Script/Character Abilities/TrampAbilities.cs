﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class TrampAbilities : Bot {

    public bool manageAbilities = true;
	
	//public CarSystem carSystem;
	public GameManager gameManager;
	

	public DirectionTargetSetter directionTargetSetter;
	public ObjectTargetSetter objectTargetSetter;
	public PointTargetSetter pointTargetSetter;

	public GameObject row;
    public int howManyTimesIUsedAbilities = 0;
	[Header("First Ability")]
	public Ability firstAbility;

	public Vector3 firstAbilityTarget;
	public int firstAbilityNumberOfBalls=3;
	public int firstAbilityBallsCounter=0;
	public float firstAbilityBallsSpawnDelay=0.2f;
	public float firstAbilityBallsSpawnTimer=0f;
	public int firstAbilityBallsSpeed = 5;
    public GameObject firstAbilityExpGO;


	[Header("Second Ability")]
	public Ability secondAbility;

	public GameObject secondAbilityRocketPrefab;
	public GameObject secondAbilityRocket;
	public GameObject secondAbilityTarget;
    public GameObject secondAbilityExplosion;
	public float secondAbilityLaunchDuration=0.5f;
	public float secondAbilityLaunchTimer;
	public bool isSecondAbilityLaunching=false;
	// Use this for initialization

	[Header("Third Ability")]

	public Ability thirdAbility;

	public Vector3 thirdAbilityTarget;
	public float thirdAbilityDuration=7f;

	public GameObject trampWall;

	private PhotonView photonView;
	// Use this for initialization
	void Awake (){
		photonView = GetComponent<PhotonView>();
        championID = 0;
        gameObject.name = name + " " + ((int)Random.Range(1000, 9999));
        name = "Tramp";
    }
	void Start () {
		base.Start ();
        if (!isOnline)
        {
            photonView.ViewID = 0;
        }
        row = transform.Find ("Car").gameObject.transform.Find ("Row").gameObject;

		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		directionTargetSetter = GetComponent<DirectionTargetSetter> ();
		if(photonView.IsMine || !isOnline){
			StartCoroutine (Update10());
		}
		
        if (!photonView.IsMine && isOnline)
        {
            return;
        }
        if (!isABot)
        {
            firstAbility.abilityButton = GameObject.Find("FirstAbility");
            secondAbility.abilityButton = GameObject.Find("SecondAbility");
            thirdAbility.abilityButton = GameObject.Find("ThirdAbility");
            firstAbility.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateFirstAbility);
            secondAbility.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateSecondAbility);
            thirdAbility.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateThirdAbility);
            //secondAbilityRocket =PhotonNetwork.Instantiate ("Razzo", new Vector3(20,0,20),gameObject.transform.rotation, 0);
            //carSystem = gameObject.GetComponent<CarSystem>();
        }

	}

	// Update is called once per frame
	void Update () {
		base.Update ();
		firstAbility.AbilityUpdate ();
		secondAbility.AbilityUpdate ();
		thirdAbility.AbilityUpdate ();

		if(Input.GetKey (KeyCode.H)&& photonView.IsMine)
        {
            firstAbility.isUnlocked = true;
            firstAbility.isAvailable = true;
            ActivateFirstAbility ();
		}

		if (firstAbility.isUsing ){
			directionTargetSetter.DirectionTargetSetterUpdate ();
			if(directionTargetSetter.target!=Vector3.zero){
				firstAbilityTarget = directionTargetSetter.target;
				directionTargetSetter.target = Vector3.zero;
				StartCoroutine (FirstAbilityBallSpawner());
			}
		}

		if (secondAbility.isUsing){
			objectTargetSetter.ObjectTargetSetterUpdate ();
            if (objectTargetSetter.target == carSystem.car)
            {
                objectTargetSetter.ActivateSetter("Car");
            }

            if (objectTargetSetter.target!=null ){
				secondAbilityTarget = objectTargetSetter.target;
				objectTargetSetter.target = null;
				secondAbilityLaunchTimer =0;
				isSecondAbilityLaunching = true;
                secondAbilityRocket.SetActive(true);
                //secondAbilityRocket = Instantiate (secondAbilityRocketPrefab);
                /*if (isOnline) {
                    secondAbilityRocket = PhotonNetwork.Instantiate("Razzo", gameObject.transform.position, gameObject.transform.rotation, 0);
                }
                else
                {
                    secondAbilityRocket =  (GameObject)Instantiate(Resources.Load("Razzo"), gameObject.transform.position, gameObject.transform.rotation);
                }*/
            }
			if(isSecondAbilityLaunching){
				secondAbilityLaunchTimer += Time.deltaTime;
				secondAbilityRocket.transform.LookAt (secondAbilityTarget.transform);
                secondAbilityRocket.transform.eulerAngles +=new Vector3(270,0,0);
                secondAbilityRocket.transform.position = Vector3.Lerp (gameObject.transform.position,secondAbilityTarget.transform.position, secondAbilityLaunchTimer/secondAbilityLaunchDuration );
				if(secondAbilityLaunchTimer>=secondAbilityLaunchDuration){
					secondAbilityLaunchTimer = 0;
					isSecondAbilityLaunching = false;
					secondAbilityTarget.transform.parent.transform.gameObject.GetComponent<CarSystem>().CantMoveForCaller(4f);
                    //secondAbilityRocket.GetComponent<MeshRenderer> ().enabled=false;
                    secondAbilityRocket.SetActive(false);
                    if (isOnline) {
                        secondAbilityExplosion = PhotonNetwork.Instantiate("ExplosionBomb4", secondAbilityTarget.transform.position, gameObject.transform.rotation, 0);
                        //PhotonNetwork.Destroy(secondAbilityRocket);
                    }
                    else
                    {
                        secondAbilityExplosion = (GameObject)Instantiate(Resources.Load("ExplosionBomb4"), secondAbilityTarget.transform.position, gameObject.transform.rotation);
                       // Destroy(secondAbilityRocket);
                    }
					secondAbilityRocket.transform.position = new Vector3(20,0,20);
					secondAbility.isUsing = false;
					secondAbility.Cooldown ();
				}
			}
		}
			


	}

	public override void OnMasterClientSwitched(Photon.Realtime.Player newMasterClient)
	{
		if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
		{
			if(photonView.IsMine){
				StartCoroutine (Update10());
			}
		}
	}
	public void CreateWall(){
        if (isOnline) {
            trampWall = PhotonNetwork.Instantiate("TrampWall", carSystem.transform.TransformPoint(thirdAbilityTarget), carSystem.car.transform.rotation, 0);
        }
        else
        {
            trampWall =(GameObject) Instantiate(Resources.Load("TrampWall"), carSystem.transform.TransformPoint(thirdAbilityTarget), carSystem.car.transform.rotation );

        }
            trampWall.transform.parent = carSystem.transform;
		
		StartCoroutine (TrampWall());
	}
	public IEnumerator Update10(){
		int random;
		bool combo = false;
		bool combo2 = false;
		int target=-1;
		while (true && manageAbilities) {
			if(isABot && carSystem.alive){
				if((carSystem.nLeftBalls+carSystem.nRightBalls>2 || carSystem.cantMove.isTiming) && thirdAbility.isAvailable){

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1) {
                        if (strategy > 3)
                        {
                            BotActivateThirdAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                BotActivateThirdAbility();
                            }
                        }
                    }
						
					
				}
				else if (combo) {
					BotActivateFirstAbility (target);
					combo = false;
				} else if (combo2) {
					BotActivateFirstAbility (target);
					combo2 = false;
				} else if (firstAbility.isAvailable && secondAbility.isAvailable && !thirdAbility.isAvailable) {

					random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
					if (random == 1) {
						random = (int)Random.Range (1, 3+strategy);
						if (random == 1) {
							target = ChooseTarget (1);
							BotActivateFirstAbility (target);
						} else if (random == 2) {
							target = ChooseTarget (2);
							BotActivateSecondAbility (target);
						} else {
							combo = true;
							target = ChooseTarget (2);
							BotActivateSecondAbility (target);
						}
					}
				
				} else if (firstAbility.isAvailable && secondAbility.isAvailable && thirdAbility.isAvailable) {

					random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
					if (random == 1) {
						random = (int)Random.Range (1, 3 + strategy);
						if (random == 1) {
							target = ChooseTarget (1);
							BotActivateFirstAbility (target);
						} else if (random == 2) {
							target = ChooseTarget (2);
							BotActivateSecondAbility (target);
						} else if (random == 3) {
							//target = ChooseTarget ();
							BotActivateThirdAbility ();
						} else {
							random = (int)Random.Range (1, 3);
							if (random == 1) {
								combo = true;
								target = ChooseTarget (2);
								BotActivateSecondAbility (target);
							} else if (random == 2) {
								combo2 = true;

								BotActivateThirdAbility ();
							}
								
						}
					}
							

				} else if (firstAbility.isAvailable && !secondAbility.isAvailable && thirdAbility.isAvailable) {
					random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
					if (random == 1) {
						random = (int)Random.Range (1, 3 + strategy);
						if (random == 1) {
							target = ChooseTarget (1);
							BotActivateFirstAbility (target);
						} else if (random == 2) {
							BotActivateThirdAbility ();
						} else {
							combo2 = true;

							BotActivateThirdAbility ();
						}
					}

				} else if (!firstAbility.isAvailable && secondAbility.isAvailable && thirdAbility.isAvailable) {
					random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
					if (random == 1) {
						random = (int)Random.Range (1, 3);
						if (random == 1) {
							target = ChooseTarget (2);
							BotActivateSecondAbility (target);
						} else if (random == 2) {
							BotActivateThirdAbility ();
						}
					}

				}
				else if (firstAbility.isAvailable) {
				random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
                    print("Random: "+random);
				if (random == 1) {

					target = ChooseTarget (1);
				
					BotActivateFirstAbility (target);
				}
				} else if (secondAbility.isAvailable) {
				random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
				if (random == 1) {
					target = ChooseTarget (2);
					BotActivateSecondAbility (target);
				}
				}else if (thirdAbility.isAvailable) {
					random = (int)Random.Range (1, possibilityToUseAbility[abilityFrequency] + 1);
					if (random == 1) {
						
						BotActivateThirdAbility ();
					}
				}

		}

			yield return new WaitForSeconds (1f);
		}
	}
	public void BotActivateFirstAbility(int target){
		if(carSystem.alive && target>-1){
			firstAbilityTarget = ChooseFirstAbilityTargetPoint(target);
			StartCoroutine (FirstAbilityBallSpawner());
            howManyTimesIUsedAbilities++;

        }
	}
    /// <summary>
    /// Sceglie il punto in cui sparare le 3 palle
    /// i bot con Strategy inferiore a 3 spareranno in un punto a caso della porta
    /// I bot con strategy da 3 in su spareranno nel lato della porta con più spazio
    /// la precisione aumenta in base alla variabile Strategy
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
	public Vector3 ChooseFirstAbilityTargetPoint(int target){
		Vector3 targetPoint;
        if (strategy < 3)
        {
            targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].left.transform.position,
                gameManager.carSystemScripts[target].right.transform.position, Random.Range(0.3f, 0.7f));
        }
        else
        {
            if (Vector3.Distance(gameManager.carSystemScripts[target].car.transform.position,
                gameManager.carSystemScripts[target].right.transform.position) >
                Vector3.Distance(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].left.transform.position))
            {

                targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].right.transform.position, Random.Range(-0.2f + ((float)strategy) / 10f, 1.2f - ((float)strategy) / 10f));
            }
            else
            {
                targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].left.transform.position, Random.Range(-0.2f + ((float)strategy) / 10f, 1.2f - ((float)strategy) / 10f));
            }
        }
		return targetPoint;
	}
	public void BotActivateSecondAbility(int target){
		if(carSystem.alive && target>-1){
		isSecondAbilityLaunching = true;
			secondAbility.isUsing = true;
		secondAbilityTarget = gameManager.carSystemScripts [target].car;
            secondAbilityRocket.SetActive(true);
            //secondAbilityRocket = Instantiate (secondAbilityRocketPrefab);
            /*if (isOnline)
            {
                secondAbilityRocket = PhotonNetwork.Instantiate("Razzo", gameObject.transform.position, gameObject.transform.rotation, 0);
            }
            else
            {
                secondAbilityRocket = (GameObject)Instantiate(Resources.Load("Razzo"), gameObject.transform.position, gameObject.transform.rotation);
            }*/
            howManyTimesIUsedAbilities++;
        }
	}

	public void BotActivateThirdAbility(){
		int random;
		random = (int)Random.Range (1, 2 + 1);
		if(random==1){
			thirdAbilityTarget = new Vector3 (-2.8f,0.5f,0f);
		}
		if(random==2){
			thirdAbilityTarget = new Vector3 (2.8f,0.5f,0f);

		}
		thirdAbilityTarget = new Vector3 (0f,0.5f,0f);
		CreateWall ();
        //howManyTimesIUsedAbilities++;
    }
	public int ChooseTarget(int ab){
		int target=-1;
        List<int> cs = new List<int>();
        int random;
        bool targetGuarded;
        

        for (int i=0;i<4;i++){
            random = (int)Random.Range(1, 5 - strategy + 1);
            if (random==1)
            {
                targetGuarded = false;
            }
            else
            {
                targetGuarded = true;

            }
            if (!targetGuarded && !gameManager.carSystemScripts[i].guarded)
            {
                targetGuarded = true;
            }
            if (gameManager.carSystemScripts[i].alive && i!=carSystem.ID && !(ab==2 && gameManager.carSystemScripts[i].cantMove.isTiming) && targetGuarded)
            {
				cs.Add(i);
				 
			}
		}
        
        random = (int)Random.Range(1, 6 - strategy + 1);
        if (random == 1)
        {
            target = GetWhoCantMove(cs);
        }
        else
        {
            target = -1;
        }
        
        if(target==-1)
        {
            random = (int)Random.Range(1, 6 - strategy + 1);
            if (random == 1)
            {
                target = GetWhoHasManyBalls(cs);
            }
            else
            {
                target = -1;
            }
            
            if (target == -1)
            {
                random = (int)Random.Range(1, 6 - strategy + 1);
                if (random==1) {
                    if (gameManager.chart[0].ID == carSystem.ID)
                    {
                        target = gameManager.chart[1].ID;
                        print("Target (" + gameManager.carSystemScripts[target].name + ") second in chart by " + carSystem.name);
                    }
                    else
                    {
                        target = gameManager.chart[0].ID;
                        print("Target (" + gameManager.carSystemScripts[target].name + ") first in chart by "+ carSystem.name);
                    }
                }
                else
                {
                    target = cs[(int)Random.Range(0, (float)cs.Count)];
                    print("Target (" + gameManager.carSystemScripts[target].name + ") random choice by " + carSystem.name);
                }
            }
        }
        //if (ncs>0){target = cs[(int)Random.Range (0,(float)ncs)];}
		return target;
	}
    public int GetWhoHasManyBalls(List<int> cs)
    {
        int target = -1;
        List<int> csl = new List<int>();
        for (int i = 0; i < cs.Count; i++)
        {
            if (gameManager.carSystemScripts[cs[i]].nBallCollisionPoint > 2)
            {
                csl.Add(cs[i]);
            }
        }
        if (csl.Count > 0)
        {
            if (csl.Count == 1)
            {
                target = csl[0];
            }
            else
            {
                target = csl[(int)Random.Range(0, (float)csl.Count)];
            }
            print("Target (" + gameManager.carSystemScripts[target].name + ") has a lot of balls by " + carSystem.name);
        }
        return target;
    }
    public int GetWhoCantMove(List<int> cs)
    {
        int target = -1;
        List<int> csl = new List<int>();
        for (int i = 0; i < cs.Count; i++)
        {
            if (gameManager.carSystemScripts[cs[i]].cantMove.isTiming)
            {
                csl.Add(cs[i]);
            }
        }
        if (csl.Count > 0)
        {
            if (csl.Count == 1)
            {
                target = csl[0];
                print("Target count 1 (" + gameManager.carSystemScripts[target].name + ") can't move by " + carSystem.name);
            }
            else
            {
                target = csl[(int)Random.Range(0, (float)csl.Count)];
                print("Target more than 1 (" + gameManager.carSystemScripts[target].name + ") can't move by " + carSystem.name);
            }

        }

        return target;
    }
	IEnumerator FirstAbilityBallSpawner(){
		firstAbility.Cooldown ();
        if (isOnline)
        {
            photonView.RPC("LightExplosion", RpcTarget.All);
        }
        else
        {
            LightExplosion();
        }
        while (firstAbilityBallsCounter<firstAbilityNumberOfBalls){
            if (isOnline)
            {
                photonView.RPC("Spawn", RpcTarget.MasterClient, firstAbilityTarget);
            }
            else
            {
                Spawn(firstAbilityTarget);
            }
			//Spawn (firstAbilityTarget);
			yield return new WaitForSeconds(firstAbilityBallsSpawnDelay);
			firstAbilityBallsCounter++;
		}
		firstAbilityBallsCounter = 0;
		firstAbility.isUsing  = false;
		firstAbility.stato=0;
	}
    [PunRPC]
    public void LightExplosion()
    {
        firstAbilityExpGO.SetActive(false);
        firstAbilityExpGO.SetActive(true);
    }
	[PunRPC]
	public void Spawn(Vector3 pos){
		if(PhotonNetwork.IsMasterClient || !isOnline){
		if(carSystem.ballsManager.ReturnFreeBall()>-1){
                
                int i = carSystem.ballsManager.ReturnFreeBall();
            //carSystem.ballsManager.balls[carSystem.ballsManager.ReturnFreeBall()].GetPhotonView().RequestOwnership();
            carSystem.ballsManager.balls [carSystem.ballsManager.ReturnFreeBall ()].transform.position = row.transform.position;
                ParticleUser pu = particleUserManager.ReturnFreeParticleUser();
                pu.SetParticleUser(0f, 1, false, false, ((GameObject)Instantiate(Resources.Load("TrumpBall"))), carSystem.ballsManager.ballsScript[carSystem.ballsManager.ReturnFreeBall()].gameObject);
                pu.Play();
                carSystem.ballsManager.ballsScript [carSystem.ballsManager.ReturnFreeBall ()].ChangeType (0);
			carSystem.ballsManager.ballsScript[carSystem.ballsManager.ReturnFreeBall()].PushTo(pos,firstAbilityBallsSpeed);

                 
                //gameManager.ballsScript[gameManager.ReturnFreeBall()].Activate((int)Random.Range(0,1.9f));
            }
		}
	}
	public void ActivateFirstAbility (){//Lancio 3 palle
		if(firstAbility.isAvailable && firstAbility.isUnlocked && carSystem.alive && !carSystem.blockedAbility.isTiming){
			firstAbility.isUsing =true;
			directionTargetSetter.ActivateSetter ();
			firstAbility.ChangeButtonColor (Color.red);
		}
	}


	public void ActivateSecondAbility (){//Lancio 3 palle
		if (secondAbility.isAvailable && secondAbility.isUnlocked && carSystem.alive && !carSystem.blockedAbility.isTiming) {
			secondAbility.isUsing = true;
			//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			objectTargetSetter.ActivateSetter("Car");
			secondAbility.ChangeButtonColor (Color.red);
		}
	}

	public void ActivateThirdAbility (){//Lancio 3 palle
		if(thirdAbility.isAvailable && thirdAbility.isUnlocked && carSystem.alive && !carSystem.blockedAbility.isTiming)
        {
			thirdAbility.isUsing =true;
            
			thirdAbility.ChangeButtonColor (Color.red);
			//pointTargetSetter.ActivateSetter ();
			thirdAbilityTarget = new Vector3 (0,0.5f,0);
			CreateWall ();
            
		}
	}

	public IEnumerator TrampWall(){
        thirdAbility.isAvailable = false;
        carSystem.guarded = true;
        yield return new WaitForSeconds(thirdAbilityDuration);
        if (isOnline) {
            PhotonNetwork.Destroy(trampWall);
        }
        else
        {
            Destroy(trampWall);
        }
        carSystem.guarded = false;
        thirdAbility.Cooldown();
    }
}
