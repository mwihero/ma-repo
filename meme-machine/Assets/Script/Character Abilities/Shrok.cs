﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class Shrok : Bot {
	public string name="Shrok";

	public GameManager gameManager;
	public BallsManager ballsManager;
	 
	public PointTargetSetter pointTargetSetter;
	public DirectionTargetSetter directionTargetSetter;
	[Header("Scream Ability")]
	public Ability screamAbility;
	public float screamAbilityDuration=4f;
	public float screamAbilityTime=0f;
	public float screamAbilityMinDistance=4f;
    public float screamAbilityMinVelocity = 10f;
	public float screamAbilityForce=0.2f;
	public float screamAbilityUpdateEvery=0.2f;
    public GameObject screamAbilityRangeGO;
	[Header("Big Shrok Ability")]
	public Ability bigShrokAbility;
	public float bigShrokAbilityDuration=10f;
	[Header("Gain Life Ability")]

	public Ability gainLifeAbility;
    public GameObject gainLifeAbilityGO;
    public float gainLifeAbilityDuration=10f;
	public float gainLifeAbilityTime=0;
	public int gainLifeAbilityTempScore;
	public int gainLifeAbilityAddingPointsMultiplicator=2;

	[Header("Mud Ability")]
	public Ability mudAbility;
	public Vector3 mudAbilityTargetPoint;
	public Vector3 mudAbilityTargetDirection;
	public GameObject mudPrefab;
	public GameObject mudGO;
	public float mudDuration=10f;
    public float mudAbilityMaxWaitDuration;
    public float mudAbilityWaitTime;
    public Vector2 mudAbilityMaxWaitDurationRange=new Vector2(5f,25f);
    public int mudAbilityMaxBallWait;

    [Header("Long Mud Ability")]
	public Ability longMudAbility;
	public Vector3 longMudAbilityTargetPoint;
	public GameObject shrokLongMud;

	private PhotonView photonView;
    public bool isOnline;
	// Use this for initialization
	void Awake (){
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        championID = 1;
        photonView = GetComponent<PhotonView>();
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		ballsManager = GameObject.Find ("GameManager").GetComponent<BallsManager>();
        gameObject.name = name + " " + ((int)Random.Range(1000, 9999));
    }
	// Use this for initialization
	void Start () {
		base.Start ();
        if (!isOnline)
        {
            photonView.ViewID = 0;
        }
        if (!isOnline || photonView.IsMine)
        {
            StartCoroutine(Update10());
        }
        if (!photonView.IsMine && isOnline)
        {

            return;
        }
        if (!isABot)
        {
            screamAbility.abilityButton = GameObject.Find("FirstAbility");
            mudAbility.abilityButton = GameObject.Find("SecondAbility");
            gainLifeAbility.abilityButton = GameObject.Find("ThirdAbility");
            screamAbility.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateFirstAbility);
            mudAbility.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateSecondAbility);
            gainLifeAbility.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateThirdAbility);
        }
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();
		mudAbility.AbilityUpdate ();
        screamAbility.AbilityUpdate ();
		gainLifeAbility.AbilityUpdate ();

		if (screamAbility.isUsing) {
			screamAbilityTime += Time.deltaTime;
			if(screamAbilityTime>=screamAbilityDuration){
				screamAbility.isUsing = false;
				screamAbilityTime = 0;
                if (photonView.IsMine || !isOnline)
                {
                    screamAbility.Cooldown();
                }
                    screamAbilityRangeGO.SetActive(false);
            }
		}

		if (gainLifeAbility.isUsing) {
			gainLifeAbilityTime += Time.deltaTime;
			if(carSystem.score>gainLifeAbilityTempScore){
				gainLifeAbilityTempScore = carSystem.score;
				carSystem.AddPoints (gainLifeAbilityAddingPointsMultiplicator);
			}
			if(gainLifeAbilityTime>=gainLifeAbilityDuration){
				gainLifeAbility.isUsing = false;
                gainLifeAbilityGO.SetActive(false);
                gainLifeAbility.Cooldown ();
			}
		}

		if (mudAbility.isUsing ){
            if (mudAbility.stato == 0) {
                pointTargetSetter.PointTargetSetterUpdate();
                float precision = (float)strategy * 0.5f;
                if (isABot)
                {
                    pointTargetSetter.SendRayFrom(
                        new Vector3(
                            Random.Range(-3.5f + precision, 3.5f - precision),
                            transform.position.y,
                            Random.Range(-3.5f + precision, 3.5f - precision)
                            )
                            );
                }
                if (pointTargetSetter.target != Vector3.zero) {
                    mudAbilityTargetPoint = pointTargetSetter.target;
                    pointTargetSetter.target = Vector3.zero;
                    //StartCoroutine (FirstAbilityBallSpawner());
                    if (isOnline) {
                        mudGO = PhotonNetwork.Instantiate("Mud", mudAbilityTargetPoint, gameObject.transform.rotation, 0);
                    }
                    else
                    {
                        mudGO = (GameObject)Instantiate(Resources.Load("Mud"), mudAbilityTargetPoint, gameObject.transform.rotation);
                    }
                    mudAbility.stato = 1;
                }
            } else if (mudAbility.stato == 1 && isABot) {
                mudAbilityWaitTime += Time.deltaTime;
                if (mudGO.GetComponent<ShrokMud>().ballsScript.Count>=mudAbilityMaxBallWait)
                {
                    mudAbility.stato = 2;
                }
                if (mudAbilityWaitTime>mudAbilityMaxWaitDuration)
                {
                    if (mudGO.GetComponent<ShrokMud>().ballsScript.Count >= 1)
                    {
                        mudAbility.stato = 2;
                    }
                    else
                    {
                        SetMudWait();
                    }
                }


            } else if (mudAbility.stato == 2) {
                directionTargetSetter.DirectionTargetSetterUpdate();
                if (isABot)
                {
                    int target = ChooseTarget();
                    directionTargetSetter.target = ChooseMudAbilityDirectionPoint(target);
                }
                if (directionTargetSetter.target != Vector3.zero) {
                    mudAbilityTargetDirection = directionTargetSetter.target;
                    directionTargetSetter.target = Vector3.zero;
                    //StartCoroutine (FirstAbilityBallSpawner());
                    //mudGO=PhotonNetwork.Instantiate ("Mud", mudAbilityTargetPoint, gameObject.transform.rotation, 0);
                    mudGO.GetComponent<ShrokMud>().LaunchBallsLauncher(mudAbilityTargetDirection, isOnline);
                    mudAbility.stato = 0;
                    mudAbility.isUsing = false;
                    mudAbility.Cooldown();
                    if (isOnline) {
                        PhotonNetwork.Destroy(mudGO);
                    }
                    else
                    {
                        Destroy(mudGO);
                    }
                }
            }
		}
	}
    public void SetMudWait()
    {
        mudAbilityMaxBallWait = Random.Range(1,7);
        mudAbilityWaitTime = 0;
        mudAbilityMaxWaitDuration = Random.Range
            (mudAbilityMaxWaitDurationRange.x, mudAbilityMaxWaitDurationRange.y);
    }
    public IEnumerator Update10()
    {
        bool combo = false;
        int random;
        while (true)
        {
            if (isABot)
            {
                if ((carSystem.nLeftBalls + carSystem.nRightBalls > 1 || carSystem.cantMove.isTiming) && screamAbility.isAvailable)
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        if (strategy > 3)
                        {
                            ActivateScreamAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                ActivateScreamAbility();
                            }
                        }
                    }



                }
                if (ballsManager.AliveBallsCounter()>2 && mudAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        if (strategy > 3)
                        {
                            ActivateMudAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                ActivateMudAbility();
                            }
                        }
                    }
                }


                if ((screamAbility.isUsing || mudAbility.isUsing) && gainLifeAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        if (strategy > 3)
                        {
                            ActivateGainLifeAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                ActivateGainLifeAbility();
                            }
                        }
                    }
                }

                if (screamAbility.isAvailable && mudAbility.isAvailable && !gainLifeAbility.isAvailable)
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        random = (int)Random.Range(1, 3 );
                        if (random == 1)
                        {
                            ActivateScreamAbility();
                        }
                        else if (random == 2)
                        {
                            ActivateMudAbility();
                        }
                        
                    }

                }
                else if (screamAbility.isAvailable && mudAbility.isAvailable && gainLifeAbility.isAvailable)
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        random = (int)Random.Range(1, 3 + strategy);
                        if (random == 1)
                        {
                            ActivateScreamAbility();
                        }
                        else if (random == 2)
                        {
                            ActivateMudAbility();
                        }
                        else if (random == 3)
                        {
                            //target = ChooseTarget ();
                            ActivateGainLifeAbility();
                        }
                        else
                        {
                            
                                combo = true;
                                ActivateGainLifeAbility();
                           

                        }
                    }


                }
                else if (screamAbility.isAvailable && !mudAbility.isAvailable && gainLifeAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        random = (int)Random.Range(1, 3 + strategy);
                        if (random == 1)
                        {
                            ActivateScreamAbility();
                        }
                        else if (random == 2)
                        {
                            ActivateGainLifeAbility();
                        }
                        else
                        {
                            combo = true;

                            ActivateGainLifeAbility();
                        }
                    }

                }
                else if (!screamAbility.isAvailable && mudAbility.isAvailable && gainLifeAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        random = (int)Random.Range(1, 3);
                        if (random == 1)
                        {
                            ActivateMudAbility();
                        }
                        else if (random == 2)
                        {
                            ActivateGainLifeAbility();
                        }
                    }

                }
                else if (screamAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    print("Random: " + random);
                    if (random == 1)
                    {

                        ActivateScreamAbility();
                    }
                }
                else if (mudAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        ActivateMudAbility();
                    }
                }
                else if (gainLifeAbility.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {

                        ActivateGainLifeAbility();
                    }
                }

            }
            yield return new WaitForSeconds(1f);
        }
     }
    public int ChooseTarget()
    {
        int target = -1;
        List<int> cs = new List<int>();
        int random;
        bool targetGuarded;


        for (int i = 0; i < 4; i++)
        {
            random = (int)Random.Range(1, 5 - strategy + 1);
            if (random == 1)
            {
                targetGuarded = false;
            }
            else
            {
                targetGuarded = true;

            }
            if (!targetGuarded && !gameManager.carSystemScripts[i].guarded)
            {
                targetGuarded = true;
            }
            if (gameManager.carSystemScripts[i].alive && i != carSystem.ID  && targetGuarded)
            {
                cs.Add(i);

            }
        }

        random = (int)Random.Range(1, 6 - strategy + 1);
        if (random == 1)
        {
            target = GetWhoCantMove(cs);
        }
        else
        {
            target = -1;
        }

        if (target == -1)
        {
            random = (int)Random.Range(1, 6 - strategy + 1);
            if (random == 1)
            {
                target = GetWhoHasManyBalls(cs);
            }
            else
            {
                target = -1;
            }

            if (target == -1)
            {
                random = (int)Random.Range(1, 6 - strategy + 1);
                if (random == 1)
                {
                    if (gameManager.chart[0].ID == carSystem.ID)
                    {
                        target = gameManager.chart[1].ID;
                        print("Target (" + gameManager.carSystemScripts[target].name + ") second in chart by " + carSystem.name);
                    }
                    else
                    {
                        target = gameManager.chart[0].ID;
                        print("Target (" + gameManager.carSystemScripts[target].name + ") first in chart by " + carSystem.name);
                    }
                }
                else
                {
                    target = cs[(int)Random.Range(0, (float)cs.Count)];
                    print("Target (" + gameManager.carSystemScripts[target].name + ") random choice by " + carSystem.name);
                }
            }
        }
        //if (ncs>0){target = cs[(int)Random.Range (0,(float)ncs)];}
        return target;
    }
    public int GetWhoHasManyBalls(List<int> cs)
    {
        int target = -1;
        List<int> csl = new List<int>();
        for (int i = 0; i < cs.Count; i++)
        {
            if (gameManager.carSystemScripts[cs[i]].nBallCollisionPoint > 2)
            {
                csl.Add(cs[i]);
            }
        }
        if (csl.Count > 0)
        {
            if (csl.Count == 1)
            {
                target = csl[0];
            }
            else
            {
                target = csl[(int)Random.Range(0, (float)csl.Count)];
            }
            print("Target (" + gameManager.carSystemScripts[target].name + ") has a lot of balls by " + carSystem.name);
        }
        return target;
    }
    public int GetWhoCantMove(List<int> cs)
    {
        int target = -1;
        List<int> csl = new List<int>();
        for (int i = 0; i < cs.Count; i++)
        {
            if (gameManager.carSystemScripts[cs[i]].cantMove.isTiming)
            {
                csl.Add(cs[i]);
            }
        }
        if (csl.Count > 0)
        {
            if (csl.Count == 1)
            {
                target = csl[0];
                print("Target count 1 (" + gameManager.carSystemScripts[target].name + ") can't move by " + carSystem.name);
            }
            else
            {
                target = csl[(int)Random.Range(0, (float)csl.Count)];
                print("Target more than 1 (" + gameManager.carSystemScripts[target].name + ") can't move by " + carSystem.name);
            }

        }

        return target;
    }
    public Vector3 ChooseMudAbilityDirectionPoint(int target)
    {
        Vector3 targetPoint;
        if (strategy < 3)
        {
            targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].left.transform.position,
                gameManager.carSystemScripts[target].right.transform.position, Random.Range(0.3f, 0.7f));
        }
        else
        {
            if (Vector3.Distance(gameManager.carSystemScripts[target].car.transform.position,
                gameManager.carSystemScripts[target].right.transform.position) >
                Vector3.Distance(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].left.transform.position))
            {

                targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].right.transform.position, Random.Range(-0.2f + ((float)strategy) / 10f, 1.2f - ((float)strategy) / 10f));
            }
            else
            {
                targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].left.transform.position, Random.Range(-0.2f + ((float)strategy) / 10f, 1.2f - ((float)strategy) / 10f));
            }
        }
        return targetPoint;
    }

    public IEnumerator ScreamAbilityUpdate(){
		while(screamAbility.isUsing){
			for(int i=0; i<ballsManager.ballsScript.Length;i++){

				if(ballsManager.ballsScript[i].alive && !ballsManager.ballsScript[i].deathTimer && ballsManager.ballsScript[i].transform.position.y< transform.position.y)
                {
                    float distance = Vector3.Distance(ballsManager.balls[i].transform.position, carSystem.car.transform.position);

                    if (distance<=screamAbilityMinDistance){
                        if (!(ballsManager.ballsScript[i].rb.velocity.magnitude< screamAbilityMinVelocity) || !(distance > screamAbilityMinDistance/2) || !carSystem.IsThisBallsGoingTowardsThe(ballsManager.ballsScript[i],carSystem.wave)) {
                            Vector3 v = (ballsManager.balls[i].transform.position - carSystem.car.transform.position).normalized;

                            ballsManager.ballsScript[i].rb.velocity +=
                                new Vector3(v.x, 0, v.z)
                                * screamAbilityForce;
                            if (true) {
                                ballsManager.ballsScript[i].UpdateLastCarTouched(carSystem);
                            }
                        }
					}
				}
			}
			yield return new WaitForSeconds(screamAbilityUpdateEvery);
		}
        
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       

			if (stream.IsWriting) {
				
			
				stream.SendNext (screamAbility.isAvailable);
				stream.SendNext (screamAbility.isUnlocked);
				stream.SendNext (carSystem.alive);
				stream.SendNext (screamAbility.isUsing);
			} else {
                screamAbility.isAvailable = (bool)stream.ReceiveNext ();
                screamAbility.isUnlocked = (bool)stream.ReceiveNext ();
                carSystem.alive = (bool)stream.ReceiveNext ();
                screamAbility.isUsing = (bool)stream.ReceiveNext ();
			}
		
    }
    [PunRPC]
    public void ActivateScreamAbility (){ 
		if (screamAbility.isAvailable && screamAbility.isUnlocked && carSystem.alive && !screamAbility.isUsing) {
			screamAbility.isUsing = true;
			//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			screamAbilityTime = 0;
			
            screamAbilityRangeGO.SetActive(true);
			StartCoroutine(ScreamAbilityUpdate());
            
            if (isOnline)
            {
                if (photonView.IsMine)
                {
                    screamAbility.ChangeButtonColor(Color.red);
                }
            }
            else
            {
                if (!isABot)
                {
                    screamAbility.ChangeButtonColor(Color.red);
                }
            }
        }
	}
	public void ActivateFirstAbility (){
        if (isOnline)
        {
            photonView.RPC("ActivateScreamAbility", RpcTarget.All);
        }
        else
        {
            ActivateScreamAbility();
        }//ActivateScreamAbility ();
	}
	public void ActivateSecondAbility (){
        ActivateMudAbility();
    }
	public void ActivateThirdAbility (){ 
		ActivateGainLifeAbility ();
	}
	public void ActivateBigShrokAbility (){ 
		if (bigShrokAbility.isAvailable && bigShrokAbility.isUnlocked && carSystem.alive && !bigShrokAbility.isUsing && !carSystem.blockedAbility.isTiming) {
			bigShrokAbility.isUsing = true;
			bigShrokAbility.ChangeButtonColor (Color.red);
			//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			SetCarBigger();
		}
	}
	public void ActivateGainLifeAbility (){ 
		if (gainLifeAbility.isAvailable && gainLifeAbility.isUnlocked && carSystem.alive && !gainLifeAbility.isUsing && !carSystem.blockedAbility.isTiming) {
			gainLifeAbility.isUsing = true;
			gainLifeAbilityTime = 0;
			gainLifeAbilityTempScore = carSystem.score;
			gainLifeAbility.ChangeButtonColor (Color.red);
            gainLifeAbilityGO.SetActive(true);
			//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
 
		}
	}
	public void ActivateMudAbility (){//Lancio 3 palle
		if(mudAbility.isAvailable && mudAbility.isUnlocked && carSystem.alive && !carSystem.blockedAbility.isTiming)
        {
            if (!mudAbility.isUsing) {
                mudAbility.isUsing = true;
                if (!isABot) { 
                    pointTargetSetter.ActivateSetter();

                }
                else
                {
                    SetMudWait();
                }
				mudAbility.ChangeButtonColor (Color.red);
				mudAbility.stato = 0;
			} else {
                if (!isABot)
                {
                    directionTargetSetter.ActivateSetter(mudAbilityTargetPoint);
                    mudAbility.ChangeButtonColor(Color.blue);
                    mudAbility.stato = 2;
                }
			}
		}
	}
	public void ActivateLongMudAbility (){ 
		if(longMudAbility.isAvailable && longMudAbility.isUnlocked && carSystem.alive && !carSystem.blockedAbility.isTiming)
        {
			if (!longMudAbility.isUsing) {
				longMudAbility.isUsing = true;
				longMudAbilityTargetPoint = new Vector3 (0f,0.5f,0f);
                if (isOnline)
                {
                    shrokLongMud = PhotonNetwork.Instantiate("ShrokLongMud", carSystem.transform.TransformPoint(longMudAbilityTargetPoint), carSystem.car.transform.rotation, 0);
                }
                else
                {

                }
			}
		}
	}
	public void SetCarBigger(){
		StartCoroutine (AbilityBiggerCar());
	}
	public IEnumerator AbilityBiggerCar(){
		int size=carSystem.car.GetComponent<Car>().GetSize();

		carSystem.car.GetComponent<Car> ().SetSize (size + 1);
		yield return new WaitForSeconds(bigShrokAbilityDuration);
		carSystem.car.GetComponent<Car> ().SetSize (size);
		bigShrokAbility.isUsing = false;
		bigShrokAbility.Cooldown ();
	}
}
