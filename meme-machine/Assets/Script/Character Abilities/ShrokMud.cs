﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
public class ShrokMud : MonoBehaviour {
	List<GameObject>  balls;
	public List<Ball>  ballsScript;
	List<Rigidbody>  ballsRB;
	bool launching=false;
    private PhotonView photonView;
    // Use this for initialization
    void Start () {
        photonView = GetComponent<PhotonView>();
        balls =new  List<GameObject>();
		ballsScript=new  List<Ball>();
		ballsRB=new  List<Rigidbody>();
		StartCoroutine(MudUpdate());
	}
	
	// Update is called once per frame
	void Update () {
		 
	}
	private IEnumerator MudUpdate()
	{
		while(!launching){
			if(ballsRB.Count>0){

				foreach(Rigidbody ball in ballsRB){
					float ao=ball.velocity.magnitude;
					if(ao>1f){
						ball.velocity /= 10f;

					}else if(ao<=1f){
						ball.velocity = Vector3.zero;
					}
                    
				}
			}
			yield return new WaitForSeconds(1f/30f);
		}
	}
    [PunRPC]
    public void LaunchBalls(Vector3 direction){
		launching = true;
		foreach(Ball ball in ballsScript){
			ball.PushTo (direction,4);
            ball.isOwned = false;
        }
		 
	}
    public void LaunchBallsLauncher(Vector3 direction, bool isOnline)
    {
        if (isOnline) {
            photonView.RPC("LaunchBalls", RpcTarget.All, direction);
        }
        else
        {
            LaunchBalls(direction);
        }
    }
    void OnTriggerEnter(Collider c){
		if (c.tag=="Ball"){
			balls.Add (c.gameObject);
			ballsRB.Add (c.gameObject.GetComponent<Rigidbody>());
			ballsScript.Add (c.gameObject.GetComponent<Ball>());
            ballsScript[ballsScript.Count - 1].isOwned = true;
		}
	}
	void OnTriggerExit(Collider c){
		if (c.tag=="Ball"){
			balls.Remove (c.gameObject);
			ballsRB.Remove (c.gameObject.GetComponent<Rigidbody>());
			ballsScript.Remove (c.gameObject.GetComponent<Ball>());
            c.gameObject.GetComponent<Ball>().isOwned = true;
        }
	}

}
