﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class Thanos : Bot, IPunObservable
{
    public string name = "Thatos";

    public GameManager gameManager;
    public BallsManager ballsManager;
    public DirectionTargetSetter directionTargetSetter;
    [Header("Energy Sphere Ability")]
    public Ability energySphere;
    public float energySphereAbilityTime;
    public float energySphereAbilityDuration;
    public float energySphereAbilityAddReflectBounce;
    public GameObject energySphereGO;


    [Header("Energy Sphere Ability")]
    public Ability halfBalls;
    List<CarSystem.BallCollisionPoint> p  ;

    [Header("Portal Ability")]
    public Ability portal;
    public float portalAbilityTime;
    public Vector3 portalAbilityDirection;
    public float portalAbilityDuration;
    public GameObject portalAbilityGO;
    public float portalAbilityBallSpeed;
    public float portalAbilityDisappearDuration = 0.5f;


    // Start is called before the first frame update
    private PhotonView photonView;
    void Awake()
    {
        championID = 3;
        photonView = GetComponent<PhotonView>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        ballsManager = GameObject.Find("GameManager").GetComponent<BallsManager>();
        gameObject.name = name +" "+ ((int)Random.Range(1000,9999)); 
        //Time.timeScale = 0.6f;
    }
    void Start()
    {
        base.Start();
        if (!isOnline)
        {
            photonView.ViewID = 0;
        }
        p = new List<CarSystem.BallCollisionPoint>();

        if (!isOnline || photonView.IsMine)
        {
            StartCoroutine(Update10());
        }
        if (!photonView.IsMine && isOnline)
        {
            return;
        }
        if (!isABot)
        {
            energySphere.abilityButton = GameObject.Find("FirstAbility");
            energySphere.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateFirstAbility);
            halfBalls.abilityButton = GameObject.Find("SecondAbility");
            halfBalls.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateSecondAbility);
            portal.abilityButton = GameObject.Find("ThirdAbility");
            portal.abilityButton.GetComponent<Button>().onClick.AddListener(ActivateThirdAbility);
        }
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
        energySphere.AbilityUpdate();
        halfBalls.AbilityUpdate();
        portal.AbilityUpdate();
        if (energySphere.isUsing)
        {
            if (photonView.IsMine || !isOnline)
            {
                energySphereAbilityTime += Time.deltaTime;

                if (energySphereAbilityTime >= energySphereAbilityDuration)
                {
                    energySphereGO.SetActive(false);
                    energySphere.isUsing = false;
                    energySphere.Cooldown();
                }
            }
        }
        if (portal.isUsing)
        {
            if (photonView.IsMine || !isOnline)
            {
                if (portal.stato == 0)
                {
                    directionTargetSetter.DirectionTargetSetterUpdate();
                    if (isABot) {
                        int target = ChooseTarget();
                        directionTargetSetter.target = ChoosePortalAbilityDirectionPoint(target);
                    }
                    if (directionTargetSetter.target != Vector3.zero)
                    {
                        portalAbilityDirection = directionTargetSetter.target;
                        directionTargetSetter.target = Vector3.zero;
                        portal.stato = 1;
                        portalAbilityGO.SetActive(true);
                        portalAbilityGO.transform.position = new Vector3(0,35.4f,0);
                        portalAbilityGO.transform.LookAt(portalAbilityDirection);
                        portalAbilityTime = 0;

                    }

                    }
            }
            if (portal.stato==1)
            {
                portalAbilityTime += Time.deltaTime;
                if (portalAbilityTime> portalAbilityDuration)
                {
                    portal.stato = 0;
                    portal.isUsing = false;
                    portal.Cooldown();
                }
                
            }
        }
    }
    public IEnumerator Update10()
    {
        int random;
        while (true)
        {
            if (isABot)
            {
                if (ballsManager.AliveBallsCounter() > 2 && energySphere.isAvailable)
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        if (strategy > 3)
                        {
                            ActivateEnergySphereAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                ActivateEnergySphereAbility();
                            }
                        }
                    }

                }
                if ((carSystem.n2UnitDistanceBalls > 0 || ballsManager.AliveBallsCounter() > 4 || carSystem.cantMove.isTiming || carSystem.nBallCollisionPoint > 2) && portal.isAvailable)
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        if (strategy > 3)
                        {
                            ActivatePortalAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                ActivatePortalAbility();
                            }
                        }
                    }

                }
                bool halfAndPortal;
                
                random = (int)Random.Range(1, 5 - strategy + 1);
                if (random == 1)
                {
                    halfAndPortal = false;
                     
                }
                else
                {
                    halfAndPortal = true;
                     
                }
                if (!halfAndPortal && !portal.isUsing)
                {
                    halfAndPortal = true;
                }

                if ((carSystem.n2UnitDistanceBalls > 0 || carSystem.nBallCollisionPoint>3)  && halfBalls.isAvailable && halfAndPortal)
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        if (strategy > 3)
                        {
                            ActivateHalfBallsAbility();
                        }
                        else
                        {
                            random = (int)Random.Range(1, 8 - strategy + 1);
                            if (random == 1)
                            {
                                ActivateHalfBallsAbility();
                            }
                        }
                    }

                }
                 

                if (energySphere.isAvailable && portal.isAvailable )
                {

                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        random = (int)Random.Range(1, 3);
                        if (random == 1)
                        {
                            ActivateEnergySphereAbility();
                        }
                        else if (random == 2)
                        {
                            ActivatePortalAbility();
                        }

                    }

                }
                
                else if (energySphere.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    print("Random: " + random);
                    if (random == 1)
                    {

                        ActivateEnergySphereAbility();
                    }
                }
                else if (portal.isAvailable)
                {
                    random = (int)Random.Range(1, possibilityToUseAbility[abilityFrequency] + 1);
                    if (random == 1)
                    {
                        ActivatePortalAbility();
                    }
                }
                 




            }
                yield return new WaitForSeconds(1f);
        }

    }

    public int ChooseTarget()
    {
        int target = -1;
        List<int> cs = new List<int>();
        int random;
        bool targetGuarded;


        for (int i = 0; i < 4; i++)
        {
            random = (int)Random.Range(1, 5 - strategy + 1);
            if (random == 1)
            {
                targetGuarded = false;
            }
            else
            {
                targetGuarded = true;

            }
            if (!targetGuarded && !gameManager.carSystemScripts[i].guarded)
            {
                targetGuarded = true;
            }
            if (gameManager.carSystemScripts[i].alive && i != carSystem.ID && targetGuarded)
            {
                cs.Add(i);

            }
        }

        random = (int)Random.Range(1, 6 - strategy + 1);
        if (random == 1)
        {
            target = GetWhoCantMove(cs);
        }
        else
        {
            target = -1;
        }

        if (target == -1)
        {
            random = (int)Random.Range(1, 6 - strategy + 1);
            if (random == 1)
            {
                target = GetWhoHasManyBalls(cs);
            }
            else
            {
                target = -1;
            }

            if (target == -1)
            {
                random = (int)Random.Range(1, 6 - strategy + 1);
                if (random == 1)
                {
                    if (gameManager.chart[0].ID == carSystem.ID)
                    {
                        target = gameManager.chart[1].ID;
                        print("Target (" + gameManager.carSystemScripts[target].name + ") second in chart by " + carSystem.name);
                    }
                    else
                    {
                        target = gameManager.chart[0].ID;
                        print("Target (" + gameManager.carSystemScripts[target].name + ") first in chart by " + carSystem.name);
                    }
                }
                else
                {
                    target = cs[(int)Random.Range(0, (float)cs.Count)];
                    print("Target (" + gameManager.carSystemScripts[target].name + ") random choice by " + carSystem.name);
                }
            }
        }
        //if (ncs>0){target = cs[(int)Random.Range (0,(float)ncs)];}
        return target;
    }
    public int GetWhoHasManyBalls(List<int> cs)
    {
        int target = -1;
        List<int> csl = new List<int>();
        for (int i = 0; i < cs.Count; i++)
        {
            if (gameManager.carSystemScripts[cs[i]].nBallCollisionPoint > 2)
            {
                csl.Add(cs[i]);
            }
        }
        if (csl.Count > 0)
        {
            if (csl.Count == 1)
            {
                target = csl[0];
            }
            else
            {
                target = csl[(int)Random.Range(0, (float)csl.Count)];
            }
            print("Target (" + gameManager.carSystemScripts[target].name + ") has a lot of balls by " + carSystem.name);
        }
        return target;
    }
    public int GetWhoCantMove(List<int> cs)
    {
        int target = -1;
        List<int> csl = new List<int>();
        for (int i = 0; i < cs.Count; i++)
        {
            if (gameManager.carSystemScripts[cs[i]].cantMove.isTiming)
            {
                csl.Add(cs[i]);
            }
        }
        if (csl.Count > 0)
        {
            if (csl.Count == 1)
            {
                target = csl[0];
                print("Target count 1 (" + gameManager.carSystemScripts[target].name + ") can't move by " + carSystem.name);
            }
            else
            {
                target = csl[(int)Random.Range(0, (float)csl.Count)];
                print("Target more than 1 (" + gameManager.carSystemScripts[target].name + ") can't move by " + carSystem.name);
            }

        }

        return target;
    }
    public Vector3 ChoosePortalAbilityDirectionPoint(int target)
    {
        Vector3 targetPoint;
        if (strategy < 3)
        {
            targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].left.transform.position,
                gameManager.carSystemScripts[target].right.transform.position, Random.Range(0.3f, 0.7f));
        }
        else
        {
            if (Vector3.Distance(gameManager.carSystemScripts[target].car.transform.position,
                gameManager.carSystemScripts[target].right.transform.position) >
                Vector3.Distance(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].left.transform.position))
            {

                targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].right.transform.position, Random.Range(-0.2f + ((float)strategy) / 10f, 1.2f - ((float)strategy) / 10f));
            }
            else
            {
                targetPoint = Vector3.Lerp(gameManager.carSystemScripts[target].car.transform.position,
                    gameManager.carSystemScripts[target].left.transform.position, Random.Range(-0.2f + ((float)strategy) / 10f, 1.2f - ((float)strategy) / 10f));
            }
        }
        return targetPoint;
    }

    public void ActivateFirstAbility()
    {
         
        ActivateEnergySphereAbility();
    }
    public void ActivateSecondAbility()
    {

        ActivateHalfBallsAbility();
    }
    public void ActivateThirdAbility()
    {

        ActivatePortalAbility();
    }
    public void ActivateHalfBallsAbility()
    {
        if (halfBalls.isAvailable && halfBalls.isUnlocked && carSystem.alive && !halfBalls.isUsing && !carSystem.blockedAbility.isTiming)
        {

            HalfBalls();
            //screamAbilityRangeGO.GetComponent<MeshRenderer>().enabled = true;
            //StartCoroutine(DoubleAbilityUpdate());

            if (photonView.IsMine || !isOnline)
            {
                //energySphere.ChangeButtonColor(Color.red);
                halfBalls.Cooldown();
            }
        }
    }
    public void HalfBalls()
    {
        
        foreach (CarSystem.BallCollisionPoint b in carSystem.ballCollisionPoint)
        {
            if (b.exists)
            {
                //b.ball.light.color = Color.green;
                print("sisisi");
                if (p.Count > 0)
                {
                    for (int i = 0; i < p.Count; i++)
                    {
                        if (b.distance < p[i].distance)
                        {
                            print("pppp2: "+i);
                            p.Insert(i, b);
                            //b.ball.light.color = Color.white;
                            i = p.Count;
                        }
                        else
                        {

                            if (i == (p.Count - 1))
                                
                            {
                                print("pppp3: "+i);
                                p.Add(b);
                               // b.ball.light.color = Color.cyan;
                                i = p.Count;
                            }
                        }
                    }
                }
                else
                {
                    print("pppp");
                    p.Add(b);
                   // b.ball.light.color = Color.magenta;
                }
                
            }
        }
        
        if (p.Count>1)
        {
            int half = p.Count / 2;
            for (int i = half; i< p.Count; i++)
            {
                p[i].ball.Disappear(true,true,true,1);
                ParticleUser pu = particleUserManager.ReturnFreeParticleUser();
                pu.SetParticleUser(0f, 3, false, false, ((GameObject)Instantiate(Resources.Load("ThaSnap"))), p[i].ball.gameObject);
                pu.Play();
                //p[i].ball.light.color = Color.black;
            }
            for (int i = 0; i < half; i++)
            {
                //p[i].ball.light.color = Color.blue;
            }
            print("cmon");
        }
        else if (p.Count == 1)
        {
            p[0].ball.Disappear(true, true, true, 1);
            ParticleUser pu = particleUserManager.ReturnFreeParticleUser();
            pu.SetParticleUser(0f, 3, false, false, ((GameObject)Instantiate(Resources.Load("ThaSnap"))), p[0].ball.gameObject);
            pu.Play();
            //p[0].ball.light.color = Color.red;
            print("cmon2");
        }
        else
        {
            print("cmon3");
        }
        print("bro: " + p.Count);
        p.Clear();
    }
    
    public void ActivateEnergySphereAbility()
    {
        print("energy");
        if (energySphere.isAvailable && energySphere.isUnlocked && carSystem.alive && !energySphere.isUsing && !carSystem.blockedAbility.isTiming)
        {
            energySphereAbilityTime = 0;
            energySphere.isUsing = true;
            carSystem.ActivateAddReflectBounce(energySphereAbilityDuration, energySphereAbilityAddReflectBounce);
            carSystem.ActivateBlockImmune(energySphereAbilityDuration);
            energySphereGO.SetActive(true);
            //screamAbilityRangeGO.GetComponent<MeshRenderer>().enabled = true;
            //StartCoroutine(DoubleAbilityUpdate());
            if (isOnline)
            {
                photonView.RPC("EnergySphereParticle", RpcTarget.All);
            }
            else
            {
                EnergySphereParticle();
            }
            if (photonView.IsMine || !isOnline)
            {
                energySphere.ChangeButtonColor(Color.red);
            }
        }

    }

    public void ActivatePortalAbility()
    {
        //print("energy");
        if (portal.isAvailable && portal.isUnlocked && carSystem.alive && !portal.isUsing && !carSystem.blockedAbility.isTiming)
        {

            energySphereAbilityTime = 0;
            portal.isUsing = true;
            //directionTargetSetter.ActivateSetter();
            
            portal.stato = 0;
            portalAbilityGO.SetActive(false);
            if (!isABot) {
                portal.ChangeButtonColor(Color.red);
                directionTargetSetter.ActivateSetter(new Vector3(0, 35, 0));
            }
            //screamAbilityRangeGO.GetComponent<MeshRenderer>().enabled = true;
            //StartCoroutine(DoubleAbilityUpdate());

            if (photonView.IsMine || !isOnline)
            {
                portal.ChangeButtonColor(Color.red);
            }
        }

    }
    [PunRPC]
    public void EnergySphereParticle() {
        //ParticleUser pu = particleUserManager.ReturnFreeParticleUser();
        //pu.SetParticleUser(0, energySphereAbilityDuration, false, false, ((GameObject)Instantiate(Resources.Load("ThaShield"))), carSystem.car);
        //pu.Play();

    }
    public int BallInPortal(Ball ball)
    {
        int c = 0;
        if (portal.stato==1)
        {
            ball.noGoal = true;
            ball.rb.velocity /= 2;
            ball.Disappear(true, true, false, 0.2f);
            StartCoroutine(SpawnBallsFromPortal());

            c = 1;
        }
        return c;
    }
    IEnumerator SpawnBallsFromPortal()
    {
        yield return new WaitForSeconds(0.5f);
         

        if (carSystem.ballsManager.ReturnFreeBall() > -1)
        {

            int i = carSystem.ballsManager.ReturnFreeBall();
            //carSystem.ballsManager.balls[carSystem.ballsManager.ReturnFreeBall()].GetPhotonView().RequestOwnership();
            carSystem.ballsManager.balls[carSystem.ballsManager.ReturnFreeBall()].transform.position = new Vector3(0, 35, 0);

            carSystem.ballsManager.ballsScript[carSystem.ballsManager.ReturnFreeBall()].ChangeType(0);
            carSystem.ballsManager.ballsScript[carSystem.ballsManager.ReturnFreeBall()].PushTo(portalAbilityDirection, 2);


            //gameManager.ballsScript[gameManager.ReturnFreeBall()].Activate((int)Random.Range(0,1.9f));
        }
    }
    public void BallTouched(Ball ball)
    {
        if (energySphere.isUsing) {
            ParticleUser pu = particleUserManager.ReturnFreeParticleUser();
            pu.SetParticleUser(0f, 1, false, false, ((GameObject)Instantiate(Resources.Load("ThaBounceBall"))), ball.gameObject);
            pu.Play();
        }
    }
}
