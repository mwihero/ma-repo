﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using com.EYS.memesarena;
public class PickCharacter : MonoBehaviour
{
    // Start is called before the first frame update
    [System.Serializable]
    public class Character
    {
        public int id;
        public Sprite image;
        public string name ="";
        public float speed;
        public int dimension;
        public float bounce;


    }
    public SelectedCharacterScript[] selectedCharacterScript;
    public Character[] characters;
    public GameObject characterBox;
    public GameObject characterPanel;
    public int selected = 0;
    public bool isOnLine;
    public int playerID;
    public int playersNumber;
    public LobbyScript lobbyScript;
    public SaveData saveData;
    void Start()
    {
        isOnLine = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        saveData = GameObject.FindWithTag("SaveData").GetComponent<SaveData>();
        if (isOnLine)
        {
            playerID = lobbyScript.GetPlayerID();
            playersNumber = lobbyScript.GetPlayersNumber();
            print("ao: "+ playersNumber);
        }
        else
        {
            playersNumber = 1;
            playerID = 1;
        }
        foreach (Character c in characters)
        {
            GameObject g = Instantiate(characterBox);
            g.transform.SetParent(characterPanel.transform);
            g.transform.localScale = Vector3.one;
            g.transform.localPosition = new Vector3(g.transform.localPosition.x, g.transform.localPosition.y, 0f);
            g.GetComponent<CharacterBoxScript>().Initialize(c.id,c.image, c.name);
        }
        
        for (int i= playersNumber+1; i < 5;i++)
        {
            int random, trophies = 0;
            if (saveData.leagueID==0)
            {
                

                random = (int)Random.Range(0, 2);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(0, 2);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(0, 3);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;

                
            }else if (saveData.leagueID == 1)
            {
                random = (int)Random.Range(1, 3);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(1, 3);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(1, 3);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }
            else if (saveData.leagueID == 2)
            {
                random = (int)Random.Range(2, 4);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(2, 4);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(1, 4);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }
            else if (saveData.leagueID == 3)
            {
                random = (int)Random.Range(2, 5);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(3, 5);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(2, 5);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }
            else if (saveData.leagueID == 4)
            {
                random = (int)Random.Range(4, 6);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(4, 6);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(3, 6);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }

            trophies *= 30;

            trophies += (int)Random.Range(-15, 15);
            PlayerPrefs.SetInt("Bot" + i + " Trophies", trophies);

            UpdateSelectedCharacter(i, 0 +((int)(Random.Range(0,2)))==1?0:2);
            SetSelectedCharacterName(i, "Bot"+i+" "+trophies);
        }

        /*
         * for (int i = 1;i< playersNumber; i++)
        {
            SetSelectedCharacterName(i, "Bot" + i);
        }*/
        SetSelectedCharacterName(1, PlayerPrefs.GetString("username"));
        foreach (Photon.Realtime.Player p in PhotonNetwork.PlayerList)
        {
            SetSelectedCharacterName(p.ActorNumber, p.NickName);
       
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateMySelectedCharacter(int champId)
    {
        selected = champId;
        selectedCharacterScript[playerID-1].Initialize(characters[champId].image, characters[champId].name);
        lobbyScript.SelectedNewChampion(champId);
        PlayerPrefs.SetInt("MyChampion", champId);
    }
    public void UpdateSelectedCharacter(int playerId,int champId)
    {
        if (champId>-1) {
            //selected = champId;
            selectedCharacterScript[playerId - 1].Initialize(characters[champId].image, characters[champId].name);
            PlayerPrefs.SetInt("Champion" + playerId, champId);
        }
        //lobbyScript.SelectedNewChampion(champId);
    }
    public void SetSelectedCharacterName(int playerId, string name)
    {
        selectedCharacterScript[playerId - 1].SetPlayerName(name);
    }
}
