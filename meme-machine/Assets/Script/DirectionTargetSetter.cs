﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionTargetSetter : MonoBehaviour {

	public CarSystem carSystem;
	public bool usingThis;
	public int stato=0;
	public GameObject rowPrefab;
	public GameObject row;
	public Vector3 target;
	public LayerMask lm;
     
    // Use this for initialization
    void Start () {
		carSystem = GetComponent<CarSystem> ();
        
        lm = LayerMask.GetMask("TapField" + carSystem.ID);
		//row = transform.Find ("Car").gameObject.transform.Find ("Row").gameObject;
	}
	
	// Update is called once per frame
	public void DirectionTargetSetterUpdate () {
		if (usingThis){

			if (stato == 0) {
				if (Input.GetMouseButtonUp (0)) {
					stato++;
				}
			}
			else if(stato==1){

				if(Input.GetMouseButton(0))
				{
                    //create a ray cast and set it to the mouses cursor position in game
                    print("PREMEEE");
					Ray rayw = transform.Find ("Camera").gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (rayw, out hit, 30,lm)) 
					{
						//draw invisible ray cast/vector
						Debug.DrawLine (rayw.origin, hit.point);
						row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
						//row.transform.eulerAngles = new Vector3 (90,0,row.transform.localEulerAngles.y*-1);
						//row.transform.eulerAngles =hit.point;
						//log hit area to the console
						//Debug.Log(hit.point);

					}    
				}
				if (Input.GetMouseButtonUp (0)) {
					Ray rayw = transform.Find ("Camera").gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (rayw, out hit, 30,lm)) 
					{
						//draw invisible ray cast/vector
						Debug.DrawLine (rayw.origin, hit.point);
						row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
						row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = false;
						target = hit.point;
						stato = 2;
						row.transform.LookAt(Vector3.forward);
						usingThis = false;
					}

				}
			}
		}
	}
		
	public void ActivateSetter(Vector3 rowPos){
		stato = 0;
		usingThis = true;
		target = Vector3.zero;
		row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
		row.transform.position = new Vector3 (rowPos.x,row.transform.position.y,rowPos.z);
	}
    public void ActivateSetter()
    {
        stato = 0;
        usingThis = true;
        target = Vector3.zero;
        row.transform.Find("RowImage").gameObject.GetComponent<SpriteRenderer>().enabled = true;
        
    }
}
