﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Alert : MonoBehaviour
{
    public Text text;
    public GameObject cancelButtonGO;
    public Text cancelButtonText;
    public Text okayButtonText;
    public GameObject blackGO;
    public Func<string, int> okayMethod;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetText(Text textComponent,string text)
    {
        textComponent.text = text;
    }
    public void ActivatePurchaseAlert(string question, Func<string, int> method)
    {
        ActivateAlert(question, "Yes", "Cancel");
        this.okayMethod = method;
    }
    public void ActivateAlert(string question, string okayText)
    {
        blackGO.SetActive(true);
        SetText(text,question);
        SetText(okayButtonText,okayText);
        cancelButtonGO.SetActive(false);
    }
    public void ActivateAlert(string question, string okayText, string cancelText)
    {
        ActivateAlert(question,okayText);
        SetText(cancelButtonText,cancelText);
        cancelButtonGO.SetActive(true);
    }
    public void CancelButtonClicked()
    {
        blackGO.SetActive(false);
    }
    public void OkayButtonClicked()
    {
        blackGO.SetActive(false);
        okayMethod("");
    }
}
