﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreGoal : MonoBehaviour
{
    // Start is called before the first frame update
    public Bot botScript;
    public float CollisionOffset = 0;
    public GameObject[] EffectOnCollision;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag=="Ball")
        {
            if (botScript.GetType() == typeof(Thanos))
            {
                RaycastHit hit;
                if (Physics.Raycast(other.gameObject.transform.position,
                    other.gameObject.GetComponent<Rigidbody>().velocity.normalized
                    , out hit, 30))
                {
                    //draw invisible ray cast/vector
                    //Debug.DrawLine(rayw.origin, hit.point);
                    //row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
                    //row.transform.eulerAngles = new Vector3 (90,0,row.transform.localEulerAngles.y*-1);
                    //row.transform.eulerAngles =hit.point;
                    //log hit area to the console
                    //Debug.Log("hit: " + hit.point);

                }
                
                if (((Thanos)botScript).BallInPortal(other.gameObject.GetComponent<Ball>()) == 1)
                {
                    foreach (var effect in EffectOnCollision)
                    {
                        Vector3 v = other.gameObject.transform.position + new Vector3(0, 0.0f, 0);

                        var instance = Instantiate(effect, v, new Quaternion()) as GameObject;
                        //instance.transform.LookAt(v+ other.gameObject.GetComponent<Rigidbody>().velocity.normalized);
                        //if (!CollisionEffectInWorldSpace) instance.transform.parent = transform;
                        //Destroy(instance, DestroyTimeDelay);
                    }
                }
                

                
            }
        }
    }
}
