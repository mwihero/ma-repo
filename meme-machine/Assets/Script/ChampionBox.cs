﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChampionBox : MonoBehaviour
{
    public int championID;
    public int ID;
    public bool selected;
    public Image championImage;
    public Text championName;
    public GameObject lockImage;
    public Image borderImage;
    public ChooseChampionPanel ccp;
    public RectTransform rt;
    // Start is called before the first frame update
    void Start()
    {
        championImage = gameObject.transform.Find("CharBox").GetComponent<Image>();
        championName = gameObject.transform.Find("BoxName").GetComponent<Text>();
        lockImage = gameObject.transform.Find("Locked").gameObject ;
        borderImage = gameObject.transform.Find("BoxBorder").GetComponent<Image>();
        rt= GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetData(int ID, int championID)
    {
        print("Cha: "+championID+" save: "+(ccp.saveData.champion[championID] != null));
        this.ID = ID;
        this.championID = championID;
        this.championName.text = ccp.saveData.champion[championID].name.ToUpper();
        this.championImage.sprite = ccp.saveData.champion[championID].sprite;
        lockImage.SetActive(!(ccp.saveData.champion[championID].IsAvailable(ccp.saveData) && ccp.saveData.champion[championID].isBought));
    }
    public void ClickedOnMe()
    {
        ccp.BoxClicked(ID);
    }
    public void SetAsSelected(bool selected)
    {
        if (selected)
        {
            rt.localScale = new Vector3(1, 1,1);
            borderImage.enabled = true;

        }
        else
        {
            rt.localScale = new Vector3(0.81f, 0.81f,1);
            borderImage.enabled = false;
        }
    }
}
