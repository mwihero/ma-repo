﻿using UnityEngine;
using System.Collections;

public class ParticleUser : MonoBehaviour
{

    public float TimeDelayToReactivate = 3;
    public float duration = 3;
    public bool loop;
    public GameObject child;
    public GameObject parent;
    public bool active = false;
    public float timer;
    public bool free = true;
    public bool leave;
    void Start()
    {
        if (active) {
            
        }
    }
    private void Update()
    {
        transform.localPosition = new Vector3(0, 0, 0);
        
        if (!free)
        {
            if (child.transform.parent!=null)
            {
                child.transform.localPosition = new Vector3(0, 0, 0);
            }
            
            timer += Time.deltaTime;
            if (timer>duration)
            {
                free = true;
                child.SetActive(false);
                timer = 0;
            }
        }
    }
    public void Play()
    {
        if (loop)
        {
            InvokeRepeating("Reactivate", TimeDelayToReactivate, TimeDelayToReactivate);
        }
        else
        {
            Invoke("Reactivate", TimeDelayToReactivate);
        }
        timer = 0;
        free = false;
    }
    
    public void SetParticleUser(float delay, float duration, bool loop, bool leave)
    {
        TimeDelayToReactivate = delay;
        this.duration = duration;
        this.loop = loop;
        this.leave = leave;
    }
    public void SetParticleUser(float delay, float duration, bool loop, bool leave, GameObject child)
    {
        SetParticleUser(delay, duration, loop, leave);
        child.transform.parent = gameObject.transform;
        child.SetActive(false);
        this.child = child;
    }
    public void SetParticleUser(float delay, float duration, bool loop, bool leave, GameObject child, GameObject parent)
    {
        SetParticleUser(delay, duration, loop, leave, child);
        gameObject.transform.parent = parent.transform;
        this.parent = parent;
        print("SETTATO");
    }
    void Reactivate()
    {
        child.SetActive(false);
        child.SetActive(true);
        if (leave)
        {
            child.gameObject.transform.parent = null;
        }
    }
}