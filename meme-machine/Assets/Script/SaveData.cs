﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{

    public int ID { get; set; }
    public string username { get; set; }
    public string password { get; set; }
    public int birthYear { get; set; }
    public string sex { get; set; }
    public int level { get; set; }
    public List<int> levelMaxList;
    public int experience { get; set; }
    public int trophies { get; set; }
    public int coins { get; set; }
    public int gems { get; set; }
    public int leagueID;
    public List<League> league;
    public List<Champion> champion;
    public Rank rank;
    public List<OpponentUser> opponentUsers;
    public List<OpponentUser> orderedUsers;
    public NameGenerator nameGenerator;
    public class Match
    {
        public int ID { get; set; }
        public int leagueID { get; set; }
        public string date { get; set; }
    }

    public class MatchResults
    {
        public int matchID { get; set; }
        public int userID { get; set; }
        public int championID { get; set; }
        public int position { get; set; }
        public int gol { get; set; }
        public float duration { get; set; }
        public int trophies { get; set; }
        public int coins { get; set; }
        public int experience { get; set; }

    }
    [System.Serializable]
    public class Champion
    {
        public int ID;
        public string name;
        public string description;
        public string aAbilityDescription;
        public string bAbilityDescription;
        public string cAbilityDescription;
        bool isAvailable;
        public bool isBought;
        public int coinsPrice;
        public int gemsPrice;
        public Requirements requirements;
        public Sprite sprite;
        public Champion(int ID, string name, string description, string aAbilityDescription, string bAbilityDescription, string cAbilityDescription,
            Requirements requirements, bool isBought, int coinsPrice, int gemsPrice, Sprite sprite)
        {
            this.ID = ID;
            this.name = name;
            this.description = description;
            this.aAbilityDescription = aAbilityDescription;
            this.bAbilityDescription = bAbilityDescription;
            this.cAbilityDescription = cAbilityDescription;
            this.requirements = requirements;
            this.isBought = isBought;
            this.coinsPrice = coinsPrice;
            this.gemsPrice = gemsPrice;
            this.sprite = sprite;
        }
       
        public bool IsAvailable(SaveData saveData)
        {

            return requirements.IsPlayerReady(saveData);
        }

    }
    public class Car
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string passiveAbilityDescription { get; set; }
        public float speed { get; set; }
        public float bounce { get; set; }
        public int size { get; set; }
        public bool isAvailable { get; set; }
        public bool isBought { get; set; }
        public int coinsPrice { get; set; }
        public int gemsPrice { get; set; }
    }
    public class League
    {
        public int ID { get; set; }
        public string name { get; set; }
        public int startingPoints { get; set; }
        public int foreignersCoinsPrice { get; set; }
        public int victoryBonusCoins { get; set; }
        public int minCups;
        public int maxCups;
        public League(int id, string name, int startingPoints, int foreignersCoinsPrice, int victoryBonusCoins,int minCups,int maxCups)
        {
            this.ID = id;
            this.name = name;
            this.startingPoints = startingPoints;
            this.foreignersCoinsPrice = foreignersCoinsPrice;
            this.victoryBonusCoins = victoryBonusCoins;
            this.minCups = minCups;
            this.maxCups = maxCups;
        }

    }
    public class Rank
    {
        public int leagueID;
        public int rankID;
        public Rank (int leagueID){
            this.leagueID = leagueID;
            rankID = Random.Range(1000,10000);
            }
    }
    [System.Serializable]
    public class OpponentUser
    {
        public string name;
        public int trophies;
        int goalMades;
        int gameWins;
        int strategy;
        int abilityFrequency;
        int reactionSpeed;
        public int picID;
        public int ID;
        public OpponentUser(int id,string name, int trophies)
        {
            ID = id;
            this.name = name;
            this.trophies = trophies;
        }
    }
    public class Requirements
    {
        int minTrophies=0;
        int minLeague=0;
        int minLevel=0;
        int minGems=0;
        public Requirements(int minTrophies, int minLeague, int minLevel, int minGems)
        {
            this.minTrophies = minTrophies;
            this.minLeague = minLeague;
            this.minLeague = minLeague;
            this.minGems = minGems;
        }
        public Requirements( )
        {
             
        }
        public bool IsPlayerReady(SaveData saveData)
        {
            bool ready = true;
            if (saveData.trophies<minTrophies)
            {
                ready = false;
            }
            if (saveData.leagueID < minLeague)
            {
                ready = false;
            }
            if (saveData.level < minLevel)
            {
                ready = false;
            }
            if (saveData.gems < minGems)
            {
                ready = false;
            }
            return ready;
        }
        public string ToString()
        {
            string s = "";
            if (minTrophies>0)
            {
                s += " you to have at least "+minTrophies+ " trophies to be unlocked\n";
            }
            if (minLeague > 0)
            {
                s += " you to be at least in the " + (minLeague+1) + "° league to be unlocked\n";
            }
            if (minLevel > 0)
            {
                s += " you to be at least in the " +  minLevel  + "° level to be unlocked\n";
            }
            if (minGems > 0)
            {
                s += " you to have accumulated at least " + minGems + " gems to be unlocked\n";
            }
            return ("This item requires" +s).Trim();
        }
    }
    private void Awake()
    {
        //PlayerPrefs.SetInt("Champ3", 0);
        //PlayerPrefs.SetInt("Champ4", 0);
        //PlayerPrefs.SetInt("Champ1", 0);
        //PlayerPrefs.SetInt("Coins", 10000);
        if ((GameObject.Find("SaveData1") || GameObject.FindGameObjectsWithTag("SaveData").Length>1 )&& gameObject.name == "SaveData")
        {
            
                Destroy(gameObject);
            
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
        ID = 1;
        username = PlayerPrefs.GetString("username");
        levelMaxList = new List<int>();
        
        levelMaxList.Add(270);
        levelMaxList.Add(270);
        levelMaxList.Add(400);
        levelMaxList.Add(700);
        levelMaxList.Add(1000);
        levelMaxList.Add(1200);
        levelMaxList.Add(1500);
        levelMaxList.Add(1600);
        levelMaxList.Add(1700);
        levelMaxList.Add(1800);
        levelMaxList.Add(2000);
        for (int i = 11; i < 15; i++)
        {
            levelMaxList.Add(2500);
        }
        for (int i = 16; i < 20; i++)
        {
            levelMaxList.Add(3000);
        }
        for (int i = 20; i < 30; i++)
        {
            levelMaxList.Add(3500);
        }
        league = new List<League>();
        league.Add(new League(0, "Beginners", 25, 40, 50, -100, 199));
        league.Add(new League(1, "Sophomores League", 25, 50, 70, 200, 299));
        league.Add(new League(2, "Iron League", 30, 70, 100, 300, 499));
        league.Add(new League(3, "Pro League", 30, 100, 150, 500, 699));
        league.Add(new League(4, "Dank League", 35, 150, 250, 700, 1000));
        level = PlayerPrefs.GetInt("Level", 1);
        coins = PlayerPrefs.GetInt("Coins", 0);
        gems = PlayerPrefs.GetInt("Gems", 0);
        trophies = PlayerPrefs.GetInt("Trophies", 0);
        experience = PlayerPrefs.GetInt("Experience", 0);


        

        champion = new List<Champion>();
        champion.Add(new Champion(0, "Tramp", "This champion can be letal in attack and defense",
            "Tramp shoots 3 balls where he wants",
            "Tramp shoots a rocket to freeze a player",
            "Tramp builds a wall to defends his goal and mexicans will pay for it",
            new Requirements(),
            true,
            900,
            50, Resources.Load<Sprite>("Champions/C0/ProPic")));
        print("jj: " + champion[0].name);
        champion.Add(new Champion(1, "Shrok", "This champion can be very resistant",
            "Shrok pushes away every ball in his area by screaming",
            "Shrok catches the balls trapped in his mud and throws them where he wants",
            "Shrok gains 1 health point everytime he scores",
            new Requirements(0,0,2,0),
            PlayerPrefs.GetInt("Champ1",0)==1?true:false,
            1200,
            50, Resources.Load<Sprite>("Champions/C1/ProPic")));
        champion.Add(new Champion(2, "Skidaddle", "This champion can get you mad bruh fr",
            "Skidaddle doubles every ball he touches",
            "Skidaddle slows down every ball in his area",
            "Skidaddle builds a noodles wall to defends his goal",
            new Requirements(),
            true,
            1000,
            50, Resources.Load<Sprite>("Champions/C2/ProPic")));
        champion.Add(new Champion(3, "Thatos", "This champion can be letal in the end of the match",
            "Thatos gets a shield that protects him from spells and bounces balls harder",
            "Thatos halfs the balls coming his way",
            "Thatos creates a portal that teleports the balls went in his goal in other directions",
            new Requirements(0, 2, 0, 0),
            PlayerPrefs.GetInt("Champ3", 0) == 1 ? true : false,
            1200,
            50, Resources.Load<Sprite>("Champions/C3/ProPic")));
        champion.Add(new Champion(4, "Mitos", "This champion can do unexpected moves",
            "Mitos throws a ball with a curved trajectory",
            "Every ball that Mitos touches slow down every champ they touch",
            "Mitos makes invisible every ball he touches",
            new Requirements(),
            PlayerPrefs.GetInt("Champ4", 0) == 1 ? true : false,
            900,
            50, Resources.Load<Sprite>("Champions/C4/ProPic")));
        
    }
    // Start ipublic string username;s called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("Rank", 0);
        UpdateCurrentLeague();
        if (PlayerPrefs.GetInt("Rank", 0) == 0)
        {
            CreateRank();
            PlayerPrefs.SetInt("Rank", 1);
        }
        else
        {
            UpdateRank();
        }
    }
    public void CreateRank()
    {
        rank = new Rank(leagueID);
        PlayerPrefs.SetInt("RankID", rank.rankID);
        opponentUsers = new List<OpponentUser>();
        for (int i=0;i<20-1; i++)
        {
            opponentUsers.Add(new OpponentUser(i,nameGenerator.GenerateName(),Random.Range(league[leagueID].minCups, league[leagueID].maxCups)));
            opponentUsers[opponentUsers.Count - 1].picID = Random.Range(0, 6);
            PlayerPrefs.SetString("OUName"+i,opponentUsers[opponentUsers.Count-1].name);
            PlayerPrefs.SetInt("OUTrophies"+i,opponentUsers[opponentUsers.Count-1].trophies);
            PlayerPrefs.SetInt("OUPic"+i, opponentUsers[opponentUsers.Count - 1].picID);
            
        }
        opponentUsers.Add(new OpponentUser(-1, username, trophies));
        opponentUsers[opponentUsers.Count - 1].picID =  5;
    }
    public void UpdateRank()
    {
        rank = new Rank(leagueID);
        rank.rankID = PlayerPrefs.GetInt("RankID");
        opponentUsers = new List<OpponentUser>();
        for (int i = 0; i < 20 - 1; i++)
        {
            opponentUsers.Add(new OpponentUser(i, PlayerPrefs.GetString("OUName" + i), PlayerPrefs.GetInt("OUTrophies" + i)));
            opponentUsers[opponentUsers.Count - 1].picID = PlayerPrefs.GetInt("OUPic" + i);
        }
        opponentUsers.Add(new OpponentUser(-1, username, trophies));
        opponentUsers[opponentUsers.Count - 1].picID = 5;
        orderedUsers = new List<OpponentUser>();
    }
    public void BuyChampion(int n)
    {
        champion[n].isBought = true;
        PlayerPrefs.SetInt("Champ"+n,1);
        AddCoins(-champion[n].coinsPrice);
    }
    public void AddTrophies(int add)
    {
        trophies += add;
        PlayerPrefs.SetInt("Trophies",trophies);
    }
    public void AddCoins(int add)
    {
        coins += add;
        PlayerPrefs.SetInt("Coins", coins);
    }
    public void AddGems(int add)
    {
        gems += add;
        PlayerPrefs.SetInt("Gems", gems);
    }
    public bool AddExperience(int add)
    {
        bool up=false;
        experience += add;
        if (experience> levelMaxList[level])
        {
            experience -= levelMaxList[level];
            LevelUp();
            up = true;
        }
        PlayerPrefs.SetInt("Experience", experience);
        return up;
    }
    public void LevelUp()
    {
        level++;
        PlayerPrefs.SetInt("Level", level);
    }
    public int GetLeagueID(int trophies)
    {
        for (int i=0;i<league.Count;i++)
        {
            if (trophies>=league[i].minCups && trophies <= league[i].maxCups)
            {
                return i;
            }
        }
        if (trophies>1000)
        {
            return 4;
        }else if (trophies<0)
        {
            return 0;
        }
        return -1;
    }
    public void UpdateCurrentLeague()
    {
        leagueID = GetLeagueID(trophies);
        PlayerPrefs.SetInt("LeagueID", leagueID);
        if (rank!=null)
        {
            if (rank.leagueID != leagueID)
            {
                CreateRank();
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
