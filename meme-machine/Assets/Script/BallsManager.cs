﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class BallsManager : MonoBehaviour {

	public GameObject[] balls;
	public Ball[] ballsScript;
	public GameObject laPalla;
	public int nBalls=5;
	public int limitOfBallsInField = 5;
	public GameManager gameManager;
    public bool gotBalls = false;
    public GameObject ballPrefab;
    public bool isOnline;
	// Use this for initialization
	void Start () {
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        if (limitOfBallsInField>nBalls){
			limitOfBallsInField = nBalls;
		}
       
        if (!PhotonNetwork.IsMasterClient)
		{
			return;
		}



	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void CreateBalls(){
		//print ("crea palle");
		balls=new GameObject[nBalls];
		ballsScript=new Ball[nBalls];
		for(int i=0; i<nBalls;i++){
            //balls [i] = Instantiate(laPalla);
            if (isOnline)
            {
                
                balls[i] = PhotonNetwork.Instantiate("Ball", new Vector3(20, 0.5f, 20), laPalla.transform.rotation, 0);
            }
            else
            {
                print("ESCE PALLE");
                balls[i] = (GameObject)Instantiate(Resources.Load("Ball"), new Vector3(20, 0.5f, 20), laPalla.transform.rotation);
            }
			ballsScript[i] = balls[i].GetComponent<Ball>();
		}
        //print ("fatto haha");
        StartCoroutine(CheckIfAllBallsAreCreated());
    }
	public void GetAllBalls(){
		GameObject[] palle=GameObject.FindGameObjectsWithTag ("Ball");
		balls=new GameObject[nBalls];
		ballsScript=new Ball[nBalls];
		for(int i=0; i<nBalls;i++){
			//balls [i] = Instantiate(laPalla);
			balls [i] =palle[i];
			ballsScript[i] = balls[i].GetComponent<Ball>();
		}
        gotBalls = true;
    }
    private IEnumerator CheckIfAllBallsAreCreated()
    {

        for (int i = 0; i < nBalls; i++)
        {
            bool doesntExist = false;
            do
            {
                if (balls[i] == null)
                {
                    doesntExist = true;
                }
                yield return new WaitForSeconds(0.1f);
            } while (doesntExist);
        }
        if (isOnline) { 
        Hashtable ht = new Hashtable { { "BallsCreated", true } };
        PhotonNetwork.CurrentRoom.SetCustomProperties(ht);
        }
        gotBalls = true;
    }
        public int ReturnFreeBall(){
		int fb = -1;
		int i = 0;
		while(i<limitOfBallsInField){
			if(!ballsScript[i].alive){
				fb = i;
				i = limitOfBallsInField;
			}
			i++;
		}
		//print ("Free Ball:" + fb);
		return fb;
	}
    public int AliveBallsCounter()
    {
        int aliveBalls = 0;
        
        for (int i =0; i< ballsScript.Length;i++)
        {
            if (ballsScript[i].alive && !ballsScript[i].deathTimer)
            {
                aliveBalls++;
            }
        }
        return aliveBalls;
    }
    public int MovingBallsCounter()
    {
        int movingBalls = 0;

        for (int i = 0; i < ballsScript.Length; i++)
        {
            if (ballsScript[i].alive && !ballsScript[i].deathTimer && !ballsScript[i].isOwned)
            {
                movingBalls++;
            }
        }
        return movingBalls;
    }
    public void DisappearAll(bool dis)
    {
        for (int i=0;i<ballsScript.Length;i++)
        {
            ballsScript[i].Disappear(dis,false,false,0.5f);
        }
    }
    public void DeleteAllTexts()
    {
        for (int i = 0; i < ballsScript.Length; i++)
        {
            ballsScript[i].UpdateBallText("");
        }
    }
}
