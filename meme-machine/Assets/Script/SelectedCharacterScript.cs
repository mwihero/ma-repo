﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SelectedCharacterScript : MonoBehaviour
{
    public Image image;
    public Text name;
    public Text playerName;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Initialize(Sprite image, string name)
    {
        this.image.sprite=image;
        this.name.text = name;
    }
    public void SetPlayerName(string name)
    {
        this.playerName.text = name;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
