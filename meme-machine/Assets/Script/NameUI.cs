﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NameUI : MonoBehaviour {


	 
	public float worldOffset=2;
	public Slider playerHealthSlider;
    public Image filler;
	public Text playerNameText;
    public Text playerScoreText;
    public GameObject target;
	Vector3 targetPosition;
	public int ID;
	public GameManager gameManager;
	public bool isSet;
    public float maxHealth;
    public RectTransform rect;

    public GameObject slowedDown;
    public GameObject blocked;
    public GameObject blind;
    public GameObject noAbility;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		
		maxHealth =(float) gameManager.startingPoints;
		
	}
	
	// Update is called once per frame
	void Update()
	{


		// Reflect the Player Health
		if ( isSet && filler!=null) {
			//playerHealthSlider.value = gameManager.carPoints[ID];
            filler.fillAmount = (float)gameManager.carPoints[ID] / maxHealth;

        }
	}
	public void UpdateText(string text){
		playerNameText.text = text;
	}
    public void UpdateText(string name,string score)
    {
        playerNameText.text = name;
        playerScoreText.text = score;
    }
    public void SetThisShit(GameObject go,int id, string name){
		target = go;
		playerNameText.text = name;
		this.ID = id;
		isSet = true;
	}
	public void SetThisShit(GameObject go,Text text,int id, string name){
		target = go;
		playerNameText = text;
		playerNameText.text = name;
		this.ID = id;
		isSet = true;
	}
	void LateUpdate () {

 
		// #Critical
		// Follow the Target GameObject on screen.
		if (target!=null)
		{
			targetPosition = target.transform.position;
			targetPosition.y += worldOffset;

			this.transform.position = Camera.main.WorldToScreenPoint (targetPosition) ;
            float f = ((12-Vector3.Distance(Camera.main.transform.position,targetPosition))/12f)*0.6f+0.4f;
            if (rect!=null) {
                rect.localScale = new Vector3(f, f, 1);
            }
		}

	}
}
