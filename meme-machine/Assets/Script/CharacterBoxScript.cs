﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBoxScript : MonoBehaviour
{
    public Image image;
    public Text name;
    public Text speed;
    public Text dimension;
    public Text bounce;
    public int id;
    public PickCharacter2 pickCharacter2;
    public PickCharacter pickCharacter;
    public bool isOnline;
    // Start is called before the first frame update
    void Start()
    {
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        if (isOnline) {
            pickCharacter2 = GameObject.Find("PickPanel2").GetComponent<PickCharacter2>();
        }
        else
        {
            pickCharacter = GameObject.Find("PickPanel").GetComponent<PickCharacter>();
        }
    }
    public void Initialize(int id, Sprite image, string name)
    {
        this.id=id;
        this.image.sprite = image;
        this.name.text = name;
    }
    public void OnClick()
    {
        if (isOnline)
        {
            pickCharacter2.UpdateMySelectedCharacter(id);
        }
        else
        {
            pickCharacter.UpdateMySelectedCharacter(id);
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
