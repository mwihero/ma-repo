﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseChampionPanel : MonoBehaviour
{
    public Text championName;
    public Image championImage;
    public Text abilityDescription;
    public ChampionBox[] championBox;
    public AbilityBox[] abilityBox;
    public Text description;
    public SaveData saveData;
    public int selected = 0;
    public int selectedAbility = 0;
    public bool isOnLine;
    public int playerID;
    public int playersNumber;
    public LobbyScript lobbyScript;
    public Menu menu;
    public Alert alert;
    public Image firstAbilityImage;
    public Image secondAbilityImage;
    public Image thirdAbilityImage;
    private void Start()
    {
        isOnLine = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        if (isOnLine)
        {
            playerID = lobbyScript.GetPlayerID();
            playersNumber = lobbyScript.GetPlayersNumber();
            print("ao: " + playersNumber);
        }
        else
        {
            playersNumber = 1;
            playerID = 1;
        }

        
        StartCoroutine(WaitForSaveData());
    }
    public void UpdateMySelectedCharacter(int champId)
    {
        selected = champId;
        
        lobbyScript.SelectedNewChampion(champId);
        PlayerPrefs.SetInt("MyChampion", champId);
    }
    public void UpdateSelectedCharacter(int playerId, int champId)
    {
        if (champId > -1)
        {
            //selected = champId;
            
            PlayerPrefs.SetInt("Champion" + playerId, champId);
        }
        //lobbyScript.SelectedNewChampion(champId);
    }
    
    IEnumerator WaitForSaveData()
    {
        saveData = GameObject.FindWithTag("SaveData").GetComponent<SaveData>();
        while (saveData==null)
        {
            yield return new WaitForSeconds(0.3f);
        }
        UpdateData();
        for (int i = playersNumber + 1; i < 5; i++)
        {
            int random, trophies = 0;
            if (saveData.leagueID == 0)
            {


                random = (int)Random.Range(0, 2);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(0, 2);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(0, 3);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;


            }
            else if (saveData.leagueID == 1)
            {
                random = (int)Random.Range(1, 3);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(1, 3);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(1, 3);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }
            else if (saveData.leagueID == 2)
            {
                random = (int)Random.Range(2, 4);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(2, 4);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(1, 4);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }
            else if (saveData.leagueID == 3)
            {
                random = (int)Random.Range(2, 5);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(3, 5);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(2, 5);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }
            else if (saveData.leagueID == 4)
            {
                random = (int)Random.Range(4, 6);
                PlayerPrefs.SetInt("Bot" + i + " ReactionSpeed", random);
                trophies += random * 2;
                random = (int)Random.Range(4, 6);
                PlayerPrefs.SetInt("Bot" + i + " AbilityFrequency", random);
                trophies += random * 3;
                random = (int)Random.Range(3, 6);
                PlayerPrefs.SetInt("Bot" + i + " Strategy", random);
                trophies += random * 1;
            }

            trophies *= 30;

            trophies += (int)Random.Range(-15, 15);
            PlayerPrefs.SetInt("Bot" + i + " Trophies", trophies);

            UpdateSelectedCharacter(i, 0 + (int)(Random.Range(0, 5)));
            
        }

        BoxClicked(PlayerPrefs.GetInt("MyChampion"));
    }
    public void UpdateData()
    {
        for (int i = 0; i < championBox.Length; i++)
        {
            //print("data: "+saveData.champion[i].name);
            championBox[i].SetData(i, i);
        }
    }
    public void BoxClicked(int n)
    {
        selected = n;
        PlayerPrefs.SetInt("MyChampion", n);
        for (int i = 0; i < championBox.Length; i++)
        {
            championBox[i].SetAsSelected(n==i);
        }
        championName.text = saveData.champion[selected].name.ToUpper();
        championImage.sprite = saveData.champion[selected].sprite;

        firstAbilityImage.sprite = Resources.Load<Sprite>("Champions/C" + n + "/A");
        secondAbilityImage.sprite = Resources.Load<Sprite>("Champions/C" + n + "/B");
        thirdAbilityImage.sprite = Resources.Load<Sprite>("Champions/C" + n + "/C");

        AbilityClicked(-1);
    }
    public void AbilityClicked(int n)
    {
        selectedAbility = n;
        
        for (int i = 0; i < abilityBox.Length; i++)
        {
            abilityBox[i].SetAsSelected(n == i);
        }
        if (n==0) {

            abilityDescription.text = saveData.champion[selected].aAbilityDescription;
        }else if (n==1) {

            abilityDescription.text = saveData.champion[selected].bAbilityDescription;
        }else if (n==2) {

            abilityDescription.text = saveData.champion[selected].cAbilityDescription;
        }
        else
        {
            abilityDescription.text = saveData.champion[selected].description;
        }
        
        

    }
    public void OnStartMatchButtonClicked()
    {
        if (saveData.champion[selected].isBought)
        {
            menu.OnStartButtonClicked();
        }
        else
        {
            alert.ActivateAlert("Broke ass nigga you didn't pay for that champion", "Okay");
        }

    }
}
