﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

public class Car : MonoBehaviour {

	private PhotonView photonView;
	public Rigidbody rb;
	private float[] size;
	private int currentSize=3;
	// Use this for initialization
	void Awake (){
		photonView = GetComponent<PhotonView>();
		size = new float[6];
		size [0] = 0.1f;
		size [1] = 0.3f;
		size [2] = 0.5f;
		size [3] = 0.75f;
		size [4] = 1f;
		size [5] = 1.25f;
	}
	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody> ();
		rb.constraints = RigidbodyConstraints.None;
		rb.constraints = RigidbodyConstraints.FreezeRotation;
        SetSize(3);
	}



	Vector3 networkPosition= Vector3.zero;
	Quaternion networkRotation;
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext(rb.position);
			stream.SendNext(rb.rotation);
			stream.SendNext(rb.velocity);
		}
		else
		{
			networkPosition = (Vector3) stream.ReceiveNext();
			//networkRotation = (Quaternion) stream.ReceiveNext();
			rb.velocity = (Vector3) stream.ReceiveNext();

			float lag = Mathf.Abs((float) (PhotonNetwork.Time - info.timestamp));
			networkPosition += (rb.velocity * lag);
		}
	}
	public void FixedUpdate()
	{
		if (!photonView.IsMine)
		{
			rb.position = Vector3.MoveTowards(rb.position, networkPosition, Time.fixedDeltaTime);
			//rb.rotation = Quaternion.RotateTowards(rb.rotation, networkRotation, Time.fixedDeltaTime * 100.0f);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}

	public int GetSize (){
		return currentSize;
	}
	public void SetSize(int size){
		currentSize = size;
		transform.localScale = new Vector3 (this.size[currentSize],this.size[currentSize],this.size[currentSize]);
	}
}
