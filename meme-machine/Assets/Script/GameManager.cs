﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class GameManager : MonoBehaviourPunCallbacks, IPunObservable {
    [System.Serializable]
    public class Results
    {
        public int position;
        public string name;
        public int championID;
        public int score;
        public int points = -1;
        public int trophies=0;
        public int ID;
        public Results(int position, string name, int championID, int score, int id)
        {
            this.position = position;
            this.name = name;
            this.championID = championID;
            this.score = score;
            this.ID = id;
        }
        public Results(int position, string name, int championID, int score, int points, int id)
        {
            this.position = position;
            this.name = name;
            this.championID = championID;
            this.score = score;
            this.points = points;
            this.ID = id;
        }
    }
    public bool isOnline;
    public int[] ballSpawnerC;
    public int ballSpawnerTurn=0;
	public BallSpawner[] ballSpawner;
	public BallsManager ballsManager;
	public Timer spawnTimer;
	public Timer pointsboardTimer;
	public float spawnTimerDuration = 4f;
	public int startingPoints=30;
	public CarSystem[] carSystemScripts;
    public ChampionsList champions;
	public int[] carPoints;
	public int[] carScore;
	public string[] carNames;
	public int points;
	public Text[] carPointsText;
    public Text[] carHealthText;
    public Text[] carNamesText;
    public Image[] carImageSprite;
    public RectTransform[] carTab;
    public NameUI[] carNameUI;

    public GameObject[] laserBeams;

    public int allPlayersSkills=3;
    public Text allPlayersSkillsText;
    public bool isSync = false;
	public float countdownTime=-1;
	public float startTime=-1;
	public bool isDoingCountdown=false;
	public int time = -1;
	public bool isTimeCounting = false;
	public Text countdownText;
	public Text timeText;
	public int phasesNumber=3;
	public float phasesDuration=30;
	public float phaseStartingTime;
	public int phase=0;
    public int[] minBallsPerPhase = {1,1,2,3};
	public int[] limitOfBallsInField;
	public Vector2[] spawnTimerRange;
	public float spawnWait;
	public Vector4[] ballTypeChoice;
	public Text pingText;
	public int nPlayers = 0;
    public float gameTime;
    public List<Results> results;

    public GameObject victoryPanel;
    public GameObject levelUpPanel;

    public Text levelUpText;
    public Text levelUpCoinsText;
    public Text levelUpGemsText;
    //public Text levelUpText;

    public Text[] resultsNameText;
    public Image[] resultsChampionImage;
    public Text[] resultsScoreText;
    public Text[] resultsPointsText;
    public Image[] resultsLineImage;
    public Image resultsMyPersonalProPic;
    public Text resultsMyNicknameText;
    public Image resultsMyPositionImage;
    public Sprite[] resultsPositionIcons;
    public Color resultsSelectedColor;
    public Color resultsUnselectedColor;
    public Text resultsMyCups;
    public Text resultsMyCoins;
    public Text resultsMyExps;

    public GameObject keepWatchingButtonGO;
    public GameObject resultsPanel;
    public List<Results> chart;
    public int changePosID;
    public int changePosID2;
    public float changePosY;
    public float changePosDuration = 0.2f;
    public Timer changePos;

    public GameObject newsGO;
    public Image newsProPic;
    public Text newsText;
    public Text newsGainText;
    public Image newsIcon;
    public Color newsImportantColor;
    public Color newsMoreImportantColor;
    public Color newsFirstGoalColor;

    public bool firstGoal;

    int multiGoalPrize = 1;
    int firstGoalPrize = 2;
    public Text positionText;

    public PhoneMessage phoneMessage;
    public bool levelUp;

    public GameObject coffinMemeGO;
    public GameObject coffinMemeGO2;

    public SaveData saveData;
    public bool saved;
    // Use this for initialization
    public void Starta () {
		

		spawnTimer=new Timer (spawnTimerDuration);
		spawnTimer.StartTimer ();
		UpdateNamesboard ();
        changePos = new Timer(changePosDuration);
        for (int i=0;i<4;i++)
        {
            chart.Add(new Results(4 - results.Count, carNames[i], carSystemScripts[i].botScript.championID,carScore[i], carPoints[i],i));
        }
        
	}
    public void chartOrder()
    {
        bool b=false;
        if (!changePos.isTiming) {
            
            for (int i=0;i<3;i++)
            {
                if (chart[i].points < chart[i+1].points)
                {

                    changePosID = chart[i].ID;
                    changePosID2 = chart[i+1].ID;
                    changePosY = GetRectTransformFromChampID(changePosID).anchoredPosition.y;
                    changePos.StartTimer();

                    Results r = chart[i];
                    chart[i] = chart[i+1];
                    chart[i+1] = r;

                    b = true;
                    if (chart[i].ID == 0)
                    {
                        positionText.text = (i + 1) + "°";
                        positionText.gameObject.SetActive(false);
                        positionText.gameObject.SetActive(true);
                    }
                    else if (chart[i + 1].ID == 0)
                    {
                        positionText.text = (i + 2) + "°";
                        positionText.gameObject.SetActive(false);
                        positionText.gameObject.SetActive(true);
                    }
                    break;
                }
                
            }
            
        }
    }
    public RectTransform GetRectTransformFromChampID(int id)
    {
        for (int i=0;i<4; i++)
        {
            if (chart[i].ID==id)
            {
                return carTab[id];
            }
        }
        return null;
    }
    public void UpdateChartPoints(int id, int points)
    {
        for (int i =0; i<4; i++)
        {
            if (chart[i].ID==id)
            {
                chart[i].points = points;
                i = 4;
            }
        }
    }
    void Start () {
		Application.targetFrameRate = 3000;
		ballsManager = GetComponent<BallsManager> ();
		pointsboardTimer=new Timer (0.5f);
		pointsboardTimer.StartTimer ();
		pingText = GameObject.Find ("PingText").GetComponent<Text>();
		Destroy (GameObject.Find("Terreno"));
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        saveData = GameObject.FindWithTag("SaveData").GetComponent<SaveData>();
    }
	// Update is called once per frame
	void Update () {
		//pingText.text=PhotonNetwork.NetworkingPeer.RoundTripTime;
		//UpdatePointsboard ();
		UpdatePing();
        if (isOnline)
        {
            gameTime = (float)PhotonNetwork.Time;
        }
        else
        {
            gameTime = Time.time;
        }
		if(isDoingCountdown){
			 
			if (gameTime - countdownTime < 3f) {
				countdownText.text = "" + (3 - (int)(gameTime - countdownTime));
			} else if (gameTime - countdownTime < 4f) {
				countdownText.text = "GO!" ;
			} else {
				countdownText.text = "" ;
				isDoingCountdown = false;
                if (isOnline)
                {
                    if (PhotonNetwork.IsMasterClient)
                    {

                        Hashtable ht = new Hashtable { { "StartTime", (float)PhotonNetwork.Time } };
                        PhotonNetwork.CurrentRoom.SetCustomProperties(ht);
                        startTime = (float)PhotonNetwork.Time;
                        isTimeCounting = true;

                        spawnTimerDuration = Random.Range(spawnTimerRange[phase].x, spawnTimerRange[phase].y);
                        spawnTimer = new Timer(spawnTimerDuration);
                        spawnTimer.StartTimer();
                        CasualSpawn();
                    }
                }
                else
                {
                    startTime = gameTime;
                    isTimeCounting = true;
                    spawnTimerDuration = Random.Range(spawnTimerRange[phase].x, spawnTimerRange[phase].y);
                    spawnTimer = new Timer(spawnTimerDuration);
                    spawnTimer.StartTimer();
                    CasualSpawn();
                }
			}
		}
		if (isTimeCounting) {
			time = (int)(gameTime - startTime);
			timeText.text = "" + time;
		
			if (Input.GetKeyDown (KeyCode.Escape)) {
				Application.Quit ();
			}
			if (Input.GetKeyDown (KeyCode.G)) {
				if (Time.timeScale == 1f) {
					Time.timeScale = 0.1f;
				} else if (Time.timeScale == 5f) {
					Time.timeScale = 1f;
				} else if (Time.timeScale == 0.1f) {
					Time.timeScale = 5f;
				}


			}
			if (Input.GetKeyDown (KeyCode.B)) {
				CasualSpawn ();
			}
			if (pointsboardTimer.UpdateTimer () == 2) {
				pointsboardTimer.StartTimer ();
				UpdatePointsboard ();
                Application.targetFrameRate = 60;
            }

            if (changePos.UpdateTimer() == 2)
            {
                GetRectTransformFromChampID(changePosID).anchoredPosition = Vector2.Lerp(new Vector2(GetRectTransformFromChampID(changePosID).anchoredPosition.x, changePosY),
                    new Vector2(GetRectTransformFromChampID(changePosID).anchoredPosition.x, changePosY - GetRectTransformFromChampID(changePosID2).sizeDelta.y),
                    1);

                GetRectTransformFromChampID(changePosID2).anchoredPosition = Vector2.Lerp(new Vector2(GetRectTransformFromChampID(changePosID2).anchoredPosition.x, changePosY - ((GetRectTransformFromChampID(changePosID).sizeDelta.y / 2f) + (GetRectTransformFromChampID(changePosID2).sizeDelta.y / 2f))),
                    new Vector2(GetRectTransformFromChampID(changePosID2).anchoredPosition.x, changePosY - (GetRectTransformFromChampID(changePosID2).sizeDelta.y / 2f) + (GetRectTransformFromChampID(changePosID).sizeDelta.y / 2f)),
                    1);
            }
            else if (changePos.isTiming)
            {
                
                GetRectTransformFromChampID(changePosID).anchoredPosition = Vector2.Lerp(new Vector2(GetRectTransformFromChampID(changePosID).anchoredPosition.x,changePosY),
                    new Vector2(GetRectTransformFromChampID(changePosID).anchoredPosition.x, changePosY- GetRectTransformFromChampID(changePosID2).sizeDelta.y),
                    changePos.time/changePos.duration);

                GetRectTransformFromChampID(changePosID2).anchoredPosition = Vector2.Lerp(new Vector2(GetRectTransformFromChampID(changePosID2).anchoredPosition.x, changePosY- ((GetRectTransformFromChampID(changePosID).sizeDelta.y/2f) + (GetRectTransformFromChampID(changePosID2).sizeDelta.y / 2f))),
                    new Vector2(GetRectTransformFromChampID(changePosID2).anchoredPosition.x, changePosY -(GetRectTransformFromChampID(changePosID2).sizeDelta.y/2f)+ (GetRectTransformFromChampID(changePosID).sizeDelta.y / 2f) ),
                    changePos.time / changePos.duration);
            } 
             


			if(phase<phasesNumber && phasesDuration <= (float)time-phaseStartingTime){
				IncrementPhase ();
			}
            if (isOnline) {
                if (!PhotonNetwork.IsMasterClient) {
                    return;
                }
            }
			//print ("prima");
			if (isSync) {
				if (spawnTimer.UpdateTimer () == 2 || ballsManager.MovingBallsCounter()<minBallsPerPhase[phase]) {
					//print ("dopo");
					spawnTimerDuration = Random.Range (spawnTimerRange [phase].x, spawnTimerRange [phase].y);
					spawnTimer.duration = spawnTimerDuration;
					spawnTimer.StartTimer ();
					int type=0;
					float ae= Random.Range(0,ballTypeChoice[phase].x+ballTypeChoice[phase].y+ballTypeChoice[phase].z+ballTypeChoice[phase].w);
					if (ae < ballTypeChoice [phase].x) {

					} else {
						type++;
						if (ae < ballTypeChoice [phase].y+ballTypeChoice [phase].x && ae > ballTypeChoice [phase].x) {

						} else {
							type++;
							if (ae < ballTypeChoice [phase].z+ballTypeChoice [phase].y+ballTypeChoice [phase].x && ae > ballTypeChoice [phase].y+ballTypeChoice [phase].x) {

							} else {
								type++;
								if (ae < ballTypeChoice [phase].w+ballTypeChoice [phase].z+ballTypeChoice [phase].y+ballTypeChoice [phase].x && ae > ballTypeChoice [phase].z+ballTypeChoice [phase].y+ballTypeChoice [phase].x) {

								} else {
									type++;
								}
							}
						}
					}

					CasualSpawn (type);

				}

			}
            bool lastAlive = true;
            for (int i = 0; i < 4; i++)
            {

                if (carSystemScripts[i].alive)
                {
                    lastAlive = false;
                }
            }
            if (lastAlive)
            {
                Finished();
            }
        }
	}
    public void Finished()
    {
        keepWatchingButtonGO.SetActive(false);
        isTimeCounting = false;
        ballsManager.DeleteAllTexts();
    }
	int _cache=-1;
    public void SetAllPlayersSkillsTo(int skill)
    {
        if (skill>5)
        {
            skill = 0;
        }
        else if(skill<0)
        {
            skill = 5;
        }
        allPlayersSkills = skill;
        allPlayersSkillsText.text = "" + skill;
        for (int i=0;i<4;i++)
        {
            carSystemScripts[i].botScript.strategy = skill;
            carSystemScripts[i].botScript.reactionSpeed = skill;
            carSystemScripts[i].botScript.abilityFrequency = skill;
        }
    }
    public void IncrementAllPlayersSkills()
    {
        SetAllPlayersSkillsTo(allPlayersSkills+1);
    }
    public void DecrementAllPlayersSkills()
    {
        SetAllPlayersSkillsTo(allPlayersSkills-1);
    }
    public void SetMyPlayerAsBot()
    {
        carSystemScripts[0].botScript.isABot = !carSystemScripts[0].botScript.isABot;
    }
	public void UpdatePing(){
		if (PhotonNetwork.IsConnectedAndReady)
		{
			if (PhotonNetwork.GetPing() != _cache)
			{
				_cache = PhotonNetwork.GetPing();
				pingText.text = "Ping: "+_cache.ToString() + " ms";

			}
		}
		else
		{
			if (_cache != -1)
			{
				_cache = -1;
				pingText.text = "n/a";
			}
		}
	}
	public void IncrementPhase(){
		phase++;
		phaseStartingTime = (float)time;
		ballsManager.limitOfBallsInField=limitOfBallsInField[phase];
	}
	public void AssignPlayerToScoreboard(){
		GameObject[] we = GameObject.FindGameObjectsWithTag ("CarSystem");
		//carPoints = new int[carSystemScripts.Length];
		for(int i=0;i<carSystemScripts.Length;i++){
			//carSystemScripts[i].ID = i;
			carPoints[i] =startingPoints;
			carHealthText[i].text= ""+startingPoints;
            carPointsText[i].text = "0";
        }
	}

	public void CasualSpawn(){
		if (ballSpawner.Length >= 1) {
			ballSpawner [(int)Random.Range (0, ballSpawner.Length)].Spawn ();
		}
	}
	public void CasualSpawn(int type){
		if (ballSpawner.Length >= 1) {
            int i = (int)Random.Range(0, ballSpawner.Length);

            ballSpawner [ballSpawnerTurn].Spawn (type);
            ballSpawnerC[ballSpawnerTurn]++;
            ballSpawnerTurn++;
            if (ballSpawnerTurn==4)
            {
                ballSpawnerTurn = 0;
            }
		}
	}




	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
        //print ("entrrrrrrrr");
        /*
		if(PhotonNetwork.LocalPlayer.ActorNumber>0 && isSync){
			for(int i=1;i<5;i++){
				if (i == PhotonNetwork.LocalPlayer.ActorNumber) {
					stream.SendNext (carPoints [i-1]);
				} else {
					carPoints[i-1] = (int) stream.ReceiveNext();
				}
			}
		}

			if (stream.IsWriting) {
				/*
			if (PhotonNetwork.LocalPlayer.ActorNumber > 0 && isSync) {
				points = carPoints [PhotonNetwork.LocalPlayer.ActorNumber];

				carPoints = (int[])stream.ReceiveNext ();
				carPoints [PhotonNetwork.LocalPlayer.ActorNumber] = points;
				stream.SendNext (carPoints);
			}
				stream.SendNext (carPoints[0]);
				stream.SendNext (carPoints[1]);
				stream.SendNext (carPoints[2]);
				stream.SendNext (carPoints[3]);
			} else {
				carPoints[0] = (int)stream.ReceiveNext ();
				carPoints[1] = (int)stream.ReceiveNext ();
				carPoints[2] = (int)stream.ReceiveNext ();
				carPoints[3] = (int)stream.ReceiveNext ();
			}
		*/
        if (stream.IsWriting)
        {

           




        } else {
            
            
        }
}

    public void UpdatePointsboard(){
        /*
        foreach (Photon.Realtime.Player p in PhotonNetwork.PlayerList)
        {
            object thisPlayerPoints;
            if (p.CustomProperties.TryGetValue("Points", out thisPlayerPoints))
            {
                carPoints [p.ActorNumber-1] = (int)thisPlayerPoints;
                carPointsText [p.ActorNumber-1].text = "" + carPoints [p.ActorNumber-1];
            }
            if (p.CustomProperties.TryGetValue("Score", out thisPlayerPoints))
            {
                carScore [p.ActorNumber-1] = (int)thisPlayerPoints;
                carPointsText [p.ActorNumber-1].text += " : " + carScore [p.ActorNumber-1];
            }
        }*/
        /*
        int r= 4-nPlayers;
        if (r > 0) {
            for(int i=3; i>3-r;i--){
                if(carPoints [i]!=null){
                    carPoints [i] = carSystemScripts[i].points;
                    carScore [i] = carSystemScripts[i].score;
                    carPointsText [i].text = "" + carPoints [i]+" : "+carScore [i] ;	
                }
            }
        }*/
        if (carSystemScripts[0].name==null)
        {
            UpdateNamesboard();
        }
        
        for (int i = 0; i < 4; i++)
        {
            if (carPoints[i] != null)
            {
                //print("carro pointooo");
                carPoints[i] = carSystemScripts[i].points;
                carScore[i] = carSystemScripts[i].score;
                carPointsText[i].text = "" + carScore[i];
                carHealthText[i].text = "" + carPoints[i];
                UpdateChartPoints(i, carPoints[i]);
            }
        }
        chartOrder();
        for (int i=0;i<4;i++){

			if(carSystemScripts[i]!=null){
				carNameUI [i].UpdateText (carSystemScripts[i].name,""+carScore[i]);
			}
		}
	}
	public void UpdateNamesboard(){
		/*
		foreach (Photon.Realtime.Player p in PhotonNetwork.PlayerList)
		{
			
			carNames [p.ActorNumber-1] = (string)p.NickName;
			carNamesText [p.ActorNumber-1].text =carNames [p.ActorNumber-1];
			carNameUI [p.ActorNumber - 1].SetThisShit (carSystemScripts[p.ActorNumber - 1].car,p.ActorNumber - 1,p.NickName);

		}
		*/
		for(int i=0;i<4;i++){
			
			if(carSystemScripts[i]!=null){
                resultsMyNicknameText.text = "" + carSystemScripts[0].name.ToUpper();
                carNames [i] = carSystemScripts[i].name;
				carNamesText [i].text =carNames [i];
				carNameUI [i].SetThisShit (carSystemScripts[i].car,i,carSystemScripts[i].name);
                carSystemScripts[i].nameUI = carNameUI[i];
                carImageSprite[i].sprite = champions.champions[carSystemScripts[i].GetComponent<Bot>().championID].image;
			}
		}
       
        //resultsMyPersonalProPic =;
    }


	public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
	{
		print ("Room properties Updated");
		object propsTime;
		if (propertiesThatChanged.TryGetValue("StartTime", out propsTime))
		{
			startTime = (float) propsTime;
			isTimeCounting = true;
		}
		print ("Room properties Updated 2");
		if (propertiesThatChanged.TryGetValue("CountdownTime", out propsTime))
		{
			countdownTime = (float) propsTime;
			isDoingCountdown = true;
		}
        object ballsCreated;
        if (propertiesThatChanged.TryGetValue("BallsCreated", out ballsCreated))
        {
            //countdownTime = (bool)ballsCreated;
            if ((bool)ballsCreated) {
                isSync = true;
                ballsManager.GetAllBalls();
            }
        }
    }

    [System.Serializable]
    public class Timer{
		public bool isTiming=false;
		public float duration;
		public float time=0f;

		public Timer(float duration){
			this.duration=duration;
            isTiming = false;
        }
		public void StartTimer(){
			isTiming = true;
			time = 0f;
		}
		public int UpdateTimer (){
			int i = 0;
			if(isTiming){
				time += Time.deltaTime;
				i = 1;
				if(time>duration){
					i = 2;
					ResetTimer ();
				}
			}
			return i;
		}
		public void ResetTimer(){
			isTiming=false;
			time=0f;
		}
	}
  
    public void AddPlayerToResults(int id)
    {
        laserBeams[id].SetActive(true);
        results.Add(new Results(4-results.Count,carNames[id],carSystemScripts[id].botScript.championID, carScore[id],id));

        resultsChampionImage[4 - results.Count].sprite = Resources.Load<Sprite>("Champions/C"+ carSystemScripts[id].botScript.championID + "/ProPicAndBG");
        resultsNameText[4 - results.Count].text= results[results.Count-1].name.ToUpper();
        resultsScoreText[4 - results.Count].text = results[results.Count - 1].score + "";
        resultsPointsText[4 - results.Count].text = (results[results.Count - 1].points==-1)?"/": results[results.Count - 1].points+"";
        if (!isOnline)
        {
            if (id == 0)
            {
                resultsNameText[4 - results.Count].color = resultsSelectedColor;
                resultsScoreText[4 - results.Count].color = resultsSelectedColor;
                resultsPointsText[4 - results.Count].color = resultsSelectedColor;
                resultsLineImage[4 - results.Count].enabled = true;
                resultsMyPositionImage.sprite = resultsPositionIcons[4 - results.Count];
            }
            else
            {
                resultsNameText[4 - results.Count].color = resultsUnselectedColor;
                resultsScoreText[4 - results.Count].color = resultsUnselectedColor;
                resultsPointsText[4 - results.Count].color = resultsUnselectedColor;
                resultsLineImage[4 - results.Count].enabled = false;
            }
        }
        if (results.Count==4 || id == 0)
        {
            if (saved) {
                UpdateTrophies(false);
            }
            else
            {
                UpdateTrophies(true);
            }
        }
    }
    public void UpdateTrophies(bool save)
    {
        for (int i=0; i<4;i++)
        {
            int sum = 0;
            for (int j=0;j<4;j++)
            {
                if (i!=j)
                {
                    if (j>i)
                    {
                        int diff;
                        if (results.Count>j)
                        {
                            diff = carSystemScripts[results[i].ID].botScript.trophies - carSystemScripts[results[j].ID].botScript.trophies;

                        }
                        else
                        {
                            diff = carSystemScripts[results[i].ID].botScript.trophies - carSystemScripts[chart[3-j].ID].botScript.trophies;

                        }
                        if (diff>=91)
                        {
                            sum += -10;
                        }
                        else if(diff>=71 && diff <=90 )
                        {
                            sum += -9;
                        }
                        else if (diff >= 51 && diff <= 70)
                        {
                            sum += -8;
                        }
                        else if (diff >= 31 && diff <= 50)
                        {
                            sum += -7;
                        }
                        else if (diff >= 16 && diff <= 30)
                        {
                            sum += -6;
                        }
                        else if (diff >= 6 && diff <= 15)
                        {
                            sum += -5;
                        }
                        else if (diff >= -4 && diff <= 5)
                        {
                            sum += -4;
                        }
                        else if (diff >= -14 && diff <= -5)
                        {
                            sum += -3;
                        }
                        else if (diff >= -24 && diff <= -15)
                        {
                            sum += -2;
                        }
                        else if (diff <= -25)
                        {
                            sum += -1;
                        }
                    }
                    else
                    {
                        int diff = carSystemScripts[results[j].ID].botScript.trophies-carSystemScripts[results[i].ID].botScript.trophies ;
                        if (diff >= 91)
                        {
                            sum += 10;
                        }
                        else if (diff >= 71 && diff <= 90)
                        {
                            sum += 9;
                        }
                        else if (diff >= 51 && diff <= 70)
                        {
                            sum += 8;
                        }
                        else if (diff >= 31 && diff <= 50)
                        {
                            sum += 7;
                        }
                        else if (diff >= 16 && diff <= 30)
                        {
                            sum += 6;
                        }
                        else if (diff >= 6 && diff <= 15)
                        {
                            sum += 5;
                        }
                        else if (diff >= -4 && diff <= 5)
                        {
                            sum += 4;
                        }
                        else if (diff >= -14 && diff <= -5)
                        {
                            sum += 3;
                        }
                        else if (diff >= -24 && diff <= -15)
                        {
                            sum += 2;
                        }
                        else if (diff <= -25)
                        {
                            sum += 1;
                        }
                    }
                }
            }
            resultsPointsText[3-i].text = sum + "";
            if (results[i].ID==0 && save)
            {
                saved = true;
                if (saveData!=null)
                {
                    int coins = 0;
                    int exp = (i + 1) * 20 + 100 / (saveData.level);
                    levelUp= saveData.AddExperience(exp); 
                    if (i == 3)
                    {
                        coins += saveData.league[saveData.leagueID].victoryBonusCoins;
                        
                        if (sum<10)
                        {
                            sum = 10;
                        }
                        ActivatePanel(victoryPanel);
                    }
                    else
                    {
                        if (levelUp)
                        {
                            LevelUp();
                        }
                        else
                        {
                            ActivatePanel(resultsPanel);
                        }
                    }
                    coins += 30 + sum;
                    saveData.AddTrophies(sum);
                    saveData.AddCoins(coins);
                    
                    saveData.UpdateCurrentLeague();
                    resultsMyCups.text = "+" + sum;
                    resultsMyCoins.text = "+" + coins;
                    resultsMyExps.text = "+" + exp;
                    saveData.UpdateRank();
                    if (i==2)
                    {
                        int n = Random.Range(0,3);
                        if (n==0)
                        {
                            phoneMessage.ActivateMessage(0,"Dad","Why the f*ck are you crying so damn loud");
                        }
                         
                    }else if (i<2)
                    {
                        int n = Random.Range(0, 8);
                        if (n == 0)
                        {
                            phoneMessage.ActivateMessage(0, "Dad", "Why the f*ck are you crying so damn loud");
                        }
                        else if (n == 1)
                        {
                            phoneMessage.ActivateMessage(0, "FBI Agent", "Be careful next time bro");
                        }
                        else if (n == 2)
                        {
                            phoneMessage.ActivateMessage(0, "Queen Elizabeth II", "I lived more than you lol");
                        }
                        else if (n == 3)
                        {
                            phoneMessage.ActivateMessage(0, "FBI Agent", "F");
                        }
                    }
                    else if (i==3)
                    {
                        int n = Random.Range(0, 8);
                        if (n == 0)
                        {
                            phoneMessage.ActivateMessage(0, "Dad", "You are adopted");
                        }else if (n==1)
                        {
                            phoneMessage.ActivateMessage(0, "E-Sports official", "You should go pro");
                        }else if (n==2)
                        {
                            phoneMessage.ActivateMessage(0, "Crush <3", "I meant i love you as a brother...");
                        }else if (n==3)
                        {
                            phoneMessage.ActivateMessage(0, "Crush <3", "I think we should be friend");
                        }
                    }
                    PlayerPrefs.SetInt("PlayedFirstGame", 1);
                }
            }
        }
    }
    public void ActivatePanel(GameObject panel)
    {
        resultsPanel.active =(resultsPanel==panel);
        victoryPanel.active =(victoryPanel==panel);
        levelUpPanel.active =(levelUpPanel== panel);

    }
    public void OnCloseVictoryClicked()
    {
        if (levelUp)
        {
            LevelUp();
        }
        else
        {
            ActivatePanel(resultsPanel);
        }
    }
    public void LevelUp()
    {
        levelUpCoinsText.text = "+" + 100;
        levelUpGemsText.text = "+" + 50;
        levelUpText.text = "" + saveData.level;
        saveData.AddCoins(100);
        saveData.AddGems(50);
        ActivatePanel(levelUpPanel);
    }
    public void ActivateNews(CarSystem cs,int type,int n)
    {
        ChangeNewsColor(newsImportantColor);
        newsGainText.gameObject.SetActive(false);
        if (type==0)
        {
            //newsProPic.sprite = Resources.Load<Sprite>("Champions/C" + carSystemScripts[0].botScript.championID + "/ProPicAndBG");
            newsProPic.sprite = carImageSprite[0].sprite;
            newsText.text = "GOAL!";
        }
        if (type==1)
        {
            if (cs.ID==0)
            {
                ChangeNewsColor(new Color(1,0.5f,0));
            }
            ChangeNewsColor(new Color(1, 0.5f, 0));
            newsProPic.sprite = carImageSprite[cs.ID].sprite;
            newsText.text = (cs.ID == 0) ? "": carNames[cs.ID] + "\n" ;
            switch (n)
            {
                case 2:
                    newsText.text += "DOUBLE GOAL!";
                    break;
                case 3:
                    newsText.text += "TRIPLE GOAL!";
                    break;
                case 4:
                    newsText.text += "QUADRA GOAL!";
                    break;
                case 5:
                    newsText.text += "PENTA GOAL!";
                    break;
                default:
                    newsText.text += "MULTI GOAL!";
                    break;
            }
            print("SINGLE: " + newsText.text +"\nSEC: "+time);
        }
        if (type==2)
        {
            if (cs.ID == 0)
            {
                ChangeNewsColor(newsMoreImportantColor);
            }
            else
            {
                ChangeNewsColor(Color.green);
                ChangeNewsColor(Color.white);
            }
            
            newsProPic.sprite = carImageSprite[cs.ID].sprite;
            newsText.text = (cs.ID == 0) ? "" : carNames[cs.ID] + "\n";
            newsGainText.gameObject.SetActive(true);
            newsGainText.text = ""+ multiGoalPrize;
            cs.AddPoints(multiGoalPrize);
            switch (n)
            {
                case 2:
                    newsText.text += "DOUBLE GOAL!";
                    break;
                case 3:
                    newsText.text += "TRIPLE GOAL!";
                    break;
                case 4:
                    newsText.text += "QUADRA GOAL!";
                    break;
                case 5:
                    newsText.text += "PENTA GOAL!";
                    break;
                default:
                    newsText.text += "MULTI GOAL!";
                    break;
            }
            print("OPPONENT: "+ newsText.text + "\nSEC: " + time);
        }
        else if (type == 3)
        {
            ChangeNewsColor(newsFirstGoalColor);
            newsProPic.sprite = carImageSprite[cs.ID].sprite;
            newsText.text = (cs.ID == 0) ? "" : carNames[cs.ID] + "\n";
            newsGainText.gameObject.SetActive(true);
            newsGainText.text = "" + firstGoalPrize;
            cs.AddPoints(firstGoalPrize);
            
            newsText.text += "FIRST GOAL!";
                    
        }
            newsGO.active = false;
        newsGO.active = true;
    }
    public void ChangeNewsColor(Color c)
    {
        newsIcon.color = c;
        newsText.color = c;
        //newsGainText.color = c;
    }
    public void OnCloseLevelUpClicked()
    {
        
            ActivatePanel(resultsPanel);
       
    }
	public void DecrementPoints(int ID){
		if (PhotonNetwork.IsMasterClient || !isOnline) {
			carPoints [ID]--;
			carHealthText [ID].text = "" + carPoints [ID];
		}
	}
    public void BackToMenu()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Menu");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
