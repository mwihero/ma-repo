﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class Ability : MonoBehaviour {
	public string type="";
	public string ability="";
	public CarSystem carSystem;
	public Bot bot;
	public GameObject abilityButton;
    public Image image;
	public bool isUnlocked=false;
	public bool isAvailable=false;
	public bool isUsing=false;
	public int unlockScore;
	public int stato = 0;

	public bool isCoolDown;
	public float coolDownDuration=9f;
	public float coolDownTime=0f;

	public Vector3 target;

	private PhotonView photonView;
    public bool isOnline;
    public bool isShowing;
	// Use this for initialization
	void Start () {
		bot = GetComponent<Bot> ();
		photonView = GetComponent<PhotonView> ();
		carSystem=GetComponent<CarSystem> ();

        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        isShowing = false;
        if (!bot.isABot)
        {
            if (!isOnline || photonView.IsMine)
            {
                isShowing = true;
            }
        }
    }
	
	// Update is called once per frame
	public void AbilityUpdate () {
        if (image==null)
        {
            if (abilityButton!=null)
            {
                image = abilityButton.transform.GetChild(0).GetComponent<Image>();
            }
        }
		if (!isUnlocked) {
			if(carSystem!=null){
				if (carSystem.score >= unlockScore) {
					print ("Unlocked first ability");
					isAvailable = true;
					isUnlocked = true;
					if (isShowing) {
                        
                            abilityButton.transform.Find("Text").gameObject.GetComponent<Text>().text = type;
                            ChangeButtonColor(Color.white);
                        
					}
				} else {
					if (isShowing) {
                        
                            abilityButton.transform.Find("Text").gameObject.GetComponent<Text>().text = "" + (unlockScore - carSystem.score);
                        
					}
				}
			}
		} else {
			if(isCoolDown){
				coolDownTime += Time.deltaTime;
				if (coolDownTime >= coolDownDuration) {
					isCoolDown = false;
					isAvailable = true;
					coolDownTime = 0;
					if (isShowing) {
						//abilityButton.GetComponent<Image> ().fillAmount = 1;
                        image.fillAmount = 1;
                    }
				} else {
					if(isShowing){
						//abilityButton.GetComponent<Image> ().fillAmount = coolDownTime / coolDownDuration;
                        image.fillAmount = coolDownTime / coolDownDuration;
                    }
				}
			}
			if (isAvailable) {
				if(bot.isABot){

				}
			} else {

			}
		}



	}
	public void ChangeButtonColor(Color c){
        if (isShowing)
        {
            ColorBlock cb = abilityButton.GetComponent<Button>().colors;
            cb.normalColor = c;
            cb.highlightedColor = c;
            abilityButton.GetComponent<Button>().colors = cb;
            image.color = c;
        }
	}
	public void Cooldown  (){//Lancio 3 palle
		isCoolDown=true;
		coolDownTime = 0;
		isAvailable = false;
		if (isShowing) {
			ChangeButtonColor (Color.white);
		}
	}
}
