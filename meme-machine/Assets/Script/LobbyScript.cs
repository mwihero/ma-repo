﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using com.EYS.memesarena;
public class LobbyScript : MonoBehaviourPunCallbacks {
	public Text connectionStatus;
	public InputField playerNameInput;
    public Text status;
	public GameObject loginPanel;
	public GameObject roomPanel;
    public GameObject roomPanel2;
    public GameObject pickPanel2;
    public GameObject mainPanel;
    public GameObject PlayerListEntryPrefab;
	public GameObject StartGameButton;
	public string name;
	private Dictionary<int, GameObject> playerListEntries;
	public bool isServer = false;
    public PickCharacter pickCharacter;
    public Menu menu;
    bool joined = false;
    // Use this for initialization
    void Start () {
		//isServer = true;
		if (!isServer) {
			playerNameInput.text = PlayerPrefs.GetString ("Player", "Player " + Random.Range (1000, 10000));
		} else {
            playerNameInput.text = PlayerPrefs.GetString("Player", "Server " + Random.Range(1000, 10000));
            //SetActivePanel("RoomPanel2");
            OnLoginButtonClicked ();
		}
        
	}
	void Awake(){
		// Sincronizza le scene per far sì che al cambio scena, tutti i client cambino scena
		PhotonNetwork.AutomaticallySyncScene = true;

	}
	// Update is called once per frame
	void Update () {
		connectionStatus.text = "Connection Status: " + PhotonNetwork.NetworkClientState +"\n"+"Rooms: "+PhotonNetwork.CountOfRooms+"\nPlayers: "+PhotonNetwork.CountOfPlayers;

		 
	}

        public int GetPlayerID()
    {
        return (int)PhotonNetwork.LocalPlayer.ActorNumber;
    }
    public int GetPlayersNumber()
    {
        return (int)PhotonNetwork.CurrentRoom.PlayerCount;
    }
    public void OnLoginButtonClicked()//quando clicca il bottone del login
	{
		

		if (PlayerPrefs.GetString("Player")!="")//se c'è scritto il nome dell'utente
		{
			//PlayerPrefs.SetString ("Player",playerName1);
			
		}
		else
		{
            PlayerPrefs.SetString("Player", "Player"+Random.Range(100,999));
        }
        PhotonNetwork.LocalPlayer.NickName = PlayerPrefs.GetString("Player");//assegna il nick all'utente
        PhotonNetwork.ConnectUsingSettings();//e si connette
    }

	public override void OnConnectedToMaster()//quando si connette
	{
        print("connected");
        status.text = "connected";
        //this.SetActivePanel(SelectionPanel.name);
        if (!isServer)
        {
            //StartCoroutine(wo());
            PhotonNetwork.JoinRandomRoom();//cerca direttamente una room disponibile
            print("searching");
            status.text = "searching";
        }
        else
        {
            CreateRoom();
        }
        
	}
    IEnumerator wo()
    {
        print("searching");
        status.text = "searching";
        PhotonNetwork.JoinRandomRoom();//cerca direttamente una room disponibile
        yield return new WaitForSeconds(5);
        
    }
    public void CreateRoom()
    {
        string roomName = PlayerPrefs.GetString("Player");

        RoomOptions options = new RoomOptions { MaxPlayers = 5, CleanupCacheOnLeave = false };

        PhotonNetwork.CreateRoom(roomName, options, null); // ne crea una
        print("creating room");
        status.text = "creating room";
    }
	public override void OnJoinRandomFailed(short returnCode, string message) //se non esistono room
	{
        status.text = "failed join room";
        print ("Failed Join Room");
        StartCoroutine(WaitToJoinRoom());
        /*
        string roomName = PlayerPrefs.GetString("Player");

        RoomOptions options = new RoomOptions { MaxPlayers = 4, CleanupCacheOnLeave = false };

        PhotonNetwork.CreateRoom(roomName, options, null); // ne crea una
        */
    }
    IEnumerator WaitToJoinRoom()
    {
        yield return new WaitForSeconds(3);
        PhotonNetwork.JoinRandomRoom();//cerca direttamente una room disponibile
    }

	public void LocalPlayerPropertiesUpdated()
	{
		//StartGameButton.gameObject.SetActive(CheckPlayersReady());
		StartGameButton.gameObject.SetActive(true);
	}
	/// <summary>
	/// Called when a remote player entered the room. This Player is already added to the playerlist.
	/// </summary>
	/// <remarks>If your game starts with a certain number of players, this callback can be useful to check the
	/// Room.playerCount and find out if you can start.</remarks>
	/// <param name="newPlayer">New player.</param>
	public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)//AVVIATO ALL'ENTRATA DI UN ALTRO PLAYER NELLA ROOM
	{
		//CREA L'OGGETTO CHE VEDRAI IN CANVAS PER MOSTRARE IL PLAYER ENTRATO
		GameObject entry = Instantiate(PlayerListEntryPrefab);
		entry.transform.SetParent(roomPanel2.transform);
		entry.transform.localScale = Vector3.one;
		entry.transform.localPosition = new Vector3(entry.transform.localPosition.x,entry.transform.localPosition.y,0f);
		entry.GetComponent<PlayerListEntry>().Initialize(newPlayer.ActorNumber-1, newPlayer.NickName);

		playerListEntries.Add(newPlayer.ActorNumber-1, entry);

		StartGameButton.gameObject.SetActive(CheckPlayersReady());
        
	}
    
	/// <summary>
	/// Called when the LoadBalancingClient entered a room, no matter if this client created it or simply joined.
	/// </summary>
	/// <remarks>When this is called, you can access the existing players in Room.Players, their custom properties and Room.CustomProperties.
	/// 
	/// In this callback, you could create player objects. For example in Unity, instantiate a prefab for the player.
	/// 
	/// If you want a match to be started "actively", enable the user to signal "ready" (using OpRaiseEvent or a Custom Property).</remarks>
	public override void OnJoinedRoom()//AVVIATO ALL'ENTRATA DEL TUO PLAYER IN ROOM
	{
        joined = true;
        print("joined");
        status.text = "joined";
        SetActivePanel(roomPanel2.name);//ATTIVA IL PANNELLO CHE MOSTRA I PLAYER IN ROOM

		if (playerListEntries == null)
		{
			playerListEntries = new Dictionary<int, GameObject>();//CREA LISTA PLAYER IN ROOM
		}

		//CREA IL PANNELLO CHE MOSTRA I PLAYER
		foreach (Photon.Realtime.Player p in PhotonNetwork.PlayerList)
		{
            if (p.ActorNumber!=PhotonNetwork.MasterClient.ActorNumber) {
                GameObject entry = Instantiate(PlayerListEntryPrefab);
                entry.transform.SetParent(roomPanel2.transform);
                entry.transform.localScale = Vector3.one;
                entry.transform.localPosition = new Vector3(entry.transform.localPosition.x, entry.transform.localPosition.y, 0f);
                entry.GetComponent<PlayerListEntry>().Initialize(p.ActorNumber-1, p.NickName);
                print("AO: " + p.ActorNumber + ", " + p.NickName);
                object isPlayerReady;
                if (p.CustomProperties.TryGetValue("IsPlayerReady", out isPlayerReady))
                {
                    entry.GetComponent<PlayerListEntry>().SetPlayerReady((bool)isPlayerReady);
                }

                playerListEntries.Add(p.ActorNumber-1, entry);
            }
		}

		StartGameButton.gameObject.SetActive(CheckPlayersReady());// MOSTRA IL BOTTONE PER COMINCIARE LA PARTITA

		//AGGIORNA LE PROPRIETA' DEL PLAYER
		ExitGames.Client.Photon.Hashtable props = new ExitGames.Client.Photon.Hashtable
		{
			{"PlayerLoadedLevel", false},
            {"Champion", -1}
        };
		PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        /*
        if (PhotonNetwork.IsMasterClient)
        {
            ExitGames.Client.Photon.Hashtable props1 = new ExitGames.Client.Photon.Hashtable() { { "IsPlayerReady", true } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props1);
        }
        */
        print("entered room");
        status.text = "entered room";
    }

	public override void OnMasterClientSwitched(Photon.Realtime.Player newMasterClient)
	{
		if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
		{
			StartGameButton.gameObject.SetActive(CheckPlayersReady());
		}
	}

	public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
	{
		Destroy(playerListEntries[otherPlayer.ActorNumber].gameObject);
		playerListEntries.Remove(otherPlayer.ActorNumber);

		StartGameButton.gameObject.SetActive(CheckPlayersReady());
	}

	public override void OnPlayerPropertiesUpdate(Photon.Realtime.Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
	{
		if (playerListEntries == null)
		{
			playerListEntries = new Dictionary<int, GameObject>();
		}

		GameObject entry;
		if (playerListEntries.TryGetValue(targetPlayer.ActorNumber, out entry))
		{
			object isPlayerReady;
			if (changedProps.TryGetValue("IsPlayerReady", out isPlayerReady))
			{
				entry.GetComponent<PlayerListEntry>().SetPlayerReady((bool) isPlayerReady);
			}
            object champion;
            if (changedProps.TryGetValue("Champion", out champion))
            {
                pickCharacter.UpdateSelectedCharacter(targetPlayer.ActorNumber,(int) champion);
            }
        }

		StartGameButton.gameObject.SetActive(CheckPlayersReady());
	}

	public void OnStartGameButtonClicked()
	{
		PhotonNetwork.CurrentRoom.IsOpen = false;
		PhotonNetwork.CurrentRoom.IsVisible = false;
		//PhotonNetwork.isMessageQueueRunning = false;
		PhotonNetwork.LoadLevel("OnlineGameScene");
	}
    public void SelectedNewChampion(int id)
    {

        ExitGames.Client.Photon.Hashtable props = new ExitGames.Client.Photon.Hashtable
        {
            {"Champion", id}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);

    }
    private bool CheckPlayersReadyr()
	{
		
		if (!PhotonNetwork.IsMasterClient)
		{
			return false;
		}

		foreach (Photon.Realtime.Player p in PhotonNetwork.PlayerList)
		{
			object isPlayerReady;
			if (p.CustomProperties.TryGetValue("IsPlayerReady", out isPlayerReady))
			{
				if (!(bool) isPlayerReady)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		return true;
	}

	private bool CheckPlayersReady()
	{
        bool b = true;
		if (!PhotonNetwork.IsMasterClient && GetPlayersNumber()<2)
		{
            print("pl count");
			return false;
		}

		foreach (Photon.Realtime.Player p in PhotonNetwork.PlayerList)
		{
			object isPlayerReady;
            if (p.ActorNumber != PhotonNetwork.LocalPlayer.ActorNumber) {
                if (p.CustomProperties.TryGetValue("IsPlayerReady", out isPlayerReady))
                {
                    if (!(bool)isPlayerReady)
                    {

                        b = false;

                        print("actor: " + p.ActorNumber);
                    }
                }
                else
                {
                    print("no check");
                    return false;
                }
            } 
			
		}
        if (b && GetPlayersNumber()>1)
        {
            //menu.OnGoPickButtonClicked();
            OnStartGameButtonClicked();
            print("go to match");
        }
        return true;
	}

	private void SetActivePanel(string activePanel)
	{
		loginPanel.SetActive(activePanel.Equals(loginPanel.name));
		roomPanel.SetActive(activePanel.Equals(roomPanel.name));
        roomPanel2.SetActive(activePanel.Equals(roomPanel2.name));
        pickPanel2.SetActive(activePanel.Equals(pickPanel2.name));
        mainPanel.SetActive(activePanel.Equals(mainPanel.name));
    }
}

