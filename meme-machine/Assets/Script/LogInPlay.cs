﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.SceneManagement;
public class LogInPlay : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
		#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
           
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        
        PlayGamesPlatform.DebugLogEnabled = true;
        
        PlayGamesPlatform.Activate();
		#else
		SceneManager.LoadScene("LobbyScene");
		#endif
    }
	//mostra la finestra di consenso e logga l'utente in google play services
	public void OnLoginButtonClicked()
    {
        
        Social.localUser.Authenticate((bool success) => {
			if (success)
			{
				Debug.Log("Authentication successful");
				string userInfo = "Username: " + Social.localUser.userName +
					"\nUser ID: " + Social.localUser.id +
					"\nIsUnderage: " + Social.localUser.underage;
				Debug.Log(userInfo);
				SceneManager.LoadScene("LobbyScene");
			}else{
				SceneManager.LoadScene("LobbyScene");
			}
        });
        print(Social.localUser.authenticated);
        print(Social.localUser.userName);

    }
    // funzione per registrare un achievement normale
    public void RegisterAchievement(string achievementID, double progress)
    {

        Social.ReportProgress(achievementID, progress, null);
    }

    //funzione per registrare un achievement incrementale
    public void RegisterAchievement(string achievementID, int steps)
    {
		#if UNITY_ANDROID
        PlayGamesPlatform.Instance.IncrementAchievement(achievementID, steps, null);
		#endif
    }
}
