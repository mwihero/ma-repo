﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityBox : MonoBehaviour
{
    public Image image;
    public RectTransform rt;
    public bool isSelected;
    public ChooseChampionPanel chooseChampionPanel;
    public int ID;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ClickedOnMe()
    {
        chooseChampionPanel.AbilityClicked(ID);
    }
    public void SetAsSelected(bool isSelected)
    {
        this.isSelected = isSelected;
        
        if (isSelected)
        {
            rt.localScale = new Vector2(1.3f, 1.3f);
        }
        else
        {
            rt.localScale = new Vector2(1f, 1f);
        }
    }
}
