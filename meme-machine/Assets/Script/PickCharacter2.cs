﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using com.EYS.memesarena;
public class PickCharacter2 : MonoBehaviour
{
    // Start is called before the first frame update
    [System.Serializable]
    public class Character
    {
        public int id;
        public Sprite image;
        public string name = "";
        public float speed;
        public int dimension;
        public float bounce;


    }
    public SelectedCharacterScript[] selectedCharacterScript;
    public Character[] characters;
    public GameObject characterBox;
    public GameObject characterPanel;
    public int selected = 0;
    public bool isOnLine;
    public int playerID;
    public int playersNumber;
    public LobbyScript lobbyScript;
    void Start()
    {
        isOnLine = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        /*if (isOnLine)
        {
            playerID = lobbyScript.GetPlayerID();
            playersNumber = lobbyScript.GetPlayersNumber();
            print("ao: " + playersNumber);
        }
        else
        {
            playerID = 1;
        }*/
        playerID = 1;
        foreach (Character c in characters)
        {
            GameObject g = Instantiate(characterBox);
            g.transform.SetParent(characterPanel.transform);
            g.transform.localScale = Vector3.one;
            g.transform.localPosition = new Vector3(g.transform.localPosition.x, g.transform.localPosition.y, 0f);
            g.GetComponent<CharacterBoxScript>().Initialize(c.id, c.image, c.name);
        }
        
        /*
         * for (int i = 1;i< playersNumber; i++)
        {
            SetSelectedCharacterName(i, "Bot" + i);
        }*/
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void UpdateMySelectedCharacter(int champId)
    {
        selected = champId;
        selectedCharacterScript[0].Initialize(characters[champId].image, characters[champId].name);
        //lobbyScript.SelectedNewChampion(champId);
        PlayerPrefs.SetInt("MyChampion", champId);
    }
    public void UpdateSelectedCharacter(int playerId, int champId)
    {
        if (champId > -1)
        {
            //selected = champId;
            selectedCharacterScript[playerId - 1].Initialize(characters[champId].image, characters[champId].name);
            PlayerPrefs.SetInt("Champion" + playerId, champId);
        }
        //lobbyScript.SelectedNewChampion(champId);
    }
    public void SetSelectedCharacterName(int playerId, string name)
    {
        selectedCharacterScript[playerId - 1].SetPlayerName(name);
    }
}
