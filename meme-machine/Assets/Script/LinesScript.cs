﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinesScript : MonoBehaviour
{
    public LineRenderer line;
    public float MaxLength;
    public GameObject flash1, flash2;
    public float MainTextureLength = 1f;
    public float NoiseTextureLength = 1f;
    public bool stretch;
    private Vector4 Length = new Vector4(1, 1, 1, 1);
    public GameObject start;
    public GameObject finish;
    bool isSet=false;
    public GameManager.Timer timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isSet) {
            if (timer.UpdateTimer()==2)
            {

            }
            if (start!=null && finish!=null && timer.isTiming) {
                line.material.SetTextureScale("_MainTex", new Vector2(Length[0], Length[1]));
                line.material.SetTextureScale("_Noise", new Vector2(Length[2], Length[3]));
                line.SetPosition(0, start.transform.position);
                line.SetPosition(1, finish.transform.position);

                flash1.transform.position = start.transform.position;
                flash2.transform.position = finish.transform.position;
                flash1.transform.LookAt(finish.transform.position);
                //flash1.transform.eulerAngles += new Vector3(0,180,0);
                flash2.transform.LookAt(start.transform.position);
                flash2.transform.eulerAngles += new Vector3(0, 180, 0);
                if (stretch) {
                    Length[0] = MainTextureLength * (Vector3.Distance(start.transform.position, finish.transform.position));
                    Length[2] = NoiseTextureLength * (Vector3.Distance(start.transform.position, finish.transform.position));
                }
            }
            else
            {
                print("DestryLine");
                print("2Line " + start.name + " " + finish.name + " " + " " + timer.isTiming);
                Destroy(gameObject);
            }
        }

    }
    public void SetLine(GameObject start, GameObject finish)
    {
        isSet = true;
        this.start = start;
        this.finish = finish;

    }
    public void SetLine(GameObject start, GameObject finish,float duration)
    {
        print("Line");
        SetLine(start,finish);
      
        timer = new GameManager.Timer(duration);
        timer.StartTimer();
        print("Line " + start.name + " " + finish.name + " " + " " + timer.isTiming);
    }
}
