﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPanel : MonoBehaviour
{
    public GameObject champBoxPrefab;
    public SaveData saveData;
    public GameObject contentGO;
    public int selected;
    public List<ShopChampBox> shopChampBoxList;
    public List<GameObject> championsGO;
    public Text selectedChampionNameText;
    public GameObject getItButtonGO;
    public GameObject objectsShop;
    public GameObject lights;
    public ChooseChampionPanel chooseChampionPanel;
    public Alert alert;
    public Menu menu;
    // Start is called before the first frame update
    void Start()
    {
        shopChampBoxList = new List<ShopChampBox>();
        StartCoroutine(WaitForSaveData());
        Vector3 point = Camera.main.ScreenToWorldPoint(selectedChampionNameText.gameObject.transform.position);
        objectsShop.transform.position = new Vector3(point.x, objectsShop.transform.position.y, objectsShop.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator WaitForSaveData()
    {
        saveData = GameObject.FindWithTag("SaveData").GetComponent<SaveData>();
        while (saveData == null)
        {
            yield return new WaitForSeconds(0.3f);
        }
        
        AddBoxes();
        BoxClicked(0);
    }
    public void AddBoxes()
    {
       
        for (int i = 0; i < saveData.champion.Count; i++)
        {
            ShopChampBox shopChampBox = Instantiate(champBoxPrefab).GetComponent<ShopChampBox>();
            shopChampBoxList.Add(shopChampBox);
            shopChampBox.transform.parent = contentGO.transform;

            shopChampBox.lockedGO.SetActive(!saveData.champion[i].IsAvailable(saveData));
            shopChampBox.charBoxImage.sprite = saveData.champion[i].sprite;
            shopChampBox.priceGO.SetActive(!saveData.champion[i].isBought);
            shopChampBox.priceText.text = "" + saveData.champion[i].coinsPrice;
            shopChampBox.nameGO.SetActive(saveData.champion[i].isBought);
            shopChampBox.checkedGO.SetActive(saveData.champion[i].isBought);
            shopChampBox.nameText.text = saveData.champion[i].name.ToUpper();
            shopChampBox.boxBorder.SetActive(false);
            shopChampBox.shopPanel = this;
            shopChampBox.ID = i;


        }
    }
    public void UpdateBoxes()
    {
       
        for (int i = 0; i < saveData.champion.Count; i++)
        {
             

            shopChampBoxList[i].lockedGO.SetActive(!saveData.champion[i].IsAvailable(saveData));
            shopChampBoxList[i].charBoxImage.sprite = saveData.champion[i].sprite;
            shopChampBoxList[i].priceGO.SetActive(!saveData.champion[i].isBought);
            shopChampBoxList[i].priceText.text = "" + saveData.champion[i].coinsPrice;
            shopChampBoxList[i].nameGO.SetActive(saveData.champion[i].isBought);
            shopChampBoxList[i].checkedGO.SetActive(saveData.champion[i].isBought);
            shopChampBoxList[i].nameText.text = saveData.champion[i].name.ToUpper();
            shopChampBoxList[i].boxBorder.SetActive(false);
            shopChampBoxList[i].shopPanel = this;
            shopChampBoxList[i].ID = i;


        }
    }
    public void GetItemButtonClicked()
    {
        SaveData.Champion champion= saveData.champion[selected];
        if (!champion.IsAvailable(saveData))
        {
            alert.ActivateAlert(champion.requirements.ToString(), "Okay");
        }
        else if (saveData.coins>=saveData.champion[selected].coinsPrice)
        {
            alert.ActivatePurchaseAlert("You want to get "+champion.name+" for "+champion.coinsPrice+" coins?",GetChampion);
        }
        else
        {
            alert.ActivateAlert("You don't have enough coins, earn coins by playing some match","Okay");
        }
    }
    public int GetChampion(string g)
    {

        saveData.BuyChampion(selected);
        menu.UpdateCoinsAndGemsText();
        UpdateBoxes();
        chooseChampionPanel.UpdateData();
        return 1;
    }
    public void BoxClicked(int n)
    {
        selected = n;
        PlayerPrefs.SetInt("MyChampion", n);
        for (int i = 0; i < shopChampBoxList.Count; i++)
        {
            shopChampBoxList[i].SetAsSelected(n == i);
            championsGO[i].SetActive(n == i);
        }
        selectedChampionNameText.text = "" + saveData.champion[n].name.ToUpper();
        getItButtonGO.SetActive(!saveData.champion[n].isBought )  ;
        /*
        championName.text = saveData.champion[selected].name.ToUpper();
        championImage.sprite = saveData.champion[selected].sprite;
        */
    }
}
