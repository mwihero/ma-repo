﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class ParticleUserManager : MonoBehaviour
{
    public bool isOnline;
    public List<ParticleUser> particleUsersScripts;
    public List<GameObject> particleUsers;
    public GameObject particleUserPrefab;
    public int nParticleUsers=5;

    // Start is called before the first frame update
    void Start()
    {
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;

        if (!PhotonNetwork.IsMasterClient && isOnline)
        {
            return;
        }

        for (int i =0; i<nParticleUsers;i++)
        {
            particleUsers.Add(Instantiate(particleUserPrefab));
            particleUsersScripts.Add(particleUsers[i].GetComponent<ParticleUser>());
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public ParticleUser ReturnFreeParticleUser()
    {
        ParticleUser fpu = null;
        int i = 0;
        while (i < particleUsersScripts.Count)
        {
            if (particleUsersScripts[i].free)
            {
                fpu = particleUsersScripts[i];
                i = particleUsersScripts.Count;
            }
            i++;
        }
        print ("Free part:" + fpu);
        return fpu;
    }
}
