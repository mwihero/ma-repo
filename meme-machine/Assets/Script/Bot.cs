﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
public class Bot : MonoBehaviourPunCallbacks, IPunObservable {

	public bool isABot;
    public string name="";

    public CarSystem carSystem;
    public GameManager gameManager;
    public BallsManager ballsManager;
    public bool goingRight;
	public bool goingLeft;
	public bool goingCenter;
    public bool goingStop;
    public bool alive;
	public int CommandUpdateFPS=15;

    float[] retardC = { 0.8f, 0.7f, 0.5f, 0.3f, 0.2f, 0.15f };
    float[] possibilityToUseAbilityC = { 35, 30, 20, 12, 8, 3 };
    int reactionSpeedC = 3;
    int abilityFrequencyC = 3;
    int strategyC = 5;

    float[] retard= { 1f, 0.7f, 0.5f, 0.3f, 0.2f, 0.15f };
    public float[] possibilityToUseAbility = { 10, 9, 7, 6, 5, 3 };
    public int reactionSpeed =3;
    public int abilityFrequency =3;
    public int strategy =3;
    public int trophies;
    public float reflectBounce = 8f;
    public int championID;
    public bool isOnline;

    public Image firstAbilityImage;
    public Image secondAbilityImage;
    public Image thirdAbilityImage;
    public GameManager.Timer casual;
    public int casualDirection=0;
    public ParticleUserManager particleUserManager;
	// Use this for initialization
    void Awake()
    {
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        
    }
	public void Start () {
        int random; 
        random= (int)Random.Range(1, 6);
        retard = retardC;
     possibilityToUseAbility = possibilityToUseAbilityC;
     reactionSpeed = random;
     abilityFrequency = random;
     strategy = random;
        
    carSystem = gameObject.GetComponent<CarSystem> ();
        if (isABot)
        {
            reactionSpeed = PlayerPrefs.GetInt("Bot" + (carSystem.ID + 1) + " ReactionSpeed");
            abilityFrequency = PlayerPrefs.GetInt("Bot" + (carSystem.ID + 1) + " AbilityFrequency");
            strategy = PlayerPrefs.GetInt("Bot" + (carSystem.ID + 1) + " Strategy");
            trophies = PlayerPrefs.GetInt("Bot" + (carSystem.ID + 1) + " Trophies");
        }
        else
        {
            trophies = PlayerPrefs.GetInt("Trophies");
        }
        alive = true;
		StartCoroutine(CommandUpdate());
        firstAbilityImage = GameObject.Find("FirstAbility").transform.Find("Image").GetComponent<Image>();
        secondAbilityImage = GameObject.Find("SecondAbility").transform.Find("Image").GetComponent<Image>();
        thirdAbilityImage = GameObject.Find("ThirdAbility").transform.Find("Image").GetComponent<Image>();

        firstAbilityImage.sprite = Resources.Load<Sprite>("Champions/C"+ championID + "/A");
        secondAbilityImage.sprite = Resources.Load<Sprite>("Champions/C"+ championID + "/B");
        thirdAbilityImage.sprite = Resources.Load<Sprite>("Champions/C"+ championID + "/C");
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        ballsManager = GameObject.Find("GameManager").GetComponent<BallsManager>();
        particleUserManager = GameObject.Find("GameManager").GetComponent<ParticleUserManager>();
        casual = new GameManager.Timer(0.3f);
    }
	
	// Update is called once per frame
	public void Update () {
		if(isABot){
		goingRight = false;
		goingLeft = false;
		goingCenter = false;
            goingStop = false;
            if (carSystem.nBallCollisionPoint > 0) {
			if (carSystem.nRightBalls > carSystem.nLeftBalls) {
				//carSystem.Right ();
				goingRight = true;
			} else {
				//carSystem.Left ();
				goingLeft = true;
			}
		} 
		///*
		else {
                //carSystem.Center();
                if (casual.UpdateTimer()==2 || !casual.isTiming)
                {
                    int c=ballsManager.AliveBallsCounter();
                    if (c<4)
                    {
                        
                        casualDirection = Mathf.RoundToInt(Random.Range(1, 5));
                        if (casualDirection<3)
                        {
                            casual.duration = Random.Range(0.05f, 0.2f);
                        }
                        else
                        {
                            casual.duration = Random.Range(0.4f, 1.2f);
                        }
                    }
                    else
                    {
                        casual.duration = Random.Range(0.1f, 0.4f);
                        casualDirection = Mathf.RoundToInt(Random.Range(1, 3));
                        if (casualDirection < 3)
                        {
                            casual.duration = Random.Range(0.05f, 0.2f);
                        }
                        else
                        {
                            casual.duration = Random.Range(0.1f, 0.4f);
                        }
                    }
                    
                    casual.StartTimer();
                    
                }else if (casual.isTiming)
                {
                    if (casualDirection==1)
                    {
                        goingRight = true;

                    }else if (casualDirection == 2)
                    {
                        goingLeft = true;
                    }
                    else if (casualDirection == 3)
                    {
                        goingCenter = true;
                    }
                    else
                    {
                        goingStop = true;
                    }
                }
                /*
				if (Mathf.Abs (Vector3.Distance (carSystem.center.transform.position, carSystem.car.transform.position)) > 0f) {
					goingCenter = true;
				}*/
		}
		//*/
		}
	}


	private IEnumerator CommandUpdate()
	{
		while(alive){
			if (goingRight) {
				StartCoroutine(RetardCommandRight());
			} else if (goingLeft) {
				StartCoroutine(RetardCommandLeft());
			} else if (goingCenter){
				StartCoroutine(RetardCommandCenter());
            }
            else if (goingStop)
            {
                StartCoroutine(RetardCommandStop());
            }
            yield return new WaitForSeconds(1f/(float)CommandUpdateFPS);
		}
	}
	private IEnumerator RetardCommandRight()
	{
		yield return new WaitForSeconds(retard[reactionSpeed]);
		carSystem.Right ();
	}
	private IEnumerator RetardCommandLeft()
	{
		yield return new WaitForSeconds(retard[reactionSpeed]);
		carSystem.Left ();
	}
	private IEnumerator RetardCommandCenter()
	{
		yield return new WaitForSeconds(retard[reactionSpeed]);
		carSystem.Center ();
	}
    private IEnumerator RetardCommandStop()
    {
        yield return new WaitForSeconds(retard[reactionSpeed]);
        carSystem.Stop();
    }
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		//print ("entrrrrrrrr");
		if (stream.IsWriting)
		{
			stream.SendNext(isABot);
		}
		else
		{
			isABot = (bool) stream.ReceiveNext();
		}
	}
    public void BallTouched(Ball ball)
    {
        print("entra in bot");
    }
}
