﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChampionsList : MonoBehaviour
{
    [System.Serializable]
    public class Champion
    {
        public int id;
        public Sprite image;
        public string name = "";
        public float speed;
        public int dimension;
        public float bounce;


    }
    public Champion[] champions;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
