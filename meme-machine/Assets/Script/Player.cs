﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace com.EYS.memesarena
{
    public class Player : MonoBehaviour
    {

        CarSystem carSystem;

        public bool goingRight;
        public bool goingLeft;
        public bool goingCenter;
        public bool goingStop;

        public bool goingRightEditor;
        public bool goingLeftEditor;
        public bool goingStopEditor;

        public int CommandUpdateFPS = 15;
        public Slider g;
        public Bot botScript;
        private PhotonView photonView;
        public Animator animator;
        public int lastMove = 0;
        public bool isOnline;
        int type = 0;
        public Vector3 goPos;
        public LayerMask lm;
        public GameObject tapMove;
        public bool tapGoing = false;
        public GameObject tapMarker;
        CarSystem.BallCollisionPoint tapPoint;
        // Use this for initialization
        void Awake()
        {
            isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
            photonView = GetComponent<PhotonView>();
            botScript = GetComponent<Bot>();
        }
        void Start()
        {
            carSystem = gameObject.GetComponent<CarSystem>();
            lm = LayerMask.GetMask("TapMove" + carSystem.ID);
            tapMove.layer = LayerMask.NameToLayer("TapMove" + carSystem.ID);

            tapPoint = new CarSystem.BallCollisionPoint();
            StartCoroutine(CommandUpdate());

            if (isOnline)
            {
                if (!photonView.IsMine)
                {
                    return;
                }
            }
            if (!botScript.isABot)
            {
                transform.Find("Camera").gameObject.GetComponent<Camera>().enabled = true;
                GameObject.Find("Main Camera").GetComponent<Camera>().enabled = false;
                //print ("cos");
            }
            else
            {
                return;
            }
            //GameObject.Find("LeftButton").GetComponent<EventTrigger>().OnPointerDown.AddListener(ActivateFirstAbility);
            //GameObject.Find("LeftButton").GetComponent<EventTrigger>().OnPointerDown(UpdateCarPosition(0f));
            //GameObject.Find("LeftButton").GetComponent<EventTrigger>().OnPointerUp(UpdateCarPosition(0.5f));
            //print("vedi de leftareee");
            EventTrigger leftTrigger = GameObject.Find("LeftButton").GetComponent<EventTrigger>();
            EventTrigger.Entry leftEntry = new EventTrigger.Entry();

            leftEntry.eventID = EventTriggerType.PointerDown;
            leftEntry.callback.AddListener((data) => { UpdateCarPosition((float)0f); });
            leftTrigger.triggers.Add(leftEntry);

            leftEntry = new EventTrigger.Entry();
            leftEntry.eventID = EventTriggerType.PointerUp;
            leftEntry.callback.AddListener((data) => { UpdateCarPosition((float)0.5f); });
            leftTrigger.triggers.Add(leftEntry);

            //print("bravooo");
            EventTrigger rightTrigger = GameObject.Find("RightButton").GetComponent<EventTrigger>();
            EventTrigger.Entry rightEntry = new EventTrigger.Entry();

            rightEntry.eventID = EventTriggerType.PointerDown;
            rightEntry.callback.AddListener((data) => { UpdateCarPosition((float)1f); });
            rightTrigger.triggers.Add(rightEntry);

            rightEntry = new EventTrigger.Entry();
            rightEntry.eventID = EventTriggerType.PointerUp;
            rightEntry.callback.AddListener((data) => { UpdateCarPosition((float)0.5f); });
            rightTrigger.triggers.Add(rightEntry);
            
            GameObject.Find("RightButton").active = (type == 0);
            GameObject.Find("LeftButton").active = (type == 0);
            
            ///GameObject.Find("RightButton").GetComponent<EventTrigger>().OnPointerUp(UpdateCarPosition(0.5f));
            //GameObject.Find("RightButton").GetComponent<EventTrigger>().onClick.AddListener(ActivateSecondAbility);
        }

        // Update is called once per frame
        void Update()
        {
            if (!photonView.IsMine && isOnline)
            {
                return;
            }

            if (type==1)
            {
                if (Input.GetMouseButton(0))
                {
                    //create a ray cast and set it to the mouses cursor position in game

                    Ray rayw = transform.Find("Camera").gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(rayw, out hit, 30, lm))
                    {
                        //draw invisible ray cast/vector
                        Debug.DrawLine(rayw.origin, hit.point);
                        //row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
                        //row.transform.eulerAngles = new Vector3 (90,0,row.transform.localEulerAngles.y*-1);
                        //row.transform.eulerAngles =hit.point;
                        //log hit area to the console
                        //Debug.Log(hit.point);
                        goPos = hit.point;
                        tapMarker.transform.position = goPos;
                        if (carSystem.ID==0) {
                            tapMarker.active = false;
                            tapMarker.active = true;
                        }
                        tapGoing = true;
                        //targetGO.transform.position = new Vector3(hit.point.x, targetGO.transform.position.y, hit.point.z);
                    }
                }
                tapPoint.Point(goPos, carSystem.car, carSystem.right);
                goingRight = false;
                goingLeft = false;
                goingCenter = false;
                goingStop = false;
                if (Mathf.Abs(tapPoint.distance) > 0.8f && tapGoing)
                {
                    goingRight = tapPoint.right;
                    goingLeft = !tapPoint.right;
                }
                else if(!Input.GetMouseButton(0))
                {
                    tapGoing = false;
                    goingStop = true;
                }
            }
#if UNITY_ANDROID

            goingRightEditor = false;
            goingLeftEditor = false;

            goingStopEditor = false;
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                goingRightEditor = true;
            }
            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                goingLeftEditor = true;
            }
            else
            {
                goingStopEditor = true;
            }


#else
		goingRight = false;
		goingLeft = false;
            goingCenter = false;
            goingStop = false;
		if(Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)){
			goingRight = true;
		}
		else if(Input.GetKey (KeyCode.A)  || Input.GetKey (KeyCode.LeftArrow)){
			goingLeft = true;
		}
            else{
            goingStop = true;
            }
#endif

        }

        public void UpdateCarPosition(float single)
        {

            if (!photonView.IsMine && isOnline)
            {
                return;
            }


            #if UNITY_ANDROID
            //print ("single: "+single);
            GameObject.Find("GameManager").GetComponent<NetworkGameManager>().InfoText.text = "AOO";
            goingRight = false;
            goingLeft = false;
            goingCenter = false;
            goingStop = false;
            if (single < 0.4f)
            {
                goingLeft = true;

                lastMove = 2;
            }
            else if (single > 0.6f)
            {
                goingRight = true;
                lastMove = 1;
            }
            else
            {
                if (lastMove == 1)
                {
                    //animator.Play("inc dx", -1);
                }
                else if (lastMove == 2)
                {
                    //animator.Play("inc sx", -1);
                }

                lastMove = 0;
                goingStop = true;
            }
            //print("hey "+goingRight);
            #endif

        }
        private IEnumerator CommandUpdate()
        {
            while (true)
            {
                //print ("dimmi");
                if (!botScript.isABot)
                {
                    if (goingRight || goingRightEditor)
                    {
                        //print ("ma...");
                        carSystem.Right();
                        //print ("davvero?");
                    }
                    else if (goingLeft || goingLeftEditor)
                    {
                        carSystem.Left();
                        //print ("davvero?");
                    }
                    else if (goingCenter)
                    {
                        carSystem.Center();
                    }
                    else if (goingStop || goingStopEditor)
                    {
                        carSystem.Stop();
                    }
                }
                yield return new WaitForSeconds(1f / (float)CommandUpdateFPS);
            }
        }
    }
}
