﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class ChartPanel : MonoBehaviour
{
    public GameObject rankRowPrefab;
    public GameObject content;
    public List<RankRow> rankRow;
    public SaveData saveData;
    public RankRow myRankRow;
    public Text leagueName;
    public Text rankID;
    public Text leagueDescription;
    public Menu menu;
    // Start is called before the first frame update
    void Start()
    {
        for (int i=0;i<20;i++)
        {
            rankRow.Add(Instantiate(rankRowPrefab).GetComponent<RankRow>());
            rankRow[rankRow.Count - 1].transform.parent = content.transform;
            //rankRow[rankRow.Count - 1].SetTexts();
        }
        StartCoroutine(WaitForSaveData());
    }
    IEnumerator WaitForSaveData()
    {
        saveData = GameObject.FindWithTag("SaveData").GetComponent<SaveData>();
        yield return new WaitForSeconds(0.3f);
        while (saveData == null)
        {
            yield return new WaitForSeconds(2.3f);
        }
        saveData.UpdateRank();
        UpdateChart();
    }
    public void UpdateChart()
    {
        saveData.orderedUsers = saveData.opponentUsers.OrderByDescending(o => o.trophies).ToList();
        for (int i = 0; i < saveData.orderedUsers.Count; i++)
        {

            rankRow[i].SetTexts(i + 1, saveData.orderedUsers[i].name, saveData.orderedUsers[i].trophies);
            if (saveData.orderedUsers[i].ID == -1)
            {
                myRankRow.SetTexts(i + 1, saveData.orderedUsers[i].name, saveData.orderedUsers[i].trophies);
            }
            rankRow[i].userPic.sprite = menu.pics[saveData.orderedUsers[i].picID];
        }
        leagueName.text =saveData.league[saveData.leagueID].name.ToUpper();
        leagueDescription.text =(saveData.leagueID+1)+"° LEAGUE\nTROPHIES RANGE: "+ saveData.league[saveData.leagueID].minCups+"-"+ saveData.league[saveData.leagueID].maxCups;
        rankID.text = "RANK #" + saveData.rank.rankID;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
}
