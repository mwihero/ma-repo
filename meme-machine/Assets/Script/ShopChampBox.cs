﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShopChampBox : MonoBehaviour
{
    public bool isSelected;
    public GameObject boxBorder;
    public GameObject charBox;
    public Image charBoxImage;
    public GameObject lockedGO;
    public GameObject checkedGO;
    public Text priceText;
    public Text nameText;
    public GameObject priceGO;
    public GameObject nameGO;
    public RectTransform rt;
    public ShopPanel shopPanel;
    public SaveData saveData;
    public int ID;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ClickedOnMe()
    {
        shopPanel.BoxClicked(ID);
    }
    public void SetAsSelected(bool isSelected)
    {
        this.isSelected = isSelected;
        boxBorder.SetActive(isSelected);
        if (isSelected)
        {
            rt.localScale = new Vector2(0.8f,0.8f);
        }
        else
        {
            rt.localScale = new Vector2(0.7f, 0.7f);
        }
    }
}
