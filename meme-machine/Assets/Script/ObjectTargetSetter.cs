﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectTargetSetter : MonoBehaviour {
	public CarSystem carSystem;
	public bool usingThis;
	public int stato=0;
	public GameObject targetImageGO;
    public Image targetImage;
    public GameObject target;
    public LayerMask lm;
	public string tag;
    public Camera camera;
	// Use this for initialization
	void Start () {
        targetImageGO = GameObject.Find("TargetImage");
        targetImage = targetImageGO.GetComponent<Image>();
        camera = transform.Find("Camera").gameObject.GetComponent<Camera>();
    }
	
	// Update is called once per frame
	public void ObjectTargetSetterUpdate () {
		if(usingThis){
		if (stato == 0) {
                
                
                if (Input.GetMouseButtonUp (0)) {
				    stato++;
                    targetImageGO.SetActive(true);
			    }
		}else if (stato == 1) {
                TargetImageUpdate();
                /*
			if (Input.GetMouseButtonUp (0)) {

				Ray rayw = camera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (rayw, out hit, 30,lm)) 
				{
					//draw invisible ray cast/vector
					Debug.DrawLine (rayw.origin, hit.point);
                        print("Clicked on "+ hit.collider.gameObject.name);
					if(hit.collider.gameObject.tag==tag && hit.collider.gameObject!=gameObject){

						target = hit.collider.gameObject;
						if(hit.collider.gameObject.tag=="Car" && target.name!="Car"){
							target = target.gameObject.transform.parent.transform.gameObject;
						}
					    stato++;
                        targetImageGO.SetActive(false);
                    }
				}

			}*/
		}
		}
	}
    public void TargetImageUpdate()
    {
        Ray rayw = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(rayw, out hit, 30, lm))
        {
            //draw invisible ray cast/vector
            Debug.DrawLine(rayw.origin, hit.point);
            targetImageGO.transform.position = Camera.main.WorldToScreenPoint(hit.point);
            //print("Hand on " + hit.collider.gameObject.name);
            if (hit.collider.gameObject.tag == tag && hit.collider.gameObject != gameObject)
            {
                targetImage.color = Color.red;
                //target = hit.collider.gameObject;
                if (Input.GetMouseButtonUp(0))
                {
                    //target = target.gameObject.transform.parent.transform.gameObject;
                    target = hit.collider.gameObject;
                    if (hit.collider.gameObject.tag == "Car" && target.name != "Car")
                    {
                        target = target.gameObject.transform.parent.transform.gameObject;
                    }
                    stato++;
                    targetImageGO.SetActive(false);
                }

                //stato++;

            }
            else
            {
                targetImage.color = Color.white;
            }
            
        }
    }
	public void ActivateSetter(string tag){
		this.tag = tag;
		stato = 0;
		usingThis = true;
		target = null;
		//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
	}
}
