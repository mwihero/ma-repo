﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject mainPanel;
    public GameObject playPanel;
    public GameObject modePanel;
    public GameObject pvPPanel;
    public GameObject pvBotPanel;
    public GameObject pickPanel;
    public GameObject pickPanel2;
    public GameObject choosePanel;
    public GameObject roomPanel;
    public GameObject shopPanel;
    public GameObject shopObjects;
    public GameObject coinsAndGemsPanel;
    public GameObject chartPanel;
    public PhoneMessage phoneMessage;
    public GameManager.Timer randomMessaggesTimer;
    public Text playerNameText;
    public Text playerTrophiesText;
    public Text playerExperienceText;
    public Text playerExperienceMaxText;
    public Text playerLevelText;
    public Image playerExperienceBarFill;
    public Text coinsText;
    public Text gemsText;
    public List<Sprite> pics;
    public Alert alert;

    public SaveData saveData;
    public bool isOnline;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(getData());
        randomMessaggesTimer = new GameManager.Timer(Random.Range(5,20));
        randomMessaggesTimer.StartTimer();

    }
    public IEnumerator getData()
    {
        yield return new WaitForSeconds(0.1f);
        bool b = false;
        saveData = GameObject.FindWithTag("SaveData").GetComponent<SaveData>();
        while (!b)
        {
            if (saveData!=null)
            {
                b = true;
                playerNameText.text = saveData.username;
                playerTrophiesText.text = "" + saveData.trophies;
                playerExperienceText.text = "" + saveData.experience+"/"+ saveData.levelMaxList[saveData.level];
                //playerExperienceMaxText.text = "" + 200;
                playerLevelText.text = "" + saveData.level;
                playerExperienceBarFill.fillAmount = ((float)saveData.experience)/ (float)saveData.levelMaxList[saveData.level];
                UpdateCoinsAndGemsText();
            }
            yield return new WaitForSeconds(0.1f);
        }
        
        if (PlayerPrefs.GetInt("PlayedFirstGame",0)==1)
        {
            if (PlayerPrefs.GetInt("InformChart",0)==0)
            {
                PlayerPrefs.SetInt("InformChart", 1);
                alert.ActivateAlert("You entered in Memes Arena Charts!\nIf you win enough matches you'll get in the next leagues and play with stronger players!","Okay");
            }
        }
    }
    public void Update()
    {
        if (randomMessaggesTimer.UpdateTimer() == 2)
        {
            randomMessaggesTimer.duration = Random.Range(10,100);
            randomMessaggesTimer.StartTimer();
            int n = Random.Range(0,8);

            if (n==0)
            {
                phoneMessage.ActivateMessage(1,"A beautiful person is playing this game","From the bottom of my heart");
            }
            else if (n == 1)
            {
                phoneMessage.ActivateMessage(0, "Game Developer", "Please, watch the ads, i'm poor :(");
            }
            else if (n == 2)
            {
                phoneMessage.ActivateMessage(0, "Game Developer", "My wife asked for divorce");
            }
            else if (n == 3)
            {
                phoneMessage.ActivateMessage(1, "Trump says Ariana Grande will perform to ", "WW3 half time");
            }
            else if (n == 4)
            {
                phoneMessage.ActivateMessage(0, "Explorer", "Still faster than your crush replies");
            }
            else if (n == 5)
            {
                phoneMessage.ActivateMessage(0, "Beethoven", "Song name? This sh*t fire");
            }
            else if (n == 6)
            {
                phoneMessage.ActivateMessage(0, "Mia Khalifa", "Ah gh Agh Ghu Uhg Ag Ah");
            }
            else if (n == 7)
            {
                phoneMessage.ActivateMessage(0, "Doctor", "I'm sorry you have ligma");
            }
        }
    }
    public void UpdateCoinsAndGemsText()
    {
        coinsText.text = "" + saveData.coins;
        gemsText.text = "" + saveData.gems;
    }
    // Update is called once per frame
    
    public void PlayButtonPressed()
    {
        //mainPanel.active = false;
        //playPanel.active = true;
        PvPButtonPressed();
    }
    public void PvPButtonPressed()
    {
        ActivatePanel(pvPPanel);
        //PlayerPrefs.SetInt("IsOnline", 1);
    }
    public void PvPMatchButtonPressed()
    {
        //SceneManager.LoadScene("LobbyScene");

    }
    public void OnStartButtonClicked()
    {
        isOnline = (PlayerPrefs.GetInt("IsOnline", 0) == 1) ? true : false;
        if (isOnline)
        {

        }
        else
        {
            SceneManager.LoadScene("OnlineGameScene");
        }
    }
    public void OnGoPickButtonClicked()
    {
         
        ActivatePanel(pickPanel);
         
        PlayerPrefs.SetInt("IsOnline", 0);
    }
    public void OnGoModeButtonClicked()
    {

        ActivatePanel(modePanel);

        PlayerPrefs.SetInt("IsOnline", 0);
    }
    public void OnGoShopButtonClicked()
    {

        ActivatePanel(shopPanel);

        
    }
    public void OnGoChartButtonClicked()
    {

        ActivatePanel(chartPanel);

        
    }
    public void OnGoPickButtonClicked2()
    {
        ActivatePanel(pickPanel2);
         
        PlayerPrefs.SetInt("IsOnline", 1);
    }
    public void OnGoChooseButtonClicked()
    {
        ActivatePanel(choosePanel);
         
        PlayerPrefs.SetInt("IsOnline", 0);
    }
    public void OnGoMainButtonClicked()
    {
        ActivatePanel(mainPanel);

        PlayerPrefs.SetInt("IsOnline", 0);
    }
    public void PvBotButtonPressed()
    {
        ActivatePanel(playPanel);
         
        PlayerPrefs.SetInt("IsOnline",0);
    }

    public void ActivatePanel(GameObject panel)
    {
        playPanel.active = (playPanel == panel);
        pvPPanel.active = (pvPPanel == panel);
        choosePanel.active = (choosePanel == panel);
        pickPanel2.active = (pickPanel2 == panel);
        pickPanel.active = (pickPanel == panel);
        mainPanel.active = (mainPanel == panel);
        roomPanel.active = (roomPanel == panel);
        mainPanel.active = (mainPanel == panel);
        modePanel.active = (modePanel == panel);
        shopPanel.active = (shopPanel == panel);
        shopObjects.active = (shopPanel == panel);
        chartPanel.active = (chartPanel == panel);
        coinsAndGemsPanel.active = (shopPanel == panel || mainPanel == panel || modePanel == panel);
    }
    public void GarageButtonPressed()
    {
        SceneManager.LoadScene("ao");
    }
    public void ShopButtonPressed()
    {
        SceneManager.LoadScene("SceneNoObjects");
    }
}
