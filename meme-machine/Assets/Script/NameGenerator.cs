﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameGenerator : MonoBehaviour
{
    public string[] name;
    string[] surname;
    string[] adjective;
    string[] noun;
    string[] things;
    string[] prePost;
    public TextAsset nameTextAsset;
    public TextAsset surnameTextAsset;
    public TextAsset adjectiveTextAsset;
    public TextAsset nounTextAsset;
    public TextAsset thingsTextAsset;
    public TextAsset prePostTextAsset;

    public string randomName;
    // Start is called before the first frame update
    void Awake()
    {
        name=nameTextAsset.text.Split('\n');
        surname = surnameTextAsset.text.Split('\n');
        adjective = adjectiveTextAsset.text.Split('\n');
        noun = nounTextAsset.text.Split('\n');
        things = thingsTextAsset.text.Split('\n');
        prePost = prePostTextAsset.text.Split('\n');
        randomName = GenerateName();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public string GenerateName()
    {
        string nickName = "";
        List<string> words=new List<string>();
         
        print("lenght: "+ this.name.Length);
        string name = this.name[Random.Range(0,this.name.Length)];
        words.Add(this.name[Random.Range(0,this.name.Length)]);
        string surname = this.surname[Random.Range(0,this.surname.Length)];
        //words.Add(this.surname[Random.Range(0,this.surname.Length)]);
        string adjective = this.adjective[Random.Range(0,this.adjective.Length)];
        words.Add(this.adjective[Random.Range(0,this.adjective.Length)]);
        string noun = this.noun[Random.Range(0,this.noun.Length)];
        words.Add(this.noun[Random.Range(0,this.noun.Length)]);
        words.Add(this.things[Random.Range(0,this.things.Length)]);
        string prePostString=this.prePost[Random.Range(0,this.prePost.Length)];
        words.Add("");
        bool usedName = false;
        bool usedSurname = false;
        bool usedAdjective = false;
        bool usedNoun = false;
        bool usePrePost = false;

        if (Random.Range(0, 18) == 3)
        {
            usePrePost = true;
            nickName += prePostString.Split(',')[0];
        }
        if (Random.Range(0, 18) == 3)
        {
            
            nickName += words[3];
        }
        int n = Random.Range(0,words.Count-1);
        nickName += words[n];
        if (n < 1)
        {
            if (Random.Range(0, 2) == 3)
            {
                words[n] = words[n].Substring(0, Random.Range(3, 5));
            }
        }
        words.Remove(words[n]);
        if (Random.Range(0,8)==3)
        {
            nickName += ""+Random.Range(0,100);
        }
        else if (Random.Range(0, 8) ==2)
        {
            nickName += ".";
        }
        else if (Random.Range(0, 8) ==2)
        {
            nickName += "_";
        }else if (Random.Range(0, 8) ==2)
        {
            nickName += "x";
        }
        n = Random.Range(0, words.Count);
        if (n<2)
        {
            if (Random.Range(0, 2) == 3)
            {
                words[n]=words[n].Substring(0, Random.Range(3, 5));
            }
        }
        if (nickName.Length<10)
        {
            nickName += words[n];
        }
        int h = Random.Range(0, 10);
        if (h== 3)
        {
            nickName += "" + Random.Range(100, 10000);
        }else if (h==5)
        {
            nickName += "" + Random.Range(0, 10);
        }else if (h==7)
        {
            nickName += "" + Random.Range(10, 100);
        }
        


        if (usePrePost)
        {
            nickName += prePostString.Split(',')[1];
        }
        else
        {
             if (Random.Range(0, 24) == 2)
            {
                nickName += "_";
            }
            else if (Random.Range(0, 14) == 2)
            {
                nickName += "x";
            }
            else if (Random.Range(0, 44) == 2)
            {
                nickName += ".exe";
            }
        }
        if (Random.Range(0,4)==0)
        {

        }
        else
        {
            nickName=nickName.ToLower();
        }
        return nickName;
    }
}
