﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QualitySettingsScript : MonoBehaviour
{
    public Text QualityText;
    // Start is called before the first frame update
    void Start()
    {
        QualitySettings.SetQualityLevel(0, true);
    }

    // Update is called once per frame
    void Update()
    {
        QualityText.text = "" + QualitySettings.GetQualityLevel();
    }
    public void Increase()
    {
        QualitySettings.IncreaseLevel();
    }
    public void Deccrease()
    {
        QualitySettings.DecreaseLevel();
    }
}
