﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointTargetSetter : MonoBehaviour {

	public CarSystem carSystem;
	public bool usingThis;
	public int stato=0;
	public GameObject circle;
	public Vector3 target;
	public Sprite targetSprite;
	public GameObject targetPrefab;
	public GameObject targetGO;
	public GameObject plane;
	public LayerMask lm;
	// Use this for initialization
	void Awake () {
		carSystem = GetComponent<CarSystem> ();
        
       plane = GameObject.FindWithTag ("Field");
		//row = transform.Find ("Car").gameObject.transform.Find ("Row").gameObject;
	}
    private void Start()
    {
        lm = LayerMask.GetMask("TapField" + carSystem.ID);
    }

    // Update is called once per frame
    public void PointTargetSetterUpdate () {
		if (usingThis){

			if (stato == 0) {
				if (Input.GetMouseButtonUp (0)) {
					stato++;
				}
			}
			else if(stato==1){

				if(Input.GetMouseButton(0))
				{
					//create a ray cast and set it to the mouses cursor position in game

					Ray rayw = transform.Find ("Camera").gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (rayw, out hit, 30,lm)) 
					{
						//draw invisible ray cast/vector
						Debug.DrawLine (rayw.origin, hit.point);
						//row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
						//row.transform.eulerAngles = new Vector3 (90,0,row.transform.localEulerAngles.y*-1);
						//row.transform.eulerAngles =hit.point;
						//log hit area to the console
						//Debug.Log(hit.point);
						targetGO.transform.position=new Vector3(hit.point.x,targetGO.transform.position.y,hit.point.z);
					}    
				}
				if (Input.GetMouseButtonUp (0)) {
					Ray rayw = transform.Find ("Camera").gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast (rayw, out hit, 30,lm)) 
					{
						//draw invisible ray cast/vector
						Debug.DrawLine (rayw.origin, hit.point);
						//row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
						//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = false;
						target = hit.point;
						stato = 2;
						//row.transform.LookAt(Vector3.forward);
						usingThis = false;
						Destroy(targetGO);
					}

				}
			}
		}
	}
    public Vector3 SendRayFrom(Vector3 start)
    {
        Vector3 pos= Vector3.zero;
        RaycastHit hit;
        if (Physics.Raycast(start, Vector3.down, out hit, 30, lm))
        {
            //draw invisible ray cast/vector
            //Debug.DrawLine(rayw.origin, hit.point);
            //row.transform.LookAt(new Vector3(hit.point.x,row.transform.position.y,hit.point.z));
            //row.transform.eulerAngles = new Vector3 (90,0,row.transform.localEulerAngles.y*-1);
            //row.transform.eulerAngles =hit.point;
            //log hit area to the console
            //Debug.Log(hit.point);
            target = hit.point;
            pos = target;
        }
        return pos;
    }
	public void ActivateSetter(){
		stato = 0;
		usingThis = true;
		target = Vector3.zero;
		//row.transform.Find ("RowImage").gameObject.GetComponent<SpriteRenderer> ().enabled = true;
		targetGO= GameObject.Instantiate(targetPrefab);
	}
}
