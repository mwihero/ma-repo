﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour {

	public GameObject ball;
	public GameManager gameManager;
	public BallsManager ballsManager;
	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		ballsManager = GameObject.Find ("GameManager").GetComponent<BallsManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void Spawn(){
		//print ("ao spawna");
		if(ballsManager.ReturnFreeBall()>-1){
			ballsManager.balls [ballsManager.ReturnFreeBall ()].transform.position = transform.position;
			ballsManager.ballsScript[ballsManager.ReturnFreeBall()].Activate();
			//gameManager.ballsScript[gameManager.ReturnFreeBall()].Activate((int)Random.Range(0,1.9f));
		}

	}
	public void Spawn(int type){
		//print ("ao spawna");
		if(ballsManager.ReturnFreeBall()>-1){
			ballsManager.balls [ballsManager.ReturnFreeBall ()].transform.position = transform.position;
			ballsManager.ballsScript[ballsManager.ReturnFreeBall()].Activate(type);
			//gameManager.ballsScript[gameManager.ReturnFreeBall()].Activate((int)Random.Range(0,1.9f));
		}

	}
}
