﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneMessage : MonoBehaviour
{
    public Text sender;
    public Text description;
    public Image messageIcon;
    public Text messageName;
    public GameObject messageGO;
    public Sprite[] icons;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ActivateMessage(int type, string sender, string description)
    {
        if (type==0)
        {
            messageName.text = "MESSAGE";
        }else if (type==1)
        {
            messageName.text = "DANKING NEWS";
        }
        messageIcon.sprite = icons[type];
        this.sender.text = sender;
        this.description.text = description;

        messageGO.SetActive(false);
        messageGO.SetActive(true);
    }
}
