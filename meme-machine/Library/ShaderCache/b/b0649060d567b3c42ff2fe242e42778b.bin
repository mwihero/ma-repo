<Q                         SOFTPARTICLES_ON   USE_ALPHA_CLIPING      USE_BLENDING    �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _NormalTex_ST;
in highp vec4 in_POSITION0;
in mediump vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out mediump vec4 vs_COLOR0;
out highp vec4 vs_TEXCOORD0;
out mediump float vs_TEXCOORD1;
out mediump vec4 vs_TEXCOORD2;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_COLOR0 = in_COLOR0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _NormalTex_ST.xy + _NormalTex_ST.zw;
    vs_TEXCOORD0.zw = in_TEXCOORD1.xy * _NormalTex_ST.xy + _NormalTex_ST.zw;
    vs_TEXCOORD1 = in_TEXCOORD1.z;
    u_xlat1.xyz = u_xlat0.xyw * vec3(0.5, 0.5, 0.5);
    u_xlat0.xy = u_xlat1.zz + u_xlat1.xy;
    vs_TEXCOORD2 = u_xlat0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _MainColor;
uniform 	mediump float _Distortion;
uniform 	mediump float _AlphaClip;
UNITY_LOCATION(0) uniform mediump sampler2D _NormalTex;
UNITY_LOCATION(1) uniform mediump sampler2D _GrabTexture;
in mediump vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD0;
in mediump float vs_TEXCOORD1;
in mediump vec4 vs_TEXCOORD2;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
mediump vec4 u_xlat16_2;
mediump float u_xlat16_8;
void main()
{
    u_xlat16_0.xyz = texture(_NormalTex, vs_TEXCOORD0.zw).xyz;
    u_xlat16_1.xyz = texture(_NormalTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat16_0.xyz + (-u_xlat16_1.xyz);
    u_xlat16_2.xyz = vec3(vs_TEXCOORD1) * u_xlat16_2.xyz + u_xlat16_1.xyz;
    u_xlat16_2.xyz = u_xlat16_2.xyz * vec3(2.0, 2.0, 2.0) + vec3(-1.0, -1.0, -1.0);
    u_xlat16_8 = log2(u_xlat16_2.z);
    u_xlat16_2.xy = u_xlat16_2.xy * vec2(_Distortion);
    u_xlat16_8 = u_xlat16_8 * 127.0;
    u_xlat16_8 = exp2(u_xlat16_8);
    u_xlat16_8 = (-u_xlat16_8) + 0.939999998;
    u_xlat16_8 = u_xlat16_8 * _AlphaClip;
    u_xlat16_8 = u_xlat16_8 * 0.200000003;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_8 = min(max(u_xlat16_8, 0.0), 1.0);
#else
    u_xlat16_8 = clamp(u_xlat16_8, 0.0, 1.0);
#endif
    u_xlat16_2.xy = vec2(u_xlat16_8) * u_xlat16_2.xy;
    u_xlat16_2.xy = u_xlat16_2.xy * vs_COLOR0.ww;
    u_xlat16_2.xy = u_xlat16_2.xy * vec2(0.00100000005, 0.00100000005) + vs_TEXCOORD2.xy;
    u_xlat16_2.xy = u_xlat16_2.xy / vs_TEXCOORD2.ww;
    u_xlat16_0.xyz = textureLod(_GrabTexture, u_xlat16_2.xy, 0.0).xyz;
    u_xlat16_2.xyw = _MainColor.xyz + vec3(-1.0, -1.0, -1.0);
    u_xlat16_2.xyw = vs_COLOR0.www * u_xlat16_2.xyw + vec3(1.0, 1.0, 1.0);
    SV_Target0.xyz = u_xlat16_0.xyz * u_xlat16_2.xyw;
    u_xlat16_2.x = _MainColor.w * _MainColor.w;
    u_xlat16_2.x = u_xlat16_8 * u_xlat16_2.x;
    SV_Target0.w = min(u_xlat16_2.x, 1.0);
    return;
}

#endif
  9                             
   _NormalTex                    _GrabTexture             