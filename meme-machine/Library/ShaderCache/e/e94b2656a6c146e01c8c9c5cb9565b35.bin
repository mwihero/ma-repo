<Q                         DIRECTIONAL    LIGHTPROBE_SH      SHADOWS_SCREEN     VERTEXLIGHT_ON        _METALLICGLOSSMAP   _8  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 unity_4LightPosX0;
uniform 	vec4 unity_4LightPosY0;
uniform 	vec4 unity_4LightPosZ0;
uniform 	mediump vec4 unity_4LightAtten0;
uniform 	mediump vec4 unity_LightColor[8];
uniform 	mediump vec4 unity_SHAr;
uniform 	mediump vec4 unity_SHAg;
uniform 	mediump vec4 unity_SHAb;
uniform 	mediump vec4 unity_SHBr;
uniform 	mediump vec4 unity_SHBg;
uniform 	mediump vec4 unity_SHBb;
uniform 	mediump vec4 unity_SHC;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _DetailAlbedoMap_ST;
uniform 	mediump float _UVSec;
in highp vec4 in_POSITION0;
in mediump vec3 in_NORMAL0;
in highp vec2 in_TEXCOORD0;
in highp vec2 in_TEXCOORD1;
out highp vec4 vs_TEXCOORD0;
out mediump vec4 vs_TEXCOORD1;
out mediump vec4 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out mediump vec4 vs_TEXCOORD4;
out mediump vec4 vs_TEXCOORD5;
vec4 u_xlat0;
vec4 u_xlat1;
bool u_xlatb1;
vec3 u_xlat2;
vec4 u_xlat3;
mediump vec4 u_xlat16_3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
float u_xlat31;
float u_xlat32;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(_UVSec==0.0);
#else
    u_xlatb1 = _UVSec==0.0;
#endif
    u_xlat1.xy = (bool(u_xlatb1)) ? in_TEXCOORD0.xy : in_TEXCOORD1.xy;
    vs_TEXCOORD0.zw = u_xlat1.xy * _DetailAlbedoMap_ST.xy + _DetailAlbedoMap_ST.zw;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD1.w = 0.0;
    u_xlat1.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat1.xyz;
    u_xlat2.xyz = u_xlat1.xyz + (-_WorldSpaceCameraPos.xyz);
    u_xlat31 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat31 = inversesqrt(u_xlat31);
    u_xlat2.xyz = vec3(u_xlat31) * u_xlat2.xyz;
    vs_TEXCOORD1.xyz = u_xlat2.xyz;
    u_xlat3 = (-u_xlat1.yyyy) + unity_4LightPosY0;
    u_xlat4 = u_xlat3 * u_xlat3;
    u_xlat5 = (-u_xlat1.xxxx) + unity_4LightPosX0;
    u_xlat1 = (-u_xlat1.zzzz) + unity_4LightPosZ0;
    u_xlat4 = u_xlat5 * u_xlat5 + u_xlat4;
    u_xlat4 = u_xlat1 * u_xlat1 + u_xlat4;
    u_xlat4 = max(u_xlat4, vec4(9.99999997e-07, 9.99999997e-07, 9.99999997e-07, 9.99999997e-07));
    u_xlat6 = u_xlat4 * unity_4LightAtten0 + vec4(1.0, 1.0, 1.0, 1.0);
    u_xlat4 = inversesqrt(u_xlat4);
    u_xlat6 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat6;
    u_xlat7.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat7.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat7.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat32 = dot(u_xlat7.xyz, u_xlat7.xyz);
    u_xlat32 = inversesqrt(u_xlat32);
    u_xlat7.xyz = vec3(u_xlat32) * u_xlat7.xyz;
    u_xlat3 = u_xlat3 * u_xlat7.yyyy;
    u_xlat3 = u_xlat5 * u_xlat7.xxxx + u_xlat3;
    u_xlat1 = u_xlat1 * u_xlat7.zzzz + u_xlat3;
    u_xlat1 = u_xlat4 * u_xlat1;
    u_xlat1 = max(u_xlat1, vec4(0.0, 0.0, 0.0, 0.0));
    u_xlat1 = u_xlat6 * u_xlat1;
    u_xlat3.xyz = u_xlat1.yyy * unity_LightColor[1].xyz;
    u_xlat3.xyz = unity_LightColor[0].xyz * u_xlat1.xxx + u_xlat3.xyz;
    u_xlat1.xyz = unity_LightColor[2].xyz * u_xlat1.zzz + u_xlat3.xyz;
    u_xlat1.xyz = unity_LightColor[3].xyz * u_xlat1.www + u_xlat1.xyz;
    u_xlat16_8.x = u_xlat7.y * u_xlat7.y;
    u_xlat16_8.x = u_xlat7.x * u_xlat7.x + (-u_xlat16_8.x);
    u_xlat16_3 = u_xlat7.yzzx * u_xlat7.xyzz;
    u_xlat16_9.x = dot(unity_SHBr, u_xlat16_3);
    u_xlat16_9.y = dot(unity_SHBg, u_xlat16_3);
    u_xlat16_9.z = dot(unity_SHBb, u_xlat16_3);
    u_xlat16_8.xyz = unity_SHC.xyz * u_xlat16_8.xxx + u_xlat16_9.xyz;
    u_xlat7.w = 1.0;
    u_xlat16_9.x = dot(unity_SHAr, u_xlat7);
    u_xlat16_9.y = dot(unity_SHAg, u_xlat7);
    u_xlat16_9.z = dot(unity_SHAb, u_xlat7);
    u_xlat16_8.xyz = u_xlat16_8.xyz + u_xlat16_9.xyz;
    u_xlat16_8.xyz = max(u_xlat16_8.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat4.xyz = log2(u_xlat16_8.xyz);
    u_xlat4.xyz = u_xlat4.xyz * vec3(0.416666657, 0.416666657, 0.416666657);
    u_xlat4.xyz = exp2(u_xlat4.xyz);
    u_xlat4.xyz = u_xlat4.xyz * vec3(1.05499995, 1.05499995, 1.05499995) + vec3(-0.0549999997, -0.0549999997, -0.0549999997);
    u_xlat4.xyz = max(u_xlat4.xyz, vec3(0.0, 0.0, 0.0));
    vs_TEXCOORD2.xyz = u_xlat1.xyz + u_xlat4.xyz;
    vs_TEXCOORD2.w = 0.0;
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD3.zw = u_xlat0.zw;
    vs_TEXCOORD3.xy = u_xlat1.zz + u_xlat1.xw;
    u_xlat16_8.x = dot(u_xlat2.xyz, u_xlat7.xyz);
    u_xlat16_8.x = u_xlat16_8.x + u_xlat16_8.x;
    vs_TEXCOORD4.yzw = u_xlat7.xyz * (-u_xlat16_8.xxx) + u_xlat2.xyz;
    u_xlat16_8.x = dot(u_xlat7.xyz, (-u_xlat2.xyz));
#ifdef UNITY_ADRENO_ES3
    u_xlat16_8.x = min(max(u_xlat16_8.x, 0.0), 1.0);
#else
    u_xlat16_8.x = clamp(u_xlat16_8.x, 0.0, 1.0);
#endif
    vs_TEXCOORD5.xyz = u_xlat7.xyz;
    u_xlat16_8.x = (-u_xlat16_8.x) + 1.0;
    u_xlat16_8.x = u_xlat16_8.x * u_xlat16_8.x;
    vs_TEXCOORD5.w = u_xlat16_8.x * u_xlat16_8.x;
    vs_TEXCOORD4.x = 0.0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _WorldSpaceLightPos0;
uniform 	vec4 unity_SpecCube0_BoxMax;
uniform 	vec4 unity_SpecCube0_BoxMin;
uniform 	vec4 unity_SpecCube0_ProbePosition;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	vec4 unity_SpecCube1_BoxMax;
uniform 	vec4 unity_SpecCube1_BoxMin;
uniform 	vec4 unity_SpecCube1_ProbePosition;
uniform 	mediump vec4 unity_SpecCube1_HDR;
uniform 	mediump vec4 _LightColor0;
uniform 	mediump vec4 _Color;
uniform 	float _GlossMapScale;
uniform 	mediump float _OcclusionStrength;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(1) uniform mediump sampler2D _MetallicGlossMap;
UNITY_LOCATION(2) uniform mediump sampler2D _ShadowMapTexture;
UNITY_LOCATION(3) uniform mediump sampler2D _OcclusionMap;
UNITY_LOCATION(4) uniform highp sampler2D unity_NHxRoughness;
UNITY_LOCATION(5) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(6) uniform mediump samplerCube unity_SpecCube1;
in highp vec4 vs_TEXCOORD0;
in mediump vec4 vs_TEXCOORD2;
in highp vec4 vs_TEXCOORD3;
in mediump vec4 vs_TEXCOORD4;
in mediump vec4 vs_TEXCOORD5;
layout(location = 0) out mediump vec4 SV_Target0;
float u_xlat0;
mediump vec2 u_xlat16_0;
vec2 u_xlat1;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec4 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump float u_xlat16_5;
mediump vec3 u_xlat16_6;
vec3 u_xlat7;
mediump vec4 u_xlat16_7;
vec3 u_xlat8;
vec3 u_xlat9;
bvec3 u_xlatb9;
mediump vec3 u_xlat16_10;
bvec3 u_xlatb11;
mediump vec3 u_xlat16_12;
mediump vec3 u_xlat16_18;
vec3 u_xlat23;
mediump vec3 u_xlat16_23;
vec2 u_xlat26;
mediump float u_xlat16_26;
bool u_xlatb26;
mediump float u_xlat16_39;
mediump float u_xlat16_42;
mediump float u_xlat16_43;
mediump float u_xlat16_45;
void main()
{
    u_xlat16_0.xy = texture(_MetallicGlossMap, vs_TEXCOORD0.xy).xw;
    u_xlat16_1.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat16_1.xyz * _Color.xyz;
    u_xlat16_3.xyz = _Color.xyz * u_xlat16_1.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat16_3.xyz = u_xlat16_0.xxx * u_xlat16_3.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat16_42 = (-u_xlat16_0.x) * 0.779083729 + 0.779083729;
    u_xlat16_4.xyz = u_xlat16_2.xyz * vec3(u_xlat16_42);
    u_xlat16_0.x = dot(vs_TEXCOORD5.xyz, _WorldSpaceLightPos0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_0.x = min(max(u_xlat16_0.x, 0.0), 1.0);
#else
    u_xlat16_0.x = clamp(u_xlat16_0.x, 0.0, 1.0);
#endif
    u_xlat26.xy = vs_TEXCOORD3.xy / vs_TEXCOORD3.ww;
    u_xlat16_26 = texture(_ShadowMapTexture, u_xlat26.xy).x;
    u_xlat16_39 = texture(_OcclusionMap, vs_TEXCOORD0.xy).y;
    u_xlat16_43 = (-_OcclusionStrength) + 1.0;
    u_xlat16_43 = u_xlat16_39 * _OcclusionStrength + u_xlat16_43;
    u_xlat16_5 = dot(vs_TEXCOORD4.yzw, _WorldSpaceLightPos0.xyz);
    u_xlat1.y = (-u_xlat16_0.y) * _GlossMapScale + 1.0;
    u_xlat16_18.xyz = vec3(u_xlat16_26) * _LightColor0.xyz;
    u_xlat16_6.xyz = vec3(u_xlat16_43) * vs_TEXCOORD2.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb26 = !!(0.0<unity_SpecCube0_ProbePosition.w);
#else
    u_xlatb26 = 0.0<unity_SpecCube0_ProbePosition.w;
#endif
    if(u_xlatb26){
        u_xlat16_26 = dot(vs_TEXCOORD4.yzw, vs_TEXCOORD4.yzw);
        u_xlat16_26 = inversesqrt(u_xlat16_26);
        u_xlat2.xyz = vec3(u_xlat16_26) * vs_TEXCOORD4.yzw;
        u_xlat7.xyz = unity_SpecCube0_BoxMax.xyz / u_xlat2.xyz;
        u_xlat8.xyz = unity_SpecCube0_BoxMin.xyz / u_xlat2.xyz;
        u_xlatb9.xyz = lessThan(vec4(0.0, 0.0, 0.0, 0.0), u_xlat2.xyzx).xyz;
        {
            vec3 hlslcc_movcTemp = u_xlat7;
            hlslcc_movcTemp.x = (u_xlatb9.x) ? u_xlat7.x : u_xlat8.x;
            hlslcc_movcTemp.y = (u_xlatb9.y) ? u_xlat7.y : u_xlat8.y;
            hlslcc_movcTemp.z = (u_xlatb9.z) ? u_xlat7.z : u_xlat8.z;
            u_xlat7 = hlslcc_movcTemp;
        }
        u_xlat26.x = min(u_xlat7.y, u_xlat7.x);
        u_xlat26.x = min(u_xlat7.z, u_xlat26.x);
        u_xlat2.xyz = u_xlat2.xyz * u_xlat26.xxx + (-unity_SpecCube0_ProbePosition.xyz);
    } else {
        u_xlat2.xyz = vs_TEXCOORD4.yzw;
    }
    u_xlat16_45 = (-u_xlat1.y) * 0.699999988 + 1.70000005;
    u_xlat16_45 = u_xlat1.y * u_xlat16_45;
    u_xlat16_45 = u_xlat16_45 * 6.0;
    u_xlat16_2 = textureLod(unity_SpecCube0, u_xlat2.xyz, u_xlat16_45);
    u_xlat16_10.x = u_xlat16_2.w + -1.0;
    u_xlat16_10.x = unity_SpecCube0_HDR.w * u_xlat16_10.x + 1.0;
    u_xlat16_10.x = u_xlat16_10.x * unity_SpecCube0_HDR.x;
    u_xlat16_23.xyz = u_xlat16_2.xyz * u_xlat16_10.xxx;
#ifdef UNITY_ADRENO_ES3
    u_xlatb26 = !!(unity_SpecCube0_BoxMin.w<0.999989986);
#else
    u_xlatb26 = unity_SpecCube0_BoxMin.w<0.999989986;
#endif
    if(u_xlatb26){
#ifdef UNITY_ADRENO_ES3
        u_xlatb26 = !!(0.0<unity_SpecCube1_ProbePosition.w);
#else
        u_xlatb26 = 0.0<unity_SpecCube1_ProbePosition.w;
#endif
        if(u_xlatb26){
            u_xlat16_26 = dot(vs_TEXCOORD4.yzw, vs_TEXCOORD4.yzw);
            u_xlat16_26 = inversesqrt(u_xlat16_26);
            u_xlat7.xyz = vec3(u_xlat16_26) * vs_TEXCOORD4.yzw;
            u_xlat8.xyz = unity_SpecCube1_BoxMax.xyz / u_xlat7.xyz;
            u_xlat9.xyz = unity_SpecCube1_BoxMin.xyz / u_xlat7.xyz;
            u_xlatb11.xyz = lessThan(vec4(0.0, 0.0, 0.0, 0.0), u_xlat7.xyzx).xyz;
            {
                vec3 hlslcc_movcTemp = u_xlat8;
                hlslcc_movcTemp.x = (u_xlatb11.x) ? u_xlat8.x : u_xlat9.x;
                hlslcc_movcTemp.y = (u_xlatb11.y) ? u_xlat8.y : u_xlat9.y;
                hlslcc_movcTemp.z = (u_xlatb11.z) ? u_xlat8.z : u_xlat9.z;
                u_xlat8 = hlslcc_movcTemp;
            }
            u_xlat26.x = min(u_xlat8.y, u_xlat8.x);
            u_xlat26.x = min(u_xlat8.z, u_xlat26.x);
            u_xlat7.xyz = u_xlat7.xyz * u_xlat26.xxx + (-unity_SpecCube1_ProbePosition.xyz);
        } else {
            u_xlat7.xyz = vs_TEXCOORD4.yzw;
        }
        u_xlat16_7 = textureLod(unity_SpecCube1, u_xlat7.xyz, u_xlat16_45);
        u_xlat16_45 = u_xlat16_7.w + -1.0;
        u_xlat16_45 = unity_SpecCube1_HDR.w * u_xlat16_45 + 1.0;
        u_xlat16_45 = u_xlat16_45 * unity_SpecCube1_HDR.x;
        u_xlat16_12.xyz = u_xlat16_7.xyz * vec3(u_xlat16_45);
        u_xlat16_2.xyz = u_xlat16_10.xxx * u_xlat16_2.xyz + (-u_xlat16_12.xyz);
        u_xlat23.xyz = unity_SpecCube0_BoxMin.www * u_xlat16_2.xyz + u_xlat16_12.xyz;
        u_xlat16_23.xyz = u_xlat23.xyz;
    }
    u_xlat16_10.xyz = vec3(u_xlat16_43) * u_xlat16_23.xyz;
    u_xlat16_18.xyz = u_xlat16_0.xxx * u_xlat16_18.xyz;
    u_xlat16_42 = (-u_xlat16_42) + 1.0;
    u_xlat16_42 = u_xlat16_0.y * _GlossMapScale + u_xlat16_42;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_42 = min(max(u_xlat16_42, 0.0), 1.0);
#else
    u_xlat16_42 = clamp(u_xlat16_42, 0.0, 1.0);
#endif
    u_xlat16_12.xyz = (-u_xlat16_3.xyz) + vec3(u_xlat16_42);
    u_xlat16_12.xyz = vs_TEXCOORD5.www * u_xlat16_12.xyz + u_xlat16_3.xyz;
    u_xlat16_10.xyz = u_xlat16_10.xyz * u_xlat16_12.xyz;
    u_xlat16_6.xyz = u_xlat16_6.xyz * u_xlat16_4.xyz + u_xlat16_10.xyz;
    u_xlat16_42 = u_xlat16_5 * u_xlat16_5;
    u_xlat16_1.x = u_xlat16_42 * u_xlat16_42;
    u_xlat1.x = u_xlat16_1.x;
    u_xlat0 = texture(unity_NHxRoughness, u_xlat1.xy).x;
    u_xlat0 = u_xlat0 * 16.0;
    u_xlat16_3.xyz = vec3(u_xlat0) * u_xlat16_3.xyz + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * u_xlat16_18.xyz + u_xlat16_6.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
 3                                _MainTex                  _MetallicGlossMap                   _ShadowMapTexture                   _OcclusionMap                   unity_NHxRoughness                  unity_SpecCube0                 unity_SpecCube1              